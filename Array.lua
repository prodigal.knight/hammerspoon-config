local Symbol = require('Symbol')

local EMPTY = Symbol('<empty>')

local MAX_ARR_LENGTH = 0xFFFFFFFF

-- Mimic JS Array object & methods
local Array = {}
Array.prototype = {}

Array.EMPTY_ELEMENT = EMPTY

-- Check to see if object is Array
function Array.isArray(value)
	return type(value) == 'table' and value.prototype == Array.prototype
end

-- Check to see if object is Array or plain Lua array
function Array.isArrayLike(value)
	return type(value) == 'table' and (Array.isArray(value) or rawget(value, 1) ~= nil or next(value) == nil)
end

-- Create an Array from the given array-like value, optionally mapping values
function Array.from(value, mapFn)
	if not Array.isArrayLike(value) then
		error('Value must be an Array or array-like structure')
	end

	return Array:new(value):map(mapFn or function(v) return v end)
end

-- Create an Array of the given values
function Array.of(...)
	return Array:new(...)
end

-- Spread the given Array or array-like value into multiple entries (alternative to table.unpack - useful?)
function Array.spread(value)
	if not Array.isArrayLike(value) then
		error('Value must be an Array or array-like structure')
	end

	return table.unpack(value)
end

-- Create a new Array with either the given length (if only one numeric argument) or from the given values
function Array:new(...)
	local args = table.pack(...)
	local entries

	if #args == 1 and type(args[1]) == 'number' then
		local len = args[1]
		if len > MAX_ARR_LENGTH then
			error(string.format('Desired length of %d greater than maximum array length %d', len, MAX_ARR_LENGTH))
		end
		entries = {}
		for i = 1, len do
			entries[i] = EMPTY
		end
	else
		entries = args
	end

	self.__index = self

	return setmetatable(entries, self)
end

-- Stringify this Array by joining values with a comma
function Array:__tostring()
	return Array.join(self)
end

-- Join this Array's values with the given separator, or a comma if none is provided
function Array:join(sep)
	return table.concat(
		Array.map(self, function(v)
			return v == EMPTY_ELEMENT and 'null' or string.format('%s', v)
		end),
		sep or ','
	)
end

-- Push the given values onto the end of this Array
function Array:push(...)
	local args = table.pack(...)

	for i = 1, #args do
		table.insert(self, args[i])
	end

	return #self
end

-- Pop a value from the end of this Array
function Array:pop()
	local res = table.remove(self)

	return res == EMPTY_ELEMENT and nil or res
end

-- Add the given values to the beginning of this Array
function Array:unshift(...)
	local args = table.pack(...)

	for i = #args, 1, -1 do
		table.insert(self, 1, args[i])
	end

	return #self
end

-- Shift a value off the front of this Array
function Array:shift()
	local res = table.remove(self, 1)

	return res == EMPTY_ELEMENT and nil or res
end

-- Concatenate this Array with the given Arrays or values, returning a new Array
function Array:concat(...)
	local args = table.pack(...)
	local res = Array.slice(self)

	for i,arg in ipairs(args) do
		if Array.isArrayLike(arg) then
			for j = 1, #arg do
				res:push(arg[j])
			end
		else
			res:push(arg)
		end
	end

	return res
end

-- Run a callback with each value of this Array, not modifying the array itself or returning a new Array
function Array:forEach(cb)
	for i,v in ipairs(self) do
		if v ~= EMPTY_ELEMENT then
			cb(v, i, self)
		end
	end
end

-- Map each value in this Array to a new value based on its contents, returning a new Array with the mapped values
function Array:map(cb)
	local res = Array:new()

	for i,v in ipairs(self) do
		if v == EMPTY_ELEMENT then
			res:push(v)
		else
			res:push(cb(v, i, self))
		end
	end

	return res
end

-- Check the values in this Array for truthiness, returning true if any element in the Array fulfills the callback
function Array:some(cb)
	for i,v in ipairs(self) do
		if v ~= EMPTY_ELEMENT and cb(v, i, self) then
			return true
		end
	end

	return false
end

-- Check the values in this Array for truthiness, returning false if any element in the Array fails the callback
function Array:every(cb)
	for i,v in ipairs(self) do
		if v ~= EMPTY_ELEMENT and cb(v, i, self) == false then
			return false
		end
	end

	return true
end

-- Reduce the values of this Array into an accumulated result, starting from the beginning
function Array:reduce(cb, init)
	if #self == 0 and not init then
		error('reduce of empty array with no initial value')
	end

	local acc = init or self[1]
	local idx = init and 1 or 2

	for i = idx, #self do
		local v = self[i]
		if v ~= EMPTY_ELEMENT then
			acc = cb(acc, v, i, self)
		end
	end

	return acc
end

-- Reduce this Array, starting from the end
function Array:reduceRight(cb, init)
	if #self == 0 and not init then
		error('reduce of empty array with no initial value')
	end

	local acc = init or self[#self]
	local idx = init and #self or #self - 1

	for i = idx, 1, -1 do
		local v = self[i]
		if v ~= EMPTY_ELEMENT then
			acc = cb(acc, v, i, self)
		end
	end

	return acc
end

-- Sort this Array's entries in-place. Returns itself.
function Array:sort(cb)
	table.sort(self, cb)

	return self
end

-- Fill this Array with the given value
function Array:fill(v)
	for i = 1, #self do
		self[i] = v
	end

  return self
end

-- Create a new Array of the values in this Array that match the given predicate
function Array:filter(cb)
	local res = Array:new()

	for i,v in ipairs(self) do
		if v ~= EMPTY_ELEMENT and cb(v, i, self) then
			res:push(v)
		end
	end

	return res
end

-- Find the first entry in this Array that matches the given predicate
function Array:find(cb)
	for i,v in ipairs(self) do
		if cb(v == EMPTY_ELEMENT and nil or v, i, self) then
			return v
		end
	end
end

-- Find the index of the first entry in this Array that matches the given predicate
function Array:findIndex(cb)
	for i,v in ipairs(self) do
		if cb(v == EMPTY_ELEMENT and nil or v, i, self) then
			return i
		end
	end

	return -1
end

-- Find the index of the given element
function Array:indexOf(value)
	for i,v in ipairs(self) do
		if v == value then
			return i
		end
	end

	return -1
end

-- Find the last index of the given element
function Array:lastIndexOf(value)
	for i = #self, 1, -1 do
		if self[i] == value then
			return i
		end
	end

	return -1
end

-- Check to see if this Array includes a matching element
function Array:includes(value)
	for i,v in ipairs(self) do
		if v == value then
			return true
		end
	end

	return false
end

-- Flatten the elements of this Array to the given depth (or 1 level if not provided)
function Array:flat(d)
	local depth = d or 1

	if depth == 0 then
		return self:slice()
	end

	return Array.reduce(
		self,
		function(acc, value)
			return acc:concat(Array.isArray(value) and value:flat(depth - 1) or value)
		end,
		Array:new()
	)
end

-- Flatten the elements of this Array while mapping them to new values
function Array:flatMap(cb)
	return Array.reduce(
		self,
		function(acc, value, i, arr)
			return acc:concat(cb(value, i, arr))
		end,
		Array:new()
	)
end

-- Reverse the elements of this Array in-place. Returns itself.
function Array:reverse()
	local startIdx = 1
	local endIdx = #self

	while startIdx < endIdx do
		local t = self[startIdx]
		self[startIdx] = self[endIdx]
		self[endIdx] = t
		startIdx = startIdx + 1
		endIdx = endIdx - 1
	end

	return self
end

-- Get the indices of this Array
function Array:keys()
	local res = Array:new()

	for i = 1, #self do
		res:push(i)
	end

	return res
end

-- Get the values of this Array
function Array:values()
	local res = Array:new()

	for i = 1, #self do
		res:push(self[i])
	end

	return res
end

-- Get the index-value pairs of this Array
function Array:entries()
	local res = Array:new()

	for i,v in ipairs(self) do
		res:push(Array:new(i, v))
	end

	return res
end

-- Get a new Array containing a shallow copy of the elements of this Array from start to end. Negative indices are mapped from the end of this Array.
function Array:slice(s, e)
	if s == 0 then
		error('Indices start at 1')
	end

	local res = Array:new()

	local startIdx = 1
	local endIdx = #self

	if s then
		if s > #self then
			return res
		elseif s < 0 then
			startIdx = #self + s + 1
		else
			startIdx = s + 1
		end
	end
	if e then
		if e < 0 then
			endIdx = #self + e
		elseif e < #self then
			endIdx = e
		end
	end

	for i = startIdx, endIdx do
		res:push(self[i])
	end

	return res
end

function Array:spread()
  return table.unpack(self)
end

-- Get the value at the given Array index
function Array:at(idx)
	if idx == 0 then
		error('0 is not a valid array index')
	end

	local index = idx > 0 and idx or #self + idx

	return self[index]
end

-- JS-style __tostring()
function Array:toString()
	return Array.join(self)
end

return Array
