#pragma once

@import Cocoa;
@import Foundation;
@import LuaSkin;
@import QuartzCore;

#import "CanvasGradient.h"
#import "DrawingInstruction.h"
#import "Path2D.h"
#import "shared.h"

static const char *CANVAS_USERDATA_TAG = "EnhancedCanvasView";

typedef enum TextDirection : NSUInteger {
  TextDirectionLtr,
  TextDirectionRtl,
} TextDirection;

@interface HSCanvasWindow : NSPanel <NSWindowDelegate>
@property NSString *subroleOverride;
@end

/******************************************************************************\
 * MASSIVE TODO: Move from CALayer to CGLayer to better support actual        *
 * CanvasRenderingContext2D capabilities                                      *
 *                                                                            *
 * References:                                                                *
 * https://flylib.com/books/en/3.310.1.64/1/                                  *
\******************************************************************************/

@interface EnhancedCanvasState : NSObject
@property double                    baselineOffset;
@property TextDirection             direction;
@property NSColor                  *fillColor;
@property CanvasGradient           *fillGradient;
@property NSFont                   *font;
@property double                    globalAlpha;
@property BOOL                      imageSmoothingEnabled;
@property CGLineCap                 lineCap;
@property NSArray<NSNumber *>      *lineDashPattern;
@property double                    lineDashOffset;
@property CGLineJoin                lineJoin;
@property double                    lineWidth;
@property double                    miterLimit;
@property double                    shadowBlur;
@property NSColor                  *shadowColor;
@property NSSize                    shadowOffset;
@property NSColor                  *strokeColor;
@property CanvasGradient           *strokeGradient;
@property CATextLayerAlignmentMode  textAlign;

// These are tracked because there's no way to get them from the current graphics context even though they're tracked there as well
@property Path2D                 *_clip;
@property NSString               *_fillColorStr;
@property NSString               *_fontStr;
@property NSCompositingOperation  _globalCompositeOperation;
@property NSString               *_shadowColorStr;
@property NSString               *_strokeColorStr;
@property TextBaseline            _textBaseline;
@property CGAffineTransform       _transform;
// These are tracked to determine which fill/stroke backing object to use at instruction creation
@property BOOL                    _useGradientFill;
@property BOOL                    _useGradientStroke;

- (instancetype)init;
- (EnhancedCanvasState *)copy;
@end

@class DrawingInstruction;

@interface EnhancedCanvasView : NSView
@property int             selfRef;
@property HSCanvasWindow *wrapperWindow;

@property NSMutableArray<DrawingInstruction*> *instructions;

@property CGContextRef  ctx;
@property Path2D       *path;
@property BOOL          clear;

@property EnhancedCanvasState                  *state;
@property NSMutableArray<EnhancedCanvasState*> *_states;

- (void)addInstruction:(DrawingInstruction *)instruction;
- (NSImage *)getImageDataInRect:(NSRect)rect;
@end

@interface EnhancedCanvasView (viewNotifications)
- (void)willRemoveFromCanvas;
- (void)didRemoveFromCanvas;
- (void)willAddToCanvas;
- (void)didAddToCanvas;

- (void)canvasWillHide;
- (void)canvasDidHide;
- (void)canvasWillShow;
- (void)canvasDidShow;
@end

@interface SmartTextLayer : CATextLayer
@property CGFloat baselineOffset;
@property CGFloat maxWidth;
@end

static NSDictionary<NSString *, NSNumber *> *CompositeOperations     = nil;
static NSDictionary<NSNumber *, NSString *> *CompositeOperationNames = nil;

static NSDictionary<NSString *, NSNumber *> *LineCapStyles     = nil;
static NSDictionary<NSNumber *, NSString *> *LineCapStyleNames = nil;

static NSDictionary<NSString *, NSNumber *> *LineJoinStyles     = nil;
static NSDictionary<NSNumber *, NSString *> *LineJoinStyleNames = nil;

static NSArray<NSString *>                  *TextAlignments   = nil;
static NSDictionary<NSString *, NSNumber *> *NSTextAlignments = nil;

static NSDictionary<NSString *, NSNumber *> *TextBaselines     = nil;
static NSDictionary<NSNumber *, NSString *> *TextBaselineNames = nil;

static NSDictionary<NSString *, NSNumber *> *TextDirections     = nil;
static NSDictionary<NSNumber *, NSString *> *TextDirectionNames = nil;

static NSDictionary<NSString *, NSNumber *> *WindingRules     = nil;
static NSDictionary<NSNumber *, NSString *> *WindingRuleNames = nil;

@interface Constants : NSObject
@end

@implementation Constants
+ (void)initialize {
  if (CompositeOperations == nil) {
    CompositeOperations = @{
      @"clear"            : @(NSCompositingOperationClear),
      @"source-over"      : @(NSCompositingOperationSourceOver),
      @"source-in"        : @(NSCompositingOperationSourceIn),
      @"source-out"       : @(NSCompositingOperationSourceOut),
      @"source-atop"      : @(NSCompositingOperationSourceAtop),
      @"destination-over" : @(NSCompositingOperationDestinationOver),
      @"destination-in"   : @(NSCompositingOperationDestinationIn),
      @"destination-out"  : @(NSCompositingOperationDestinationOut),
      @"destination-atop" : @(NSCompositingOperationDestinationAtop),
      @"darker"           : @(NSCompositingOperationPlusDarker),
      @"lighter"          : @(NSCompositingOperationPlusLighter),
      @"copy"             : @(NSCompositingOperationCopy),
      @"xor"              : @(NSCompositingOperationXOR),
      @"multiply"         : @(NSCompositingOperationMultiply),
      @"screen"           : @(NSCompositingOperationScreen),
      @"overlay"          : @(NSCompositingOperationOverlay),
      @"darken"           : @(NSCompositingOperationDarken),
      @"lighten"          : @(NSCompositingOperationLighten),
      @"color-dodge"      : @(NSCompositingOperationColorDodge),
      @"color-burn"       : @(NSCompositingOperationColorBurn),
      @"hard-light"       : @(NSCompositingOperationHardLight),
      @"soft-light"       : @(NSCompositingOperationSoftLight),
      @"difference"       : @(NSCompositingOperationDifference),
      @"exclusion"        : @(NSCompositingOperationExclusion),
      @"hue"              : @(NSCompositingOperationHue),
      @"saturation"       : @(NSCompositingOperationSaturation),
      @"color"            : @(NSCompositingOperationColor),
      @"luminosity"       : @(NSCompositingOperationLuminosity),
    };

    CompositeOperationNames = createReverseMappedDictionary(CompositeOperations);
  }
  if (LineCapStyles == nil) {
    LineCapStyles = @{
      kCALineCapButt   : @(kCGLineCapButt),
      kCALineCapRound  : @(kCGLineCapRound),
      kCALineCapSquare : @(kCGLineCapSquare),
    };

    LineCapStyleNames = createReverseMappedDictionary(LineCapStyles);
  }
  if (LineJoinStyles == nil) {
    LineJoinStyles = @{
      kCALineJoinMiter : @(kCGLineJoinMiter),
      kCALineJoinRound : @(kCGLineJoinRound),
      kCALineJoinBevel : @(kCGLineJoinBevel),
    };

    LineJoinStyleNames = createReverseMappedDictionary(LineJoinStyles);
  }
  if (TextAlignments == nil) {
    TextAlignments = @[
      kCAAlignmentCenter,
      kCAAlignmentJustified,
      kCAAlignmentLeft,
      kCAAlignmentNatural,
      kCAAlignmentRight,
    ];

    NSTextAlignments = @{
      kCAAlignmentCenter    : @(NSTextAlignmentCenter),
      kCAAlignmentJustified : @(NSTextAlignmentJustified),
      kCAAlignmentLeft      : @(NSTextAlignmentLeft),
      kCAAlignmentNatural   : @(NSTextAlignmentNatural),
      kCAAlignmentRight     : @(NSTextAlignmentRight),
    };
  }
  if (TextBaselines == nil) {
    TextBaselines = @{
      @"top"         : @(TextBaselineTop),
      @"hanging"     : @(TextBaselineHanging),
      @"middle"      : @(TextBaselineMiddle),
      @"alphabetic"  : @(TextBaselineAlphabetic),
      @"ideographic" : @(TextBaselineIdeographic),
      @"bottom"      : @(TextBaselineBottom),
    };

    TextBaselineNames = createReverseMappedDictionary(TextBaselines);
  }
  if (TextDirections == nil) {
    TextDirections = @{
      @"ltr" : @(TextDirectionLtr),
      @"rtl" : @(TextDirectionRtl),
    };

    TextDirectionNames = createReverseMappedDictionary(TextDirections);
  }
  if (WindingRules == nil) {
    WindingRules = @{
      kCAFillRuleEvenOdd : @(NSEvenOddWindingRule),
      kCAFillRuleNonZero : @(NSNonZeroWindingRule),
    };

    WindingRuleNames = createReverseMappedDictionary(WindingRules);
  }
}
@end
