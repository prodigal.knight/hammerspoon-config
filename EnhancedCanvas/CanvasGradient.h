#pragma once

@import Cocoa;
@import Foundation;
@import LuaSkin;

#import "shared.h"

static const char *GRADIENT_USERDATA_TAG = "CanvasGradient";

typedef enum CanvasGradientType : NSUInteger {
  CanvasGradientTypeConic,
  CanvasGradientTypeLinear,
  CanvasGradientTypeRadial,
} CanvasGradientType;

@interface CanvasGradient : NSObject
@property int                         selfRef;
@property CanvasGradientType          type;
@property NSMutableArray<NSColor *>  *colors;
@property NSMutableArray<NSNumber *> *stops;

- (instancetype)init:(CanvasGradientType)type;
- (void)addColorStop:(NSColor *)color at:(NSNumber *)position;
- (instancetype)copy;
@end

@interface ConicCanvasGradient : CanvasGradient
@property NSPoint center;
@property double  angle;

- (instancetype)initWithCenter:(NSPoint)center startAngle:(double)angle;
- (ConicCanvasGradient *)copy;
@end

@interface LinearCanvasGradient : CanvasGradient
@property NSPoint start;
@property NSPoint end;

- (instancetype)initWithStartPoint:(NSPoint)start endPoint:(NSPoint)end;
- (LinearCanvasGradient *)copy;
@end

@interface RadialCanvasGradient : CanvasGradient
@property NSPoint fromCenter;
@property double  fromRadius;
@property NSPoint toCenter;
@property double  toRadius;

- (instancetype)initWithCenter:(NSPoint)fromCenter radius:(double)fromRadius toCenter:(NSPoint)toCenter radius:(double)toRadius;
- (RadialCanvasGradient *)copy;
@end

static NSDictionary<NSNumber *, NSString *> *GradientTypeNames = nil;

@interface GradientConstants : NSObject
@end

@implementation GradientConstants
+ (void)initialize {
  if (GradientTypeNames == nil) {
    GradientTypeNames = @{
      @(CanvasGradientTypeConic)  : @"Conic",
      @(CanvasGradientTypeLinear) : @"Linear",
      @(CanvasGradientTypeRadial) : @"Radial",
    };
  }
}
@end
