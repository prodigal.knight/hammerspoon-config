#import "CanvasGradient.h"

static LSRefTable gradientRefTable = LUA_NOREF;

/******************************************************************************\
 * Canvas Gradient implementations                                            *
\******************************************************************************/
/**
 * Base implementation
 */
@implementation CanvasGradient
- (instancetype)init:(CanvasGradientType)type {
  _selfRef = LUA_NOREF;
  _type    = type;

  _colors  = [NSMutableArray array];
  _stops   = [NSMutableArray array];

  return self;
}

- (void)addColorStop:(NSColor *)color at:(NSNumber *)position {
  [_colors addObject:color];
  [_stops addObject:position];
}

- (instancetype)copy {
  return [[self class] alloc];
}

- (void)copyInto:(CanvasGradient *)obj {
  obj.type = _type;
  obj.colors = [_colors copy];
  obj.stops = [_stops copy];
}
@end

/**
 * Conic gradient implementation
 */
@implementation ConicCanvasGradient
- (instancetype)initWithCenter:(NSPoint)center startAngle:(double)angle {
  self = [super init:CanvasGradientTypeConic];

  if (self) {
    _center = center;
    _angle = angle;
  }

  return self;
}

- (instancetype)copy {
  ConicCanvasGradient *copy = [ConicCanvasGradient alloc];
  [self copyInto:copy];

  copy.center = _center;
  copy.angle = _angle;

  return copy;
}
@end

/**
 * Linear gradient implementation
 */
@implementation LinearCanvasGradient
- (instancetype)initWithStartPoint:(NSPoint)start endPoint:(NSPoint)end {
  self = [super init:CanvasGradientTypeLinear];

  if (self) {
    _start = start;
    _end   = end;
  }

  return self;
}

- (instancetype)copy {
  LinearCanvasGradient *copy = [LinearCanvasGradient alloc];
  [self copyInto:copy];

  copy.start = _start;
  copy.end = _end;

  return copy;
}
@end

/**
 * Radial gradient implementation
 */
@implementation RadialCanvasGradient
- (instancetype)initWithCenter:(NSPoint)fromCenter radius:(double)fromRadius toCenter:(NSPoint)toCenter radius:(double)toRadius {
  self = [super init:CanvasGradientTypeRadial];

  if (self) {
    _fromCenter = fromCenter;
    _fromRadius = fromRadius;
    _toCenter   = toCenter;
    _toRadius   = toRadius;
  }

  return self;
}

- (instancetype)copy {
  RadialCanvasGradient *copy = [RadialCanvasGradient alloc];
  [self copyInto:copy];

  copy.fromCenter = _fromCenter;
  copy.fromRadius = _fromRadius;
  copy.toCenter   = _toCenter;
  copy.toRadius   = _toRadius;

  return copy;
}
@end

/******************************************************************************\
 * CanvasGradient instance methods                                            *
\******************************************************************************/
/// gradient:addColorStop(position, color)
/// Method
/// Add a color stop to this gradient
///
/// Parameters
///  * `position` - The position of the color stop as a real number between 0 and 1
///  * `color`    - The color to use at this stop
///
/// Returns:
///  * None
///
/// Notes:
///  * If the color cannot be converted to an NSColor then it will default to black
static int gradient_addColorStop(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin checkArgs:LS_TUSERDATA, GRADIENT_USERDATA_TAG, LS_TNUMBER, LS_TSTRING, LS_TBREAK];

  CanvasGradient *gradient = [skin luaObjectAtIndex:1 toClass:GRADIENT_USERDATA_TAG];

  NSNumber *position = [skin toNSObjectAtIndex:2];
  NSString *colorString = [skin toNSObjectAtIndex:3];

  NSColor *color = parseColorString(colorString);

  [gradient addColorStop:color at:position];

  return 0;
}

static int gradient_tostring(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  CanvasGradient *gradient = [skin luaObjectAtIndex:1 toClass:GRADIENT_USERDATA_TAG];
  [skin pushNSObject:[NSString stringWithFormat:@"%@ Gradient (%p)", GradientTypeNames[[NSNumber numberWithUnsignedInteger:gradient.type]], lua_topointer(L, 1)]];
  return 1;
}

static int gradient_gc(lua_State *L) {
  // LuaSkin *skin = [LuaSkin sharedWithState:L];
  // CanvasGradient *gradient = get_objectFromUserdata(__bridge_transfer CanvasGradient, L, 1, GRADIENT_USERDATA_TAG);

  // Remove the Metatable so future use of the variable in Lua won't think its valid
  lua_pushnil(L);
  lua_setmetatable(L, 1);
  return 0;
}

/******************************************************************************\
 * CanvasGradient helper methods                                              *
\******************************************************************************/
static int pushCanvasGradient(lua_State *L, id obj) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  CanvasGradient *value = obj;
  if (value.selfRef == LUA_NOREF) {
    void **valuePtr = lua_newuserdata(L, sizeof(CanvasGradient *));
    *valuePtr = (__bridge_retained void *)value;
    luaL_getmetatable(L, GRADIENT_USERDATA_TAG);
    lua_setmetatable(L, -2);
    value.selfRef = [skin luaRef:gradientRefTable];
  }
  [skin pushLuaRef:gradientRefTable ref:value.selfRef];
  return 1;
}

static id toCanvasGradientFromLua(lua_State *L, int idx) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  CanvasGradient *value;
  if (luaL_testudata(L, idx, GRADIENT_USERDATA_TAG)) {
    value = get_objectFromUserdata(__bridge CanvasGradient, L, idx, GRADIENT_USERDATA_TAG);
  } else {
    [skin logError:[NSString stringWithFormat:@"expected %s object, found %s", GRADIENT_USERDATA_TAG, lua_typename(L, lua_type(L, idx))]];
  }
  return value;
}

static const luaL_Reg gradient_metaLib[] = {
  {"addColorStop", gradient_addColorStop},

  {"__tostring",   gradient_tostring},
  {"__gc",         gradient_gc},

  {NULL,           NULL}
};

static luaL_Reg gradient_moduleLib[] = {
  {NULL, NULL}
};

/******************************************************************************\
 * Register library methods                                                   *
\******************************************************************************/
int luaopen_EnhancedCanvas_CanvasGradient(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  // Register "helper object" libraries first
  gradientRefTable = [skin registerLibraryWithObject:GRADIENT_USERDATA_TAG
                                           functions:gradient_moduleLib
                                       metaFunctions:nil // or gradient_moduleMetaLib
                                     objectFunctions:gradient_metaLib];

  registerHelpers(skin, GRADIENT_USERDATA_TAG, pushCanvasGradient, toCanvasGradientFromLua);

  [GradientConstants initialize];

  return 1;
}
