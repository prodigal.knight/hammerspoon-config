#pragma once

@import Cocoa;
@import Foundation;
@import LuaSkin;
@import QuartzCore;

#import "Canvas.h"
#import "CanvasGradient.h"
#import "Path2D.h"
#import "shared.h"

@class EnhancedCanvasView;

// Drawing Instruction (Abstract)
@interface DrawingInstruction : NSObject
- (void)run:(EnhancedCanvasView *)view;
@end

// Drawing instruction interfaces
@interface ClearRectInstruction : DrawingInstruction
@property NSRect rect;

- (instancetype)init:(NSRect)rect;
@end

@interface DrawLayerInstruction : DrawingInstruction
@property CGLayerRef layer;

- (instancetype)init:(CGLayerRef)layer;
@end
