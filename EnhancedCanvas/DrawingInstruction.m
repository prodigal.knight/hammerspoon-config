#import "DrawingInstruction.h"

/******************************************************************************\
 * Drawing instruction implementations                                        *
 *                                                                            *
 * NOTE: These are private, internal-use only objects that compose the actual *
 * drawing operations used at runtime (i.e. when the OS calls the             *
 * EnhancedCanvas' `drawRect` method), and cannot be manually instantiated by *
 * the end user.                                                              *
\******************************************************************************/
// This is a base implementation of DrawingInstruction just to make everything happy at runtime
@implementation DrawingInstruction
- (void)run:(__unused EnhancedCanvasView *)view {
  // Do nothing
}
@end

/**
 * Clear the given rectangle of any previously rendered content
 */
@implementation ClearRectInstruction
- (instancetype)init:(NSRect)rect {
  _rect = rect;

  return self;
}

- (void)run:(EnhancedCanvasView *)view {
  CGContextClearRect(view.ctx, _rect);
}
@end

@implementation DrawLayerInstruction
- (instancetype)init:(CGLayerRef)layer {
  _layer = layer;

  return self;
}

- (void)run:(EnhancedCanvasView *)view {
  CGContextDrawLayerAtPoint(view.ctx, CGPointMake(0, 0), _layer);
}

- (void)dealloc {
  CGLayerRelease(_layer);
}
@end
