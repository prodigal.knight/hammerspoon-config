#pragma once

@import Cocoa;
@import Foundation;
@import LuaSkin;
@import QuartzCore;

#import "shared.h"

static const char *PATH2D_USERDATA_TAG = "Path2D";

@interface Path2D : NSObject
+ (Path2D *)path2d;
+ (Path2D *)rect:(NSRect)rect;


@property int                   selfRef;
@property CGMutablePathRef      path;
@property double                lineWidth;
@property CGLineCap             lineCap;
@property CGLineJoin            lineJoin;
@property double                miterLimit;
@property CAShapeLayerFillRule  fillRule;
@property NSArray<NSNumber *>  *lineDashPattern;
@property double                lineDashOffset;

- (instancetype)init;
- (Path2D *)copy;
- (CGRect)bounds;
- (BOOL)empty;

- (CGPathRef)CGPath;
- (CGPathRef)CGPathWithTransform:(CGAffineTransform)transform;

- (Path2D *)applyTransform:(CGAffineTransform)transform;

// Helper methods for:
// - EnhancedCanvasView:isPointInPath(x, y[, fillRule]) -> boolean
// - EnhancedCanvasView:isPointInPath(path, x, y[, fillRule]) -> boolean
- (BOOL)isPointInPath:(NSPoint)point fillRule:(CAShapeLayerFillRule)rule;
// - EnhancedCanvasView:isPointInStroke(x, y) -> boolean
// - EnhancedCanvasView:isPointInStroke(path, x, y) -> boolean
- (BOOL)isPointInStroke:(NSPoint)point;

// Path2D methods
- (void)addPath:(Path2D *)path;
- (void)addCGPath:(CGPathRef)other;
- (void)arc:(NSPoint)center radius:(double)radius startAngle:(double)startAngle endAngle:(double)endAngle ccw:(BOOL)ccw;
- (void)arcTo:(NSPoint)end from:(NSPoint)start radius:(double)radius;
- (void)closePath;
- (void)bezierCurveTo:(NSPoint)end cp1:(NSPoint)cp1 cp2:(NSPoint)cp2;
- (void)ellipse:(NSPoint)center rx:(double)rx ry:(double)ry rot:(double)rot startAngle:(double)startAngle endAngle:(double)endAngle ccw:(BOOL)ccw;
- (void)lineTo:(NSPoint)point;
- (void)moveTo:(NSPoint)point;
- (void)rect:(NSRect)rect;
- (void)quadraticCurveTo:(NSPoint)end cp:(NSPoint)cp;
@end
