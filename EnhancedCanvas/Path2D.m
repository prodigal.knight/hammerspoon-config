#import "Path2D.h"

static LSRefTable path2DRefTable = LUA_NOREF;

@implementation Path2D
+ (Path2D *)path2d {
  return [[Path2D alloc] init];
}
+ (Path2D *)path2dWithPath:(CGMutablePathRef)path {
  return [[Path2D alloc] initWithPath:path];
}

+ (Path2D *)pathWithCGPath:(CGPathRef)cgPath {
  return [Path2D path2dWithPath:CGPathCreateMutableCopy(cgPath)];
}

+ (Path2D *)rect:(NSRect)rect {
  Path2D *path = [Path2D path2d];
  [path rect:rect];

  return path;
}

- (instancetype)init {
  return [self initWithPath:CGPathCreateMutable()];
}
- (instancetype)initWithPath:(CGMutablePathRef)path {
  self = [super init];

  if (self) {
    _selfRef         = LUA_NOREF;
    _path            = path;
    _lineWidth       = 1.0;
    _lineCap         = kCGLineCapButt;
    _lineJoin        = kCGLineJoinMiter;
    _miterLimit      = 10.0;
    _fillRule        = kCAFillRuleNonZero;
    _lineDashPattern = nil;
    _lineDashOffset  = 0.0;
  }

  return self;
}
- (Path2D *)copy {
  Path2D *path = [Path2D path2dWithPath:CGPathCreateMutableCopy(_path)];

  path.lineWidth       = _lineWidth;
  path.lineCap         = _lineCap;
  path.lineJoin        = _lineJoin;
  path.miterLimit      = _miterLimit;
  path.fillRule        = _fillRule;
  path.lineDashPattern = _lineDashPattern == nil ? nil : [_lineDashPattern copy];
  path.lineDashOffset  = _lineDashOffset;

  return path;
}

- (CGPathRef)CGPath {
  return CGPathCreateCopy(_path);
}
- (CGPathRef)CGPathWithTransform:(CGAffineTransform)transform {
  return CGPathCreateCopyByTransformingPath(_path, &transform);
}

- (CGRect)bounds {
  return CGPathGetPathBoundingBox(_path);
}

- (Path2D *)applyTransform:(CGAffineTransform)transform {
  return [Path2D path2dWithPath:CGPathCreateMutableCopyByTransformingPath(_path, &transform)];
}

- (BOOL)empty {
  return CGPathIsEmpty(_path);
}

/******************************************************************************\
 * JS CanvasRenderingContext2D helper methods                                 *
\******************************************************************************/
- (BOOL)containsPoint:(NSPoint)point {
  return CGPathContainsPoint(_path, NULL, point, _fillRule == kCAFillRuleEvenOdd);
}
- (BOOL)isPointInPath:(NSPoint)point fillRule:(CAShapeLayerFillRule)rule {
  self.fillRule = rule;

  return [self containsPoint:point];
}

- (BOOL)isPointInStroke:(NSPoint)point {
  return [[self toStrokedPathArea] containsPoint:point];
}

- (Path2D *)toStrokedPathArea {
  static NSBitmapImageRep *rep  = nil;

  if (rep == nil) {
    rep = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
                                                  pixelsWide:1
                                                  pixelsHigh:1
                                               bitsPerSample:8
                                             samplesPerPixel:3
                                                    hasAlpha:NO
                                                    isPlanar:NO
                                              colorSpaceName:NSCalibratedRGBColorSpace
                                                 bytesPerRow:4
                                                bitsPerPixel:32];
  }

  NSGraphicsContext *ctx = [NSGraphicsContext graphicsContextWithBitmapImageRep:rep];

  NSAssert(ctx != nil, @"No context for - [Path2D -toStrokedPathArea:]");

  [NSGraphicsContext saveGraphicsState];
  [NSGraphicsContext setCurrentContext:ctx];

  Path2D *path = self;

  CGContextRef context = [ctx CGContext];

  NSAssert(context != nil, @"No context for [Path2D -toStrokedPathArea]");

  if (context) {
    CGPathRef cgp = [self CGPath];
    CGContextBeginPath(context);
    CGContextAddPath(context, cgp);
    CGPathRelease(cgp);
    CGContextSetLineWidth(context, self.lineWidth);
    CGContextSetLineCap(context, self.lineCap);
    CGContextSetLineJoin(context, self.lineJoin);
    CGContextSetMiterLimit(context, self.miterLimit);
    NSUInteger count = [_lineDashPattern count];
    CGFloat *lengths = malloc(sizeof(CGFloat) * count);
    for (NSUInteger i = 0; i < count; i++) {
      lengths[i] = [_lineDashPattern[i] doubleValue];
    }
    CGContextSetLineDash(context, _lineDashOffset, lengths, (size_t) count);
    free(lengths);
    CGContextReplacePathWithStrokedPath(context);
    CGPathRef strokedPath = CGContextCopyPath(context);
    path = [Path2D pathWithCGPath:strokedPath];
    CGPathRelease(strokedPath);
  }

  [NSGraphicsContext restoreGraphicsState];

  return path;
}

/******************************************************************************\
 * JS Path2D helper methods                                                   *
\******************************************************************************/
- (void)addPath:(Path2D *)other {
  CGPathAddPath(_path, NULL, other.path);
}
- (void)addCGPath:(CGPathRef)other {
  CGPathAddPath(_path, NULL, other);
}
- (void)arc:(NSPoint)center radius:(double)radius startAngle:(double)startAngle endAngle:(double)endAngle ccw:(BOOL)ccw {
  CGPathAddArc(_path, NULL, center.x, center.y, radius, startAngle, endAngle, !ccw); // Invert clockwise due to flipped coordinate system
}
- (void)arcTo:(NSPoint)end from:(NSPoint)start radius:(double)radius {
  CGPathAddArcToPoint(_path, NULL, start.x, start.y, end.x, end.y, radius);
}
- (void)bezierCurveTo:(NSPoint)point cp1:(NSPoint)cp1 cp2:(NSPoint)cp2 {
  CGPathAddCurveToPoint(_path, NULL, cp1.x, cp1.y, cp2.x, cp2.y, point.x, point.y);
}
- (void)closePath {
  CGPathCloseSubpath(_path);
}
- (void)ellipse:(NSPoint)center rx:(double)rx ry:(double)ry rot:(double)rot startAngle:(double)startAngle endAngle:(double)endAngle ccw:(BOOL)ccw {
  Path2D *ellipse = [Path2D path2d];
  [ellipse arc:center radius:1.0 startAngle:startAngle endAngle:endAngle ccw:!ccw]; // Invert clockwise due to flipped coordinate system

  CGAffineTransform t = CGAffineTransformMakeScale(rx, ry);
  CGAffineTransformRotate(t, rot);

  [self addPath:[ellipse applyTransform:t]];
}
- (void)lineTo:(NSPoint)point {
  CGPathAddLineToPoint(_path, NULL, point.x, point.y);
}
- (void)moveTo:(NSPoint)point {
  CGPathMoveToPoint(_path, NULL, point.x, point.y);
}
- (void)quadraticCurveTo:(NSPoint)point cp:(NSPoint)cp {
  CGPathAddQuadCurveToPoint(_path, NULL, cp.x, cp.y, point.x, point.y);
}
- (void)rect:(NSRect)rect {
  CGPathAddRect(_path, NULL, rect);
}

- (void)dealloc {
  CGPathRelease(_path);
  _path = NULL;
}
@end

/******************************************************************************\
 * Path2D instance methods                                                    *
\******************************************************************************/
/// path2d:addPath(path) -> void
/// Method
/// Add a path to this path
///
/// Parameters:
///  * `path` - The path to be added
///
/// Returns:
///  * None
static int path2d_addPath(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TBREAK];

  Path2D *path  = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];
  Path2D *other = [skin luaObjectAtIndex:2 toClass:PATH2D_USERDATA_TAG];

  [path addPath:other];

  return 0;
}

/// path2d:arc(x, y, radius, startAngle, endAngle[, counterclockwise]) -> void
/// Method
/// Add an arc centered on x,y with the given radius, starting at the given startAngle and ending at the given endAngle
///
/// Parameters:
///  * `x`                - The center X coordinate
///  * `y`                - The center Y coordinate
///  * `radius`           - The arc's radius
///  * `startAngle`       - The starting angle for this arc, in radians
///  * `endAngle`         - The ending angle for this arc, in radians
///  * `counterclockwise` - [Optional] Indicates whether the arc is drawn in a clockwise direction (default) or a counter-clockwise direction between the start and end angles
///
/// Returns:
///  * None
static int path2d_arc(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *x          = [skin toNSObjectAtIndex:2];
  NSNumber *y          = [skin toNSObjectAtIndex:3];
  NSNumber *radius     = [skin toNSObjectAtIndex:4];
  NSNumber *startAngle = [skin toNSObjectAtIndex:5];
  NSNumber *endAngle   = [skin toNSObjectAtIndex:6];
  BOOL ccw = lua_gettop(L) == 7 ? [(NSNumber *) [skin toNSObjectAtIndex:9] boolValue] : NO;

  NSPoint center = NSMakePoint([x doubleValue], [y doubleValue]);

  [path arc:center radius:[radius doubleValue] startAngle:[startAngle doubleValue] endAngle:[endAngle doubleValue] ccw:ccw];

  return 0;
}

/// path2d:arcTo(x1, y1, x2, y2, radius) -> void
/// Method
/// Add a circular arc from x1,y1 to x2,y2 with the given radius to the current subpath
///
/// Parameters:
///  * `x1`     - The starting X coordinate
///  * `y1`     - The starting Y coordinate
///  * `x2`     - The ending X coordinate
///  * `y2`     - The ending Y coordinate
///  * `radius` - The arc's radius
///
/// Returns:
///  * None
///
/// Notes:
///  * If the given radius is much larger than the radius needed to connect the two points then additional lines will be added to connect the arc to the rest of the path
static int path2d_arcTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *x1     = [skin toNSObjectAtIndex:2];
  NSNumber *y1     = [skin toNSObjectAtIndex:3];
  NSNumber *x2     = [skin toNSObjectAtIndex:4];
  NSNumber *y2     = [skin toNSObjectAtIndex:5];
  NSNumber *radius = [skin toNSObjectAtIndex:6];

  NSPoint cp1 = NSMakePoint([x1 doubleValue], [y1 doubleValue]);
  NSPoint cp2 = NSMakePoint([x2 doubleValue], [y2 doubleValue]);

  [path arcTo:cp2 from:cp1 radius:[radius doubleValue]];

  return 0;
}

/// path2d:bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y) -> void
/// Method
/// Add a cubic bezier curve to the current subpath using the given control points
///
/// Parameters:
///  * `cp1x` - The first control point's X coordinate
///  * `cp1y` - The first control point's Y coordinate
///  * `cp2x` - The second control point's X coordinate
///  * `cp2y` - The second control point's Y coordinate
///  * `x`    - The end point's X coordinate
///  * `y`    - The end point's Y coordinate
///
/// Returns:
///  * None
static int path2d_bezierCurveTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *cpx1 = [skin toNSObjectAtIndex:2];
  NSNumber *cpy1 = [skin toNSObjectAtIndex:3];
  NSNumber *cpx2 = [skin toNSObjectAtIndex:4];
  NSNumber *cpy2 = [skin toNSObjectAtIndex:5];
  NSNumber *x    = [skin toNSObjectAtIndex:6];
  NSNumber *y    = [skin toNSObjectAtIndex:7];

  NSPoint cp1 = NSMakePoint([cpx1 doubleValue], [cpy1 doubleValue]);
  NSPoint cp2 = NSMakePoint([cpx2 doubleValue], [cpy2 doubleValue]);
  NSPoint end = NSMakePoint([x doubleValue], [y doubleValue]);

  [path bezierCurveTo:end cp1:cp1 cp2:cp2];

  return 0;
}

/// path2d:closePath() -> void
/// Method
/// Close the current subpath by adding a straight line from its last point to its first point
///
/// Parameters:
///  * None
///
/// Returns:
///  * None
static int path2d_closePath(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  [path closePath];

  return 0;
}

/// path2d:ellipse(x, y, rx, ry, rotation, startAngle, endAngle) -> void
/// Method
/// Add an elliptical arc to the current subpath
///
/// Parameters:
///  * `x`          - The center X coordinate of the ellipse
///  * `y`          - The center Y coordinate of the ellipse
///  * `rx`         - The X (horizontal) radius of the ellipse
///  * `ry`         - The Y (vertical) radius of the ellipse
///  * `rotation`   - The rotation of the ellipse in radians
///  * `startAngle` - The angle at which the ellipse starts, measured clockwise from the positive x-axis and expressed in radians
///  * `endAngle`   - The angle at which the ellipse ends, measured clockwise from the positive x-axis and expressed in radians
///
/// Returns:
///  * None
static int path2d_ellipse(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *x          = [skin toNSObjectAtIndex:2];
  NSNumber *y          = [skin toNSObjectAtIndex:3];
  NSNumber *rx         = [skin toNSObjectAtIndex:4];
  NSNumber *ry         = [skin toNSObjectAtIndex:5];
  NSNumber *rot        = [skin toNSObjectAtIndex:6];
  NSNumber *startAngle = [skin toNSObjectAtIndex:7];
  NSNumber *endAngle   = [skin toNSObjectAtIndex:8];
  BOOL ccw = lua_gettop(L) == 9 ? [(NSNumber *) [skin toNSObjectAtIndex:9] boolValue] : NO;

  NSPoint center = NSMakePoint([x doubleValue], [y doubleValue]);

  [path ellipse:center rx:[rx doubleValue] ry:[ry doubleValue] rot:[rot doubleValue] startAngle:[startAngle doubleValue] endAngle:[endAngle doubleValue] ccw:ccw];

  return 0;
}

/// path2d:lineTo(x, y) -> void
/// Method
/// Add a straight line to the given coordinates to the current subpath
///
/// Parameters:
///  * `x` - The X coordinate to end at
///  * `y` - The Y coordinate to end at
///
/// Returns:
///  * None
static int path2d_lineTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];

  [path lineTo:NSMakePoint([x doubleValue], [y doubleValue])];

  return 0;
}

/// path2d:moveTo(x, y) -> void
/// Method
/// Start a new subpath on the current path at x,y
///
/// Parameters:
///  * `x` - The X coordinate to move to
///  * `y` - The Y coordinate to move to
///
/// Returns:
///  * None
static int path2d_moveTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];

  [path moveTo:NSMakePoint([x doubleValue], [y doubleValue])];

  return 0;
}

/// path2d:quadraticCurveTo(cpx, cpy, x, y) -> void
/// Method
/// Add a quadratic bezier curve to the current subpath using the given control points
///
/// Parameters:
///  * `cpx` - The control point's X coordinate
///  * `cpy` - The control point's Y coordinate
///  * `x`   - The end point's X coordinate
///  * `y`   - The end point's Y coordinate
///
/// Returns:
///  * None
static int path2d_quadraticCurveTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *cpx = [skin toNSObjectAtIndex:2];
  NSNumber *cpy = [skin toNSObjectAtIndex:3];
  NSNumber *x   = [skin toNSObjectAtIndex:4];
  NSNumber *y   = [skin toNSObjectAtIndex:5];

  NSPoint cp = NSMakePoint([cpx doubleValue], [cpy doubleValue]);
  NSPoint to = NSMakePoint([x doubleValue], [y doubleValue]);

  [path quadraticCurveTo:to cp:cp];

  return 0;
}

/// path2d:rect(x, y, w, h) -> void
/// Method
/// Add a rectangular subpath to the current path
///
/// Parameters:
///  * `x` - The X coordinate of the rectangle's starting point
///  * `y` - The Y coordinate of the rectangle's starting point
///  * `w` - The width of the rectangle
///  * `h` - The height of the rectangle
///
/// Returns:
///  * None
static int path2d_rect(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  Path2D *path = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];
  NSNumber *w = [skin toNSObjectAtIndex:4];
  NSNumber *h = [skin toNSObjectAtIndex:5];

  [path rect:NSMakeRect([x doubleValue], [y doubleValue], [w doubleValue], [h doubleValue])];

  return 0;
}

/******************************************************************************\
 * Path module functions                                                      *
\******************************************************************************/
/// EnhancedCanvas.Path2D.new() -> Path2D
/// Constructor
/// Create a new Path2D object
///
/// Parameters:
///  * None
///
/// Returns:
///  * A new, empty Path2D
static int path2d_new(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TBREAK];

  Path2D *path = [Path2D path2d];

  // [skin pushNSObject:path];
  // NOTE: The above wasn't working so I've inlined important parts of what would be `pushPath2D` here since this is the only function that creates a Path2D that's passed to Lua
  void** valuePtr = lua_newuserdata(L, sizeof(Path2D *));
  *valuePtr = (__bridge_retained void *)path;
  luaL_getmetatable(L, PATH2D_USERDATA_TAG);
  lua_setmetatable(L, -2);
  path.selfRef = [skin luaRef:path2DRefTable];
  [skin pushLuaRef:path2DRefTable ref:path.selfRef];

  return 1;
}

static int path2d_tostring(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  // Path2D *obj = [skin luaObjectAtIndex:1 toClass:PATH2D_USERDATA_TAG];
  [skin pushNSObject:[NSString stringWithFormat:@"%s (%p)", PATH2D_USERDATA_TAG, lua_topointer(L, 1)]];
  return 1;
}

static int path2d_gc(lua_State* L) {
  // LuaSkin *skin = [LuaSkin sharedWithState:L];
  // Path2D *path = get_objectFromUserdata(__bridge_transfer Path2D, L, 1, PATH2D_USERDATA_TAG);

  // Remove the Metatable so future use of the variable in Lua won't think its valid
  lua_pushnil(L);
  lua_setmetatable(L, 1);
  return 0;
}

/******************************************************************************\
 * Path2D helper methods                                                      *
\******************************************************************************/
// These must not throw a lua error to ensure LuaSkin can safely be used from Objective-C
// delegates and blocks.
// static int pushPath2D(lua_State *L, id obj) {
//   LuaSkin *skin = [LuaSkin sharedWithState:L];
//   Path2D *value = obj;
//   if (value.selfRef == LUA_NOREF) {
//     void** valuePtr = lua_newuserdata(L, sizeof(Path2D *));
//     *valuePtr = (__bridge_retained void *)value;
//     luaL_getmetatable(L, PATH2D_USERDATA_TAG);
//     lua_setmetatable(L, -2);
//     value.selfRef = [skin luaRef:path2DRefTable];
//   }
//   [skin pushLuaRef:path2DRefTable ref:value.selfRef];
//   return 1;
// }

static id toPath2DFromLua(lua_State *L, int idx) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  Path2D *value;
  if (luaL_testudata(L, idx, PATH2D_USERDATA_TAG)) {
    value = get_objectFromUserdata(__bridge Path2D, L, idx, PATH2D_USERDATA_TAG);
  } else {
    [skin logError:[NSString stringWithFormat:@"expected %s object, found %s", PATH2D_USERDATA_TAG, lua_typename(L, lua_type(L, idx))]];
  }
  return value;
}

// Implement Path2D API https://developer.mozilla.org/en-US/docs/Web/API/Path2D
static const luaL_Reg path2d_metaLib[] = {
// Public members
  {"addPath",          path2d_addPath},
  {"arc",              path2d_arc},
  {"arcTo",            path2d_arcTo},
  {"bezierCurveTo",    path2d_bezierCurveTo},
  {"closePath",        path2d_closePath},
  {"ellipse",          path2d_ellipse},
  {"lineTo",           path2d_lineTo},
  {"moveTo",           path2d_moveTo},
  {"quadraticCurveTo", path2d_quadraticCurveTo},
  {"rect",             path2d_rect},

  {"__tostring",       path2d_tostring},
  {"__gc",             path2d_gc},

  {NULL,               NULL}
};

// Functions for returned object when module loads
static luaL_Reg path2d_moduleLib[] = {
  {"new", path2d_new},

  {NULL,  NULL}
};

/******************************************************************************\
 * Register library methods                                                   *
\******************************************************************************/
int luaopen_EnhancedCanvas_Path2D(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  // Register canvas library last so its module functions are available to Lua
  path2DRefTable = [skin registerLibraryWithObject:PATH2D_USERDATA_TAG
                                         functions:path2d_moduleLib
                                     metaFunctions:nil    // or path2d_moduleMetaLib
                                   objectFunctions:path2d_metaLib];

  if (![skin registerLuaObjectHelper:toPath2DFromLua forClass:PATH2D_USERDATA_TAG withUserdataMapping:PATH2D_USERDATA_TAG]) {
    [skin logError:[NSString stringWithFormat:@"An error occurred while registering the lua object helper for %s", PATH2D_USERDATA_TAG]];
  }

  return 1;
}
