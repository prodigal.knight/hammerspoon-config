--- === EnhancedCanvas ===
---
--- Yet another different approach to drawing in Hammerspoon
---
--- `hs.canvas` approaches graphical images as a series of graphical primitives being drawn to the canvas.
---
--- EnhancedCanvas follows the JavaScript `CanvasRenderingContext2D` interface and creates a series of drawing instructions as small objects containing the bare minimum amount of information they need to do their job, and are called in the order they were created.
---
--- The `CanvasRenderingContext2D` interface is described on the [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D). This EnhancedCanvas_module attempts to follow that interface as closely as possible but some exceptions have been made in order to better fit Hammerspoon/Lua, and some additional methods have also been added in order to be a better candidate for replacing `hs.canvas` entirely.

local EnhancedCanvas_module = require("EnhancedCanvas.internal")

local canvas = hs.getObjectMetatable('EnhancedCanvasView')

-- Public interface ------------------------------------------------------

EnhancedCanvas_module.compositeTypes  = ls.makeConstantsTable(EnhancedCanvas_module.compositeTypes)
EnhancedCanvas_module.windowBehaviors = ls.makeConstantsTable(EnhancedCanvas_module.windowBehaviors)
EnhancedCanvas_module.windowLevels    = ls.makeConstantsTable(EnhancedCanvas_module.windowLevels)

local _level = canvas.level     -- save raw version
canvas.level = function(self, ...) -- add nice wrapper version
  local arg = table.pack(...)
  local theLevel = _level(self)

  if arg.n ~= 0 then
    if math.type(arg[1]) == "integer" then
      theLevel = arg[1]
    elseif type(arg[1]) == "string" then
      if EnhancedCanvas_module.windowLevels[arg[1]] then
        theLevel = EnhancedCanvas_module.windowLevels[arg[1]]
      else
        return error("unrecognized level specified: "..arg[1])
      end
    else
      return error("integer or string expected, got "..type(arg[1]))
    end
    return _level(self, theLevel)
  else
    return theLevel
  end
end

--- hs.canvas:behavior([behavior]) -> canvasObject | currentValue
--- Method
--- Get or set the window behavior settings for the canvas object using labels defined in [hs.canvas.windowBehaviors](#windowBehaviors).
---
--- Parameters:
---  * `behavior` - if present, the behavior should be a combination of values found in [hs.canvas.windowBehaviors](#windowBehaviors) describing the window behavior.  The behavior should be specified as one of the following:
---    * integer - a number representing the behavior which can be created by combining values found in [hs.canvas.windowBehaviors](#windowBehaviors) with the logical or operator.
---    * string  - a single key from [hs.canvas.windowBehaviors](#windowBehaviors) which will be toggled in the current window behavior.
---    * table   - a list of keys from [hs.canvas.windowBehaviors](#windowBehaviors) which will be combined to make the final behavior by combining their values with the logical or operator.
---
--- Returns:
---  * if an argument is provided, then the canvasObject is returned; otherwise the current behavior value is returned.
local _behavior = canvas.behavior -- save raw version
canvas.behavior = function(self, ...) -- add nice wrapper version
  local arg = table.pack(...)
  local theBehavior = _behavior(self)

  if arg.n ~= 0 then
    if math.type(arg[1]) == "integer" then
      theBehavior = arg[1]
    elseif type(arg[1]) == "string" then
      if EnhancedCanvas_module.windowBehaviors[arg[1]] then
        theBehavior = theBehavior ~ EnhancedCanvas_module.windowBehaviors[arg[1]]
      else
        return error("unrecognized behavior specified: "..arg[1])
      end
    elseif type(arg[1]) == "table" then
      theBehavior = 0
      for _,v in ipairs(arg[1]) do
        if EnhancedCanvas_module.windowBehaviors[v] then
          theBehavior = theBehavior | ((type(v) == "string") and EnhancedCanvas_module.windowBehaviors[v] or v)
        else
          return error("unrecognized behavior specified: "..v)
        end
      end
    else
      return error("integer, string, or table expected, got "..type(arg[1]))
    end
    return _behavior(self, theBehavior)
  else
    return theBehavior
  end
end

--- hs.canvas:behaviorAsLabels(behaviorTable) -> canvasObject | currentValue
--- Method
--- Get or set the window behavior settings for the canvas object using labels defined in [hs.canvas.windowBehaviors](#windowBehaviors).
---
--- Parameters:
---  * behaviorTable - an optional table of strings and/or integers specifying the desired window behavior for the canvas object.
---
--- Returns:
---  * If an argument is provided, the canvas object; otherwise the current value as a table of strings.
canvas.behaviorAsLabels = function(self, ...)
  local args = table.pack(...)

  if args.n == 0 then
    local results = {}
    local behaviorNumber = self:behavior()

    if behaviorNumber ~= 0 then
      for i, v in pairs(EnhancedCanvas_module.windowBehaviors) do
        if type(i) == "string" then
          if (behaviorNumber & v) > 0 then table.insert(results, i) end
        end
      end
    else
      table.insert(results, EnhancedCanvas_module.windowBehaviors[0])
    end
    return setmetatable(results, { __tostring = function(_)
      table.sort(_)
      return "{ "..table.concat(_, ", ").." }"
    end})
  elseif args.n == 1 and type(args[1]) == "table" then
    local newBehavior = 0
    for _,v in ipairs(args[1]) do
      local flag = tonumber(v) or EnhancedCanvas_module.windowBehaviors[v]
      if flag then newBehavior = newBehavior | flag end
    end
    return self:behavior(newBehavior)
  elseif args.n > 1 then
    error("behaviorByLabels method expects 0 or 1 arguments", 2)
  else
    error("behaviorByLabels method argument must be a table", 2)
  end
end

--- hs.canvas:frame([rect]) -> canvasObject | currentValue
--- Method
--- Get or set the frame of the canvasObject.
---
--- Parameters:
---  * rect - An optional rect-table containing the co-ordinates and size the canvas object should be moved and set to
---
--- Returns:
---  * If an argument is provided, the canvas object; otherwise the current value.
---
--- Notes:
---  * a rect-table is a table with key-value pairs specifying the new top-left coordinate on the screen of the canvas (keys `x`  and `y`) and the new size (keys `h` and `w`).  The table may be crafted by any method which includes these keys, including the use of an `hs.geometry` object.
---
---  * elements in the canvas that have the `absolutePosition` attribute set to false will be moved so that their relative position within the canvas remains the same with respect to the new size.
---  * elements in the canvas that have the `absoluteSize` attribute set to false will be resized so that their relative size with respect to the canvas remains the same with respect to the new size.
canvas.frame = function(obj, ...)
  local args = table.pack(...)

  if args.n == 0 then
    return canvas.getFrame(obj)
  elseif args.n == 1 and type(args[1]) == "table" then
    return canvas.setFrame(obj, args[1])
  elseif args.n > 1 then
    error("frame method expects 0 or 1 arguments", 2)
  else
    error("frame method argument must be a table", 2)
  end
end

canvas.getFrame = function(obj)
  local topLeft = obj:topLeft()
  local size    = obj:size()
  return {
    __luaSkinType = "NSRect",
    x = topLeft.x,
    y = topLeft.y,
    h = size.h,
    w = size.w,
  }
end

canvas.setFrame = function(obj, frame)
  obj:size(frame)
  obj:topLeft(frame)
  return obj
end

--- hs.canvas:bringToFront([aboveEverything]) -> canvasObject
--- Method
--- Places the canvas object on top of normal windows
---
--- Parameters:
---  * aboveEverything - An optional boolean value that controls how far to the front the canvas should be placed. Defaults to false.
---    * if true, place the canvas on top of all windows (including the dock and menubar and fullscreen windows).
---    * if false, place the canvas above normal windows, but below the dock, menubar and fullscreen windows.
---
--- Returns:
---  * The canvas object
---
--- Notes:
---  * As of macOS Sierra and later, if you want a `hs.canvas` object to appear above full-screen windows you must hide the Hammerspoon Dock icon first using: `hs.dockicon.hide()`
canvas.bringToFront = function(obj, ...)
  local args = table.pack(...)

  if args.n == 0 then
    return obj:level(EnhancedCanvas_module.windowLevels.floating)
  elseif args.n == 1 and type(args[1]) == "boolean" then
    return obj:level(EnhancedCanvas_module.windowLevels[(args[1] and "screenSaver" or "floating")])
  elseif args.n > 1 then
    error("bringToFront method expects 0 or 1 arguments", 2)
  else
    error("bringToFront method argument must be boolean", 2)
  end
end

--- hs.canvas:sendToBack() -> canvasObject
--- Method
--- Places the canvas object behind normal windows, between the desktop wallpaper and desktop icons
---
--- Parameters:
---  * None
---
--- Returns:
---  * The canvas object
canvas.sendToBack = function(obj, ...)
  local args = table.pack(...)

  if args.n == 0 then
    return obj:level(EnhancedCanvas_module.windowLevels.desktopIcon - 1)
  else
    error("sendToBack method expects 0 arguments", 2)
  end
end

--- hs.canvas:isVisible() -> boolean
--- Method
--- Returns whether or not the canvas is currently showing and is (at least partially) visible on screen.
---
--- Parameters:
---  * None
---
--- Returns:
---  * a boolean indicating whether or not the canvas is currently visible.
---
--- Notes:
---  * This is syntactic sugar for `not hs.canvas:isOccluded()`.
---  * See [hs.canvas:isOccluded](#isOccluded) for more details.
canvas.isVisible = function(obj, ...) return not obj:isOccluded(...) end

canvas.__index = function(self, key)
  if type(key) == 'string' then
    if canvas[key] then
      return canvas[key]
    elseif canvas['_get' .. key] then
      return canvas['_get' .. key](self)
    end
  end
end

canvas.__newindex = function(self, key, value)
  if type(key) == 'string' then
    if canvas[key] then
      rawset(canvas, key, value)
    elseif canvas['_set' .. key] then
      canvas['_set' .. key](self, value)
    end
  end
end

-- Return EnhancedCanvas_module Object --------------------------------------------------

return EnhancedCanvas_module
