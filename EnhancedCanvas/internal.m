#import "Canvas.h"
#import "CanvasGradient.m"
#import "DrawingInstruction.m"
#import "Path2D.m"

static LSRefTable canvasRefTable = LUA_NOREF;

static BOOL defaultCustomSubRole = YES;

static int cg_windowLevels(lua_State *L);

static inline NSRect RectWithFlippedYCoordinate(NSRect theRect) {
  return NSMakeRect(
    theRect.origin.x,
    [[NSScreen screens][0] frame].size.height - theRect.origin.y - theRect.size.height,
    theRect.size.width,
    theRect.size.height
  );
}

static int canvas_gc(lua_State* L);

@implementation HSCanvasWindow
// Apple's stupid API change gave this an enum name (finally) in 10.12, but clang complains about using the underlying
// type directly, which we have to do to maintain Xcode 7 compilability, so to keep Xcode 8 quite... this:
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverriding-method-mismatch"
#pragma clang diagnostic ignored "-Wmismatched-parameter-types"
- (instancetype)initWithContentRect:(NSRect)contentRect
                          styleMask:(NSUInteger)windowStyle
                            backing:(NSBackingStoreType)bufferingType
                              defer:(BOOL)deferCreation
#pragma clang diagnostic pop
{
  if (!(isfinite(contentRect.origin.x) && isfinite(contentRect.origin.y) && isfinite(contentRect.size.height) && isfinite(contentRect.size.width))) {
    [LuaSkin logError:[NSString stringWithFormat:@"%s:coordinates must be finite numbers", CANVAS_USERDATA_TAG]];
    return nil;
  }

  self = [super initWithContentRect:contentRect
                          styleMask:windowStyle
                            backing:bufferingType
                              defer:deferCreation];
  if (self) {
    [self setDelegate:self];

    [self setFrameOrigin:RectWithFlippedYCoordinate(contentRect).origin];

    // Configure the window
    self.releasedWhenClosed = NO;
    self.backgroundColor    = [NSColor clearColor];
    self.opaque             = NO;
    self.hasShadow          = NO;
    self.ignoresMouseEvents = YES;
    self.restorable         = NO;
    self.hidesOnDeactivate  = NO;
    self.animationBehavior  = NSWindowAnimationBehaviorNone;
    self.level              = NSScreenSaverWindowLevel;
    _subroleOverride        = nil;
  }
  return self;
}

- (NSString *)accessibilitySubrole {
  NSString *defaultSubrole = [super accessibilitySubrole];
  NSString *customSubrole  = [defaultSubrole stringByAppendingString:@".Hammerspoon"];

  if (_subroleOverride) {
    if ([_subroleOverride isEqualToString:@""]) {
      return defaultCustomSubRole ? defaultSubrole : customSubrole;
    } else {
      return _subroleOverride;
    }
  } else {
    return defaultCustomSubRole ? customSubrole : defaultSubrole;
  }
}

- (BOOL)canBecomeKeyWindow { return NO; }

- (BOOL)windowShouldClose:(id __unused)sender {
  return NO;
}
@end

/**
 * Track various canvas variables through the process of adding instructions
 */
@implementation EnhancedCanvasState
- (instancetype)init {
  self = [super init];

  if (self) {
    NSColor *black = [NSColor blackColor];

    _direction             = TextDirectionLtr;
    _fillColor             = black;
    _fillGradient          = [CanvasGradient alloc];
    _font                  = [NSFont systemFontOfSize:NSFont.systemFontSize];
    _globalAlpha           = 1.0;
    _imageSmoothingEnabled = YES;
    _lineCap               = kCGLineCapButt;
    _lineDashPattern       = nil;
    _lineDashOffset        = 0.0;
    _lineJoin              = kCGLineJoinMiter;
    _lineWidth             = 1.0;
    _miterLimit            = 10.0;
    _shadowBlur            = 0.0;
    _shadowColor           = [NSColor clearColor];
    _shadowOffset          = NSMakeSize(0.0, 0.0);
    _strokeColor           = black;
    _strokeGradient        = [CanvasGradient alloc];
    _textAlign             = kCAAlignmentLeft;

    _baselineOffset        = computeTextBaselineOffsetForFont(_font, TextBaselineAlphabetic);

    __clip                     = nil;
    __fillColorStr             = @"#000";
    __fontStr                  = [NSString stringWithFormat:@"%dpx system", (int) _font.pointSize];
    __globalCompositeOperation = NSCompositingOperationSourceOver;
    __shadowColorStr           = @"transparent";
    __strokeColorStr           = @"#000";
    __textBaseline             = TextBaselineAlphabetic;
    __transform                = CGAffineTransformIdentity;

    __useGradientFill   = NO;
    __useGradientStroke = NO;
  }

  return self;
}

- (EnhancedCanvasState *)copy {
  EnhancedCanvasState *res = [EnhancedCanvasState alloc];

  res.baselineOffset        = _baselineOffset;
  res.direction             = _direction;
  res.fillColor             = [_fillColor copy];
  res.fillGradient          = [_fillGradient copy];
  res.font                  = [_font copy];
  res.globalAlpha           = _globalAlpha;
  res.imageSmoothingEnabled = _imageSmoothingEnabled;
  res.lineCap               = _lineCap;
  res.lineDashPattern       = _lineDashPattern;
  res.lineDashOffset        = _lineDashOffset;
  res.lineJoin              = _lineJoin;
  res.lineWidth             = _lineWidth;
  res.miterLimit            = _miterLimit;
  res.shadowBlur            = _shadowBlur;
  res.shadowColor           = [_shadowColor copy];
  res.shadowOffset          = _shadowOffset;
  res.strokeColor           = [_strokeColor copy];
  res.strokeGradient        = [_strokeGradient copy];
  res.textAlign             = _textAlign;

  res._clip                     = __clip ? [__clip copy] : nil;
  res._fillColorStr             = __fillColorStr;
  res._fontStr                  = __fontStr;
  res._globalCompositeOperation = __globalCompositeOperation;
  res._shadowColorStr           = __shadowColorStr;
  res._strokeColorStr           = __strokeColorStr;
  res._textBaseline             = __textBaseline;
  res._transform                = __transform;
  res._useGradientFill          = __useGradientFill;
  res._useGradientStroke        = __useGradientStroke;

  return res;
}
@end

@implementation EnhancedCanvasView
- (instancetype)initWithFrame:(NSRect)frameRect {
  self = [super initWithFrame:frameRect];
  if (self) {
    _selfRef       = LUA_NOREF;
    _wrapperWindow = nil;

    _instructions  = [NSMutableArray array];

    _path          = nil;
    _clear         = NO;

    _state         = [[EnhancedCanvasState alloc] init];
    __states       = [NSMutableArray array];
  }
  return self;
}

- (BOOL)canDrawConcurrently { return YES; }
- (BOOL)isFlipped { return YES; }
- (BOOL)canBecomeKeyView { return NO; }
- (BOOL)wantsLayer { return YES; }

- (void)viewDidMoveToSuperview {
  if (!self.superview && self.wrapperWindow) { // stick us back into our wrapper window if we've been released from another canvas
    self.frame = self.wrapperWindow.contentView.bounds;
    self.wrapperWindow.contentView = self;
  }
}

- (BOOL)isParentWindow {
  return self.window && [self.window.contentView isEqualTo:self];
}

- (void)save {
  EnhancedCanvasState *next = [_state copy];
  [__states addObject:_state];
  _state = next;
}

- (void)restore {
  NSUInteger l = [__states count];

  if (l == 0) {
    [LuaSkin logWarn:@"No more states to pop"];
    return;
  }

  EnhancedCanvasState *prev = __states[l - 1];
  _state = prev;
  [__states removeLastObject];
}

- (void)addInstruction:(DrawingInstruction *)instruction {
  [_instructions addObject:instruction];
  [self.layer setNeedsDisplay];
}

- (CALayer *)makeBackingLayer {
  CALayer *layer = [CALayer layer];

  layer.drawsAsynchronously = YES;
  layer.frame = self.bounds;

  [layer layoutIfNeeded];

  return layer;
}

- (void)addLayer:(CALayer *)layer {
  if (_clear) {
    [(NSArray<CALayer *> *)[self.layer.sublayers copy] makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    _clear = NO;
  }
  [self.layer addSublayer:layer];
}

- (void)drawRect:(NSRect)rect {
  _path = nil;
  _ctx  = [[NSGraphicsContext currentContext] CGContext];

  for (DrawingInstruction *instruction in _instructions) {
    [instruction run:self];
  }

  _state = [[EnhancedCanvasState alloc] init];
  [_instructions removeAllObjects];
  [__states removeAllObjects];
  _clear = YES;
}

- (NSImage *)getImageDataInRect:(NSRect)rect {
  @autoreleasepool {
    NSBitmapImageRep *bir = [self bitmapImageRepForCachingDisplayInRect:rect];
    [bir setSize:rect.size];
    [self cacheDisplayInRect:rect toBitmapImageRep:bir];

    NSImage *image = [[NSImage alloc] initWithSize:rect.size];
    [image addRepresentation:bir];

    return image;
  }
}
@end

@implementation SmartTextLayer
- (instancetype)init {
  self = [super init];

  if (self) {
    _baselineOffset = 0.0;
    _maxWidth = CGFLOAT_MAX;
  }

  return self;
}

- (void)drawInContext:(CGContextRef)context {
  CGContextSaveGState(context);
  CGContextTranslateCTM(context, 0, _baselineOffset);
  [super drawInContext:context];
  CGContextRestoreGState(context);
}
@end

/******************************************************************************\
 * EnhancedCanvasView instance methods                                        *
\******************************************************************************/
/// canvas:arc(x, y, radius, startAngle, endAngle[, counterclockwise]) -> void
/// Method
/// Add an arc centered on x,y with the given radius, starting at the given startAngle and ending at the given endAngle
///
/// Parameters:
///  * `x`                - The center X coordinate
///  * `y`                - The center Y coordinate
///  * `radius`           - The arc's radius
///  * `startAngle`       - The starting angle for this arc, in radians
///  * `endAngle`         - The ending angle for this arc, in radians
///  * `counterclockwise` - [Optional] Indicates whether the arc is drawn in a clockwise direction (default) or a counter-clockwise direction between the start and end angles
///
/// Returns:
///  * None
static int canvas_arc(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x          = [skin toNSObjectAtIndex:2];
  NSNumber *y          = [skin toNSObjectAtIndex:3];
  NSNumber *radius     = [skin toNSObjectAtIndex:4];
  NSNumber *startAngle = [skin toNSObjectAtIndex:5];
  NSNumber *endAngle   = [skin toNSObjectAtIndex:6];
  BOOL ccw = lua_gettop(L) == 7 ? [(NSNumber *) [skin toNSObjectAtIndex:9] boolValue] : NO;

  NSPoint center = NSMakePoint([x doubleValue], [y doubleValue]);

  [view.path arc:center radius:[radius doubleValue] startAngle:[startAngle doubleValue] endAngle:[endAngle doubleValue] ccw:ccw];

  return 0;
}

/// canvas:arcTo(x1, y1, x2, y2, radius) -> void
/// Method
/// Add a circular arc from x1,y1 to x2,y2 with the given radius to the current subpath
///
/// Parameters:
///  * `x1`     - The starting X coordinate
///  * `y1`     - The starting Y coordinate
///  * `x2`     - The ending X coordinate
///  * `y2`     - The ending Y coordinate
///  * `radius` - The arc's radius
///
/// Returns:
///  * None
///
/// Notes:
///  * If the given radius is much larger than the radius needed to connect the two points then additional lines will be added to connect the arc to the rest of the path
static int canvas_arcTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x1     = [skin toNSObjectAtIndex:2];
  NSNumber *y1     = [skin toNSObjectAtIndex:3];
  NSNumber *x2     = [skin toNSObjectAtIndex:4];
  NSNumber *y2     = [skin toNSObjectAtIndex:5];
  NSNumber *radius = [skin toNSObjectAtIndex:6];

  NSPoint from = NSMakePoint([x1 doubleValue], [y1 doubleValue]);
  NSPoint to   = NSMakePoint([x2 doubleValue], [y2 doubleValue]);

  [view.path arcTo:to from:from radius:[radius doubleValue]];

  return 0;
}

/// canvas:beginPath() -> void
/// Method
/// Begin a new path
///
/// Parameters:
///  * None
///
/// Returns:
///  * None
static int canvas_beginPath(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  view.path = [Path2D path2d];

  return 0;
}

/// canvas:bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y) -> void
/// Method
/// Add a cubic bezier curve to the current subpath using the given control points
///
/// Parameters:
///  * `cp1x` - The first control point's X coordinate
///  * `cp1y` - The first control point's Y coordinate
///  * `cp2x` - The second control point's X coordinate
///  * `cp2y` - The second control point's Y coordinate
///  * `x`    - The end point's X coordinate
///  * `y`    - The end point's Y coordinate
///
/// Returns:
///  * None
static int canvas_bezierCurveTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *cpx1 = [skin toNSObjectAtIndex:2];
  NSNumber *cpy1 = [skin toNSObjectAtIndex:3];
  NSNumber *cpx2 = [skin toNSObjectAtIndex:4];
  NSNumber *cpy2 = [skin toNSObjectAtIndex:5];
  NSNumber *x    = [skin toNSObjectAtIndex:6];
  NSNumber *y    = [skin toNSObjectAtIndex:7];

  NSPoint cp1 = NSMakePoint([cpx1 doubleValue], [cpy1 doubleValue]);
  NSPoint cp2 = NSMakePoint([cpx2 doubleValue], [cpy2 doubleValue]);
  NSPoint end = NSMakePoint([x doubleValue], [y doubleValue]);

  [view.path bezierCurveTo:end cp1:cp1 cp2:cp2];

  return 0;
}

/// canvas:clearRect(x, y, w, h) -> void
/// Method
/// Clear the given area of the canvas of all previously rendered content
///
/// Parameters:
///  * `x` - The left of the rectangle
///  * `y` - The top of the rectangle
///  * `w` - The width of the rectangle
///  * `h` - The height of the rectangle
///
/// Returns:
///  * None
static int canvas_clearRect(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];
  NSNumber *w = [skin toNSObjectAtIndex:4];
  NSNumber *h = [skin toNSObjectAtIndex:5];

  CGRect rect = CGRectMake([x doubleValue], [y doubleValue], [w doubleValue], [h doubleValue]);

  if (!CGRectIsEmpty(rect)) {
    [view addInstruction:[[ClearRectInstruction alloc] init:rect]];
  }

  return 0;
}

/// canvas:clip([fillRule]) -> void
/// canvas:clip(path, [fillRule]) -> void
/// Method
/// Turn the current or given path into the current clipping region
///
/// Parameters:
///  * `path`     - [Optional] The path to use instead of the current path
///  * `fillRule` - [Optional] The name of the winding rule to use ("nonzero" or "evenodd")
///
/// Returns:
///  * None
static int canvas_clip(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  Path2D *path = view.path;
  NSString *fillRuleName = nil;

  int args = lua_gettop(L);
  if (args == 1) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];
  } else if (args == 2) {
    if (lua_type(L, 2) == LUA_TSTRING) {
      [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

      fillRuleName = [skin toNSObjectAtIndex:2];
    } else {
      [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TBREAK];

      path = [skin luaObjectAtIndex:2 toClass:PATH2D_USERDATA_TAG];
    }
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

    path = [skin luaObjectAtIndex:2 toClass:PATH2D_USERDATA_TAG];
    fillRuleName = [skin toNSObjectAtIndex:3];
  }

  if (!path.empty) {
    Path2D *copy = [path applyTransform:view.state._transform];
    [copy closePath];
    if (view.state._clip) {
      [copy addPath:view.state._clip];
    }
    copy.fillRule = (fillRuleName != nil ? [WindingRules[fillRuleName] unsignedIntValue] : NSBezierPath.defaultWindingRule) == NSNonZeroWindingRule ? kCAFillRuleNonZero : kCAFillRuleEvenOdd;
    view.state._clip = copy;
  } else {
    view.state._clip = NULL;
  }

  return 0;
}

/// canvas:closePath() -> void
/// Method
/// Close the current subpath by adding a straight line from its last point to its first point
///
/// Parameters:
///  * None
///
/// Returns:
///  * None
static int canvas_closePath(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [view.path closePath];

  return 0;
}

/// canvas:createConicGradient(startAngle, x, y) -> CanvasGradient
/// Method
/// Create a conic gradient around the given coordinates
///
/// Parameters:
///  * `startAngle` - The angle at which to start the gradient
///  * `x`          - The center X coordinate
///  * `y`          - The center Y coordinate
///
/// Returns:
///  * A CanvasGradient object to add color stops to, which can be used as `fillStyle` or `strokeStyle` later on
static int canvas_createConicGradient(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin logWarn:@"Conic gradients are not fully implemented at this time"];

  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  NSNumber *angle = [skin toNSObjectAtIndex:2];
  NSNumber *x     = [skin toNSObjectAtIndex:3];
  NSNumber *y     = [skin toNSObjectAtIndex:4];

  NSPoint center = NSMakePoint([x doubleValue], [y doubleValue]);

  ConicCanvasGradient *gradient = [[ConicCanvasGradient alloc] initWithCenter:center startAngle:[angle doubleValue]];

  [skin pushNSObject:gradient];

  return 1;
}

/// canvas:createImageData(width, height) -> ImageData
/// canvas:createImageData(imagedata) -> ImageData
/// Method
/// Create image data with the given size (or the same size as the given image data)
///
/// Parameters:
///  * `width`     - The width for the new ImageData object. A negative value inverts along the vertical axis
///  * `height`    - The height for the new ImageData object. A negative value inverts along the horizontal axis
///  * `imagedata` - An existing ImageData object from which to copy the width and height. The image itself is *not* copied.
///
/// Returns:
///  * An ImageData with the specified width and height, filled with transparent black pixels
static int canvas_createImageData(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"createImageData is not implemented at this time"]];
  return 0;
}

/// canvas:createLinearGradient(x0, y0, x1, y1) -> CanvasGradient
/// Method
/// Create a linear gradient starting at x0,y0 going to x1,y1
///
/// Parameters:
///  * `x0` - The starting X coordinate
///  * `y0` - The starting Y coordinate
///  * `x1` - The ending X coordinate
///  * `y1` - The ending Y coordinate
///
/// Returns:
///  * A CanvasGradient object to add color stops to, which can be used as `fillStyle` or `strokeStyle` later on
///
/// Notes:
///  * If the containing area is larger than necessary the beginning and ending colors of the gradient will extend to fill the available space
static int canvas_createLinearGradient(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  NSNumber *x0 = [skin toNSObjectAtIndex:2];
  NSNumber *y0 = [skin toNSObjectAtIndex:3];
  NSNumber *x1 = [skin toNSObjectAtIndex:4];
  NSNumber *y1 = [skin toNSObjectAtIndex:5];

  NSPoint start = NSMakePoint([x0 doubleValue], [y0 doubleValue]);
  NSPoint end   = NSMakePoint([x1 doubleValue], [y1 doubleValue]);

  LinearCanvasGradient *gradient = [[LinearCanvasGradient alloc] initWithStartPoint:start endPoint:end];

  [skin pushNSObject:gradient];

  return 1;
}

/// canvas:createPattern(image, repetition) -> CanvasPattern
/// Method
/// Create a pattern using the specified image and repetition
///
/// Parameters:
///  * `image`      - A source image to be used for repetition
///  * `repetition` - A string describing possible repetitions. Valid values are "repepat", "repeat-x", "repeat-y", and "no-repeat".
///
/// Returns:
///  * An opaque CanvasPattern object that describes the repetition, which can be used as `fillStyle` or `strokeStyle` later on
///
/// Notes:
///  * If `repetition` is specified as an empty string or `nil` then it will default to `repeat`
static int canvas_createPattern(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"createPattern is not implemented at this time"]];
  return 0;
}

/// canvas:createRadialGradient(x0, y0, r0, x1, y1, r1) -> CanvasGradient
/// Method
/// Create a radial gradient centered at x0,y0 with radius r0 extending outward to the circle bounded by x1,y1 with radius r1
///
/// Parameters:
///  * `x0` - The center X coordinate
///  * `y0` - The center Y coordinate
///  * `r0` - The center's radius
///  * `x1` - The bounding circle's center X coordinate
///  * `y1` - The bounding circle's center Y coordinate
///  * `r1` - The bounding circle's radius
///
/// Returns:
///  * A CanvasGradient object to add color stops to, which can be used as `fillStyle` or `strokeStyle` later on
static int canvas_createRadialGradient(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  NSNumber *x0 = [skin toNSObjectAtIndex:2];
  NSNumber *y0 = [skin toNSObjectAtIndex:3];
  NSNumber *r0 = [skin toNSObjectAtIndex:4];
  NSNumber *x1 = [skin toNSObjectAtIndex:5];
  NSNumber *y1 = [skin toNSObjectAtIndex:6];
  NSNumber *r1 = [skin toNSObjectAtIndex:7];

  NSPoint fromCenter = NSMakePoint([x0 doubleValue], [y0 doubleValue]);
  NSPoint toCenter   = NSMakePoint([x1 doubleValue], [y1 doubleValue]);

  RadialCanvasGradient *gradient = [[RadialCanvasGradient alloc] initWithCenter:fromCenter radius:[r0 doubleValue] toCenter:toCenter radius:[r1 doubleValue]];

  [skin pushNSObject:gradient];

  return 1;
}

// NOTE: Intentionally skipping for now. Not sure about the usefulness in this context.
static int canvas_drawFocusIfNeeded(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"drawFocusIfNeeded is not implemented at this time"]];
  return 0;
}

static CALayer *createImageLayer(EnhancedCanvasState *state, NSImage *image, CGRect dr, CGRect sr) {
  CALayer *l = [CALayer layer];

  l.contents = image;
  l.contentsRect = sr;
  l.frame = dr;
  l.opacity = (float)state.globalAlpha;

  Path2D *clip = state._clip;
  if (clip) {
    CAShapeLayer *cl = [CAShapeLayer layer];
    CGPathRef clippingPath = [clip CGPath];
    cl.path = clippingPath;
    CGPathRelease(clippingPath);
    cl.fillColor = [[NSColor whiteColor] CGColor];
    cl.frame = [l bounds];
    cl.fillRule = clip.fillRule;

    l.mask = cl;
  }

  return l;
}

/// canvas:drawImage(image, dx, dy) -> void
/// canvas:drawImage(image, dx, dy, dw, dh) -> void
/// canvas:drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh) -> void
/// Method
/// Create image data with the given size (or the same size as the given image data)
///
/// Parameters:
///  * `image` - The source image to draw into the current context
///  * `sx`    - [Optional] The source X coordinate
///  * `sy`    - [Optional] The source Y coordinate
///  * `sw`    - [Optional] The source width
///  * `sh`    - [Optional] The source height
///  * `dx`    - The destination X coordinate
///  * `dy`    - The destination Y coordinate
///  * `dw`    - The destination width
///  * `dh`    - The destination height
///
/// Returns:
///  * None
static int canvas_drawImage(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSImage *image = [skin luaObjectAtIndex:2 toClass:"NSImage"];

  NSNumber *dx;
  NSNumber *dy;
  NSNumber *dw = @(image.size.width);
  NSNumber *dh = @(image.size.height);
  NSNumber *sx = @0.0;
  NSNumber *sy = @0.0;
  NSNumber *sw = @(image.size.width);
  NSNumber *sh = @(image.size.height);

  int args = lua_gettop(L);
  if (args == 4) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, "hs.image", LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

    dx = [skin toNSObjectAtIndex:3];
    dy = [skin toNSObjectAtIndex:4];
  } else if (args == 6) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, "hs.image", LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

    dx = [skin toNSObjectAtIndex:3];
    dy = [skin toNSObjectAtIndex:4];
    dw = [skin toNSObjectAtIndex:5];
    dh = [skin toNSObjectAtIndex:6];
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, "hs.image", LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

    sx = [skin toNSObjectAtIndex:3];
    sy = [skin toNSObjectAtIndex:4];
    sw = [skin toNSObjectAtIndex:5];
    sh = [skin toNSObjectAtIndex:6];
    dx = [skin toNSObjectAtIndex:7];
    dy = [skin toNSObjectAtIndex:8];
    dw = [skin toNSObjectAtIndex:9];
    dh = [skin toNSObjectAtIndex:10];
  }

  CGRect dr = CGRectApplyAffineTransform(CGRectMake([dx doubleValue], [dy doubleValue], [dw doubleValue], [dh doubleValue]), view.state._transform);
  CGRect sr = CGRectMake([sx doubleValue] / image.size.width, [sy doubleValue] / image.size.height, [sw doubleValue] / image.size.width, [sh doubleValue] / image.size.height);

  if (!CGRectIsEmpty(dr) && !CGRectIsEmpty(sr)) {
    CALayer *l = createImageLayer(view.state, image, dr, sr);
    l.shadowOpacity = (float)view.state.shadowColor.alphaComponent;
    l.shadowRadius = view.state.shadowBlur;
    l.shadowOffset = view.state.shadowOffset;
    l.shadowColor = [view.state.shadowColor CGColor];

    [view addLayer:l];
  }

  return 0;
}

/// canvas:ellipse(x, y, rx, ry, rotation, startAngle, endAngle) -> void
/// Method
/// Add an elliptical arc to the current subpath
///
/// Parameters:
///  * `x`          - The center X coordinate of the ellipse
///  * `y`          - The center Y coordinate of the ellipse
///  * `rx`         - The X (horizontal) radius of the ellipse
///  * `ry`         - The Y (vertical) radius of the ellipse
///  * `rotation`   - The rotation of the ellipse in radians
///  * `startAngle` - The angle at which the ellipse starts, measured clockwise from the positive x-axis and expressed in radians
///  * `endAngle`   - The angle at which the ellipse ends, measured clockwise from the positive x-axis and expressed in radians
///
/// Returns:
///  * None
static int canvas_ellipse(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x          = [skin toNSObjectAtIndex:2];
  NSNumber *y          = [skin toNSObjectAtIndex:3];
  NSNumber *rx         = [skin toNSObjectAtIndex:4];
  NSNumber *ry         = [skin toNSObjectAtIndex:5];
  NSNumber *rot        = [skin toNSObjectAtIndex:6];
  NSNumber *startAngle = [skin toNSObjectAtIndex:7];
  NSNumber *endAngle   = [skin toNSObjectAtIndex:8];
  BOOL ccw = lua_gettop(L) == 9 ? [(NSNumber *) [skin toNSObjectAtIndex:9] boolValue] : NO;

  NSPoint center = NSMakePoint([x doubleValue], [y doubleValue]);

  [view.path ellipse:center rx:[rx doubleValue] ry:[ry doubleValue] rot:[rot doubleValue] startAngle:[startAngle doubleValue] endAngle:[endAngle doubleValue] ccw:ccw];

  return 0;
}

static void fillGradient(EnhancedCanvasView *view, CALayer *mask, CanvasGradient *gradient) {
  CAGradientLayer *gl = [CAGradientLayer layer];
  [gl setAffineTransform:[mask affineTransform]];
  gl.frame = [mask bounds];
  NSUInteger count = [gradient.colors count];
  NSMutableArray<id> *colors = [NSMutableArray arrayWithCapacity:count];
  for (NSUInteger i = 0; i < count; i++) {
    colors[i] = (id)[gradient.colors[i] CGColor];
  }
  gl.colors = colors;
  gl.locations = gradient.stops;
  switch (gradient.type) {
    case CanvasGradientTypeConic:
      if (@available(macOS 10.14, *)) {
        gl.type = kCAGradientLayerConic;
      } else {
        [LuaSkin logWarn:@"Conic gradients are not available until macOS 10.14"];
      }
      gl.startPoint = ((ConicCanvasGradient *) gradient).center;
      gl.endPoint = CGPointMake(((ConicCanvasGradient *) gradient).center.x, 1.0);
      [gl setAffineTransform:CGAffineTransformRotate([gl affineTransform], ((ConicCanvasGradient *) gradient).angle)];
      break;
    case CanvasGradientTypeLinear:
      gl.type = kCAGradientLayerAxial;
      gl.startPoint = ((LinearCanvasGradient *) gradient).start;
      gl.endPoint = ((LinearCanvasGradient *) gradient).end;
      break;
    case CanvasGradientTypeRadial:
      gl.type = kCAGradientLayerRadial;
      gl.startPoint = ((RadialCanvasGradient *) gradient).fromCenter;
      gl.endPoint = ((RadialCanvasGradient *) gradient).toCenter;
      // TODO: Add support for gradient radii
      break;
  }
  gl.mask = mask;
  gl.opacity = (float) view.state.globalAlpha;
  gl.shadowOpacity = (float)view.state.shadowColor.alphaComponent;
  gl.shadowRadius = view.state.shadowBlur;
  gl.shadowOffset = view.state.shadowOffset;
  gl.shadowColor = [view.state.shadowColor CGColor];

  [view addLayer:gl];
}

/**
 * Helper method for filling paths
 */
static void fill(EnhancedCanvasView *view, Path2D *path, NSString *fillRuleName) {
  if (path.empty) {
    return;
  }

  CAShapeLayer *sl = [CAShapeLayer layer];

  CGPathRef shapePath = [path CGPathWithTransform:view.state._transform];
  sl.path = shapePath;
  CGPathRelease(shapePath);
  sl.frame = view.bounds;

  NSWindingRule fillRule = fillRuleName != nil ? [WindingRules[fillRuleName] unsignedIntValue] : NSBezierPath.defaultWindingRule;
  sl.fillRule = fillRule == NSNonZeroWindingRule ? kCAFillRuleNonZero : kCAFillRuleEvenOdd;

  Path2D *clip = view.state._clip;
  if (clip) {
    CAShapeLayer *cl = [CAShapeLayer layer];
    CGPathRef clippingPath = [clip CGPath];
    cl.path = clippingPath;
    CGPathRelease(clippingPath);
    cl.fillColor = [[NSColor whiteColor] CGColor];
    cl.frame = [sl bounds];
    cl.fillRule = clip.fillRule;

    sl.mask = cl;
  }

  if (view.state._useGradientFill) {
    sl.fillColor = [[NSColor whiteColor] CGColor];
    sl.strokeColor = nil;
    fillGradient(view, sl, view.state.fillGradient);
  } else {
    sl.opacity = (float)view.state.globalAlpha;
    sl.fillColor = [view.state.fillColor CGColor];
    sl.strokeColor = nil;
    sl.shadowOpacity = (float)view.state.shadowColor.alphaComponent;
    sl.shadowRadius = view.state.shadowBlur;
    sl.shadowOffset = view.state.shadowOffset;
    sl.shadowColor = [view.state.shadowColor CGColor];

    [view addLayer:sl];
  }
}

/// canvas:fill([fillRule]) -> void
/// canvas:fill(path, [fillRule]) -> void
/// Method
/// Fill the current or given path with the current `fillStyle`
///
/// Parameters:
///  * `path`     - [Optional] The path to use instead of the current path
///  * `fillRule` - [Optional] The name of the winding rule to use ("nonzero" [default] or "evenodd")
///
/// Returns:
///  * None
static int canvas_fill(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  Path2D *path = view.path;
  NSString *fillRuleName = nil;

  int args = lua_gettop(L);
  if (args == 1) { // First form: no arguments
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];
  } else if (args == 2) { // Second form: fill rule provided
    if (lua_type(L, 2) == LUA_TSTRING) {
      [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

      fillRuleName = [skin toNSObjectAtIndex:2];
    } else { // Third form: path provided
      [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TBREAK];

      path = [skin luaObjectAtIndex:2 toClass:PATH2D_USERDATA_TAG];
    }
  } else { // Final form: path and fill rule provided
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

    path = [skin luaObjectAtIndex:2 toClass:PATH2D_USERDATA_TAG];
    fillRuleName = [skin toNSObjectAtIndex:3];
  }

  fill(view, [path copy], fillRuleName);

  return 0;
}

/// canvas:fillRect(x, y, w, h) -> void
/// Method
/// Fill the rectangle bounded by x,y, x+w,x+h with the current `fillStyle`. This does not affect the current subpath.
///
/// Parameters:
///  * `x` - The X coordinate of the rectangle's starting point
///  * `y` - The Y coordinate of the rectangle's starting point
///  * `w` - The width of the rectangle
///  * `h` - The height of the rectangle
///
/// Returns:
///  * None
static int canvas_fillRect(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];
  NSNumber *w = [skin toNSObjectAtIndex:4];
  NSNumber *h = [skin toNSObjectAtIndex:5];

  fill(view, [Path2D rect:NSMakeRect([x doubleValue], [y doubleValue], [w doubleValue], [h doubleValue])], nil);
  return 0;
}

static void renderText(lua_State *L, BOOL stroke) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *text = [skin toNSObjectAtIndex:2];
  if (text.length == 0) {
    return; // Don't bother
  }

  NSNumber *x = [skin toNSObjectAtIndex:3];
  NSNumber *y = [skin toNSObjectAtIndex:4];
  NSNumber *w = lua_gettop(L) == 5 ? [skin toNSObjectAtIndex:5] : nil;

  SmartTextLayer *tl = [SmartTextLayer layer];

  BOOL useGradient = stroke ? view.state._useGradientStroke : view.state._useGradientFill;

  NSFont *font = view.state.font;

  NSDictionary *attributes = stroke
    ? @{
      NSFontAttributeName        : font,
      NSStrokeColorAttributeName : (id)[(useGradient ? [NSColor whiteColor] : view.state.strokeColor) CGColor],
      NSStrokeWidthAttributeName : @(view.state.lineWidth),
    }
    : @{
      NSFontAttributeName            : font,
      NSForegroundColorAttributeName : (id)[(useGradient ? [NSColor whiteColor] : view.state.fillColor) CGColor],
    };

  NSString *displayed = text;

  if (view.state.direction == TextDirectionRtl) {
    NSUInteger length = [text length];
    NSMutableString *reversed = [NSMutableString stringWithCapacity:length];

    [text enumerateSubstringsInRange:NSMakeRange(0, length)
                             options:(NSStringEnumerationReverse | NSStringEnumerationByComposedCharacterSequences)
                          usingBlock:^(NSString *substring, __unused NSRange substringRange, __unused NSRange enclosingRange, __unused BOOL *stop) {
                            [reversed appendString:substring];
                          }];

    displayed = reversed;
  }

  tl.string = [[NSAttributedString alloc] initWithString:displayed attributes:attributes];
  tl.allowsFontSubpixelQuantization = NO;

  tl.frame = CGRectApplyAffineTransform(
    CGRectMake(
      [x doubleValue],
      [y doubleValue] - font.pointSize,
      w ? [w doubleValue] : view.bounds.size.width - [x doubleValue],
      view.bounds.size.height
    ),
    view.state._transform
  );

  tl.alignmentMode = view.state.textAlign;
  tl.baselineOffset = font.pointSize - view.state.baselineOffset;
  tl.shadowOpacity = (float)view.state.shadowColor.alphaComponent;
  tl.shadowRadius = view.state.shadowBlur;
  tl.shadowOffset = view.state.shadowOffset;
  tl.shadowColor = [view.state.shadowColor CGColor];

  Path2D *clip = view.state._clip;
  if (clip) {
    CAShapeLayer *cl = [CAShapeLayer layer];
    CGPathRef clippingPath = [clip CGPath];
    cl.path = clippingPath;
    CGPathRelease(clippingPath);
    cl.fillColor = [[NSColor whiteColor] CGColor];
    cl.frame = [tl bounds];
    cl.fillRule = clip.fillRule;

    tl.mask = cl;
  }

  if (useGradient) {
    fillGradient(view, tl, stroke ? view.state.strokeGradient : view.state.fillGradient);
  } else {
    tl.opacity = (float) view.state.globalAlpha;
    [view addLayer:tl];
  }
}

/// canvas:fillText(text, x, y[, maxWidth]) -> void
/// Method
/// Fills the given text at the specified coordinates with the current `fillStyle`. An optional maximum width can be specified and text will be compressed to try and fit.
///
/// Parameters:
///  * 'text'     - The text to render
///  * `x`        - The X coordinate to start rendering the text at
///  * `y`        - The Y coordinate to start rendering the text at
///  * `maxWidth` - [Optional] The maximum width within which the text should fit
///
/// Returns:
///  * None
static int canvas_fillText(lua_State *L) {
  renderText(L, false);

  return 0;
}

/// canvas:getContextAttributes() -> void
/// Method
/// Get the current context parameters (alpha support, desynchronized [deferred] rendering)
///
/// Parameters:
///  * None
///
/// Returns:
///  * An object with booleans indicating alpha support and deferred rendering
static int canvas_getContextAttributes(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  lua_newtable(L);
    lua_pushboolean(L, YES); lua_setfield(L, -2, "alpha");
    lua_pushboolean(L, YES); lua_setfield(L, -2, "desynchronized");
  return 1;
}

static int canvas__getdirection(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:TextDirectionNames[[NSNumber numberWithUnsignedInteger:view.state.direction]]];
  return 1;
}

static int canvas__getfillStyle(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  if (view.state._useGradientFill) {
    [skin pushNSObject:view.state.fillGradient];
  } else {
    [skin pushNSObject:view.state._fillColorStr];
  }

  return 1;
}

// NOTE: Intentionally skipping for now
static int canvas__getfilter(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"filter is not implemented at this time"]];
  return 0;
}

static int canvas__getfont(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:view.state._fontStr];
  return 1;
}

static int canvas__getglobalAlpha(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.globalAlpha)];
  return 1;
}

static int canvas__getglobalCompositeOperation(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:CompositeOperationNames[[NSNumber numberWithUnsignedInteger:view.state._globalCompositeOperation]]];
  return 1;
}

/// canvas:getImageData(sx, sy, sw, sh) -> ImageData
/// Method
/// Get the underlying pixel data for the specified portion of the canvas
///
/// Parameters:
///  * `sx` - The X coordinate of the top-left corner of the rectangle from which the ImageData will be extracted
///  * `sy` - The Y coordinate of the top-left corner of the rectangle from which the ImageData will be extracted
///  * `sw` - The width of the rectangle from which the ImageData will be extracted
///  * `sh` - The height of the rectangle from which the ImageData will be extracted
///
/// Returns:
///  * An ImageData object which can be used later in `putImageData` or `drawImage`
static int canvas_getImageData(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *sx = [skin toNSObjectAtIndex:2];
  NSNumber *sy = [skin toNSObjectAtIndex:3];
  NSNumber *sw = [skin toNSObjectAtIndex:4];
  NSNumber *sh = [skin toNSObjectAtIndex:5];

  NSImage *image = [view getImageDataInRect:NSMakeRect([sx intValue], [sy intValue], [sw intValue], [sh intValue])];

  [skin pushNSObject:image];
  return 1;
}

static int canvas__getimageSmoothingEnabled(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  lua_pushboolean(L, view.state.imageSmoothingEnabled);
  return 1;
}

static int canvas__getimageSmoothingQuality(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"imageSmoothingQuality is not implemented at this time"]];
  return 0;
}

static int canvas__getlineCap(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:LineCapStyleNames[[NSNumber numberWithInteger:view.state.lineCap]]];
  return 1;
}

/// canvas:getLineDash() -> number{}
/// Method
/// Gets the current line dash pattern
///
/// Parameters:
///  * None
///
/// Returns:
///  * An array of numbers that specifies the distances used for the line dash pattern
static int canvas_getLineDash(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:view.state.lineDashPattern];
  return 1;
}

static int canvas__getlineDashOffset(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.lineDashOffset)];
  return 1;
}

static int canvas__getlineJoin(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:LineJoinStyleNames[[NSNumber numberWithInteger:view.state.lineJoin]]];
  return 1;
}

static int canvas__getlineWidth(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.lineWidth)];
  return 1;
}

static int canvas__getmiterLimit(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.miterLimit)];
  return 1;
}

static int canvas__getshadowBlur(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.shadowBlur)];
  return 1;
}

static int canvas__getshadowColor(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:view.state._shadowColorStr];
  return 1;
}

static int canvas__getshadowOffsetX(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.shadowOffset.width)];
  return 1;
}

static int canvas__getshadowOffsetY(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:@(view.state.shadowOffset.height)];
  return 1;
}

static int canvas__getstrokeStyle(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  if (view.state._useGradientStroke) {
    [skin pushNSObject:view.state.strokeGradient];
  } else {
    [skin pushNSObject:view.state._strokeColorStr];
  }

  return 1;
}

static int canvas__gettextAlign(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:view.state.textAlign];
  return 1;
}

static int canvas__gettextBaseline(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [skin pushNSObject:TextBaselineNames[[NSNumber numberWithUnsignedInteger:view.state._textBaseline]]];
  return 1;
}

/// canvas:getTransform() -> DOMMatrix
/// Method
/// Get the current transformation matrix
///
/// Parameters:
///  * None
///
/// Returns:
///  * A plain object representing the current transformation matrix (see hs.canvas.matrix for more information)
static int canvas_getTransform(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSAffineTransform *t = [NSAffineTransform transform];
  NSAffineTransformStruct ts = t.transformStruct;
  CGAffineTransform transform = view.state._transform;

  ts.m11 = transform.a;
  ts.m12 = transform.b;
  ts.m21 = transform.c;
  ts.m22 = transform.d;
  ts.tX  = transform.tx;
  ts.tY  = transform.ty;

  [t setTransformStruct:ts];

  [skin pushNSObject:t];
  return 1;
}

/// canvas:isPointInPath(x, y[, fillRule]) -> boolean
/// canvas:isPointInPath(path, x, y[, fillRule]) -> boolean
/// Method
/// Check whether or not the specified point is in the current path
///
/// Parameters:
///  * `path`     - [Optional] The path to use instead of the current path
///  * `x`        - The X coordinate of the point to check
///  * `y`        - The Y coordinate of the point to check
///  * `fillRule` - [Optional] The name of the winding rule to use ("nonzero" [default] or "evenodd")
///
/// Returns:
///  * A boolean indicating whether or not the point was found inside the path
static int canvas_isPointInPath(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  int args = lua_gettop(L);
  if (args == 4) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TSTRING | LS_TOPTIONAL, LS_TBREAK];
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TSTRING | LS_TOPTIONAL, LS_TBREAK];
  }

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  uint8_t idx = 2;

  Path2D *path                  = lua_type(L, idx) == LS_TUSERDATA ? (Path2D *) [skin luaObjectAtIndex:idx++ toClass:PATH2D_USERDATA_TAG] : view.path;
  NSNumber *x                   = [skin toNSObjectAtIndex:idx++];
  NSNumber *y                   = [skin toNSObjectAtIndex:idx++];
  CAShapeLayerFillRule fillRule = (idx == args ? [WindingRules[[skin toNSObjectAtIndex:idx]] unsignedIntValue] : NSNonZeroWindingRule) == NSNonZeroWindingRule ? kCAFillRuleNonZero : kCAFillRuleEvenOdd;

  lua_pushboolean(L, [path isPointInPath:NSMakePoint([x doubleValue], [y doubleValue]) fillRule:fillRule]);

  return 1;
}

/// canvas:isPointInStroke(x, y) -> boolean
/// canvas:isPointInStroke(path, x, y) -> boolean
/// Method
/// Check whether or not the specified point is in the current path
///
/// Parameters:
///  * `path` - [Optional] The path to use instead of the current path
///  * `x`    - The X coordinate of the point to check
///  * `y`    - The Y coordinate of the point to check
///
/// Returns:
///  * A boolean indicating whether or not the point was found inside the area found by stroking the path
static int canvas_isPointInStroke(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  int args = lua_gettop(L);
  if (args == 4) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];
  }

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  uint8_t idx = 2;

  Path2D *path = lua_type(L, idx) == LS_TUSERDATA ? [skin luaObjectAtIndex:idx++ toClass:PATH2D_USERDATA_TAG] : view.path;
  NSNumber *x  = [skin toNSObjectAtIndex:idx++];
  NSNumber *y  = [skin toNSObjectAtIndex:idx++];

  path.lineWidth = view.state.lineWidth;
  path.lineCap = view.state.lineCap;
  path.lineJoin = view.state.lineJoin;
  path.miterLimit = view.state.miterLimit;
  path.lineDashPattern = view.state.lineDashPattern;
  path.lineDashOffset = view.state.lineDashOffset;

  lua_pushboolean(L, [path isPointInStroke:NSMakePoint([x doubleValue], [y doubleValue])]);
  return 1;
}

/// canvas:lineTo(x, y) -> void
/// Method
/// Add a straight line to the given coordinates to the current subpath
///
/// Parameters:
///  * `x` - The X coordinate to end at
///  * `y` - The Y coordinate to end at
///
/// Returns:
///  * None
static int canvas_lineTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];

  [view.path lineTo:NSMakePoint([x doubleValue], [y doubleValue])];

  return 0;
}

/// canvas:measureText(text) -> void
/// Method
/// Determine the width and height of the given text once rendered using current settings
///
/// Parameters:
///  * `text` - The text to measure
///
/// Returns:
///  * An object with the calculated width and height of the text once rendered
static int canvas_measureText(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *text = [skin toNSObjectAtIndex:2];

  NSDictionary<NSString *, NSObject *> *attributes = @{
    NSFontAttributeName : view.state.font,
  };

  [skin pushNSSize:[text sizeWithAttributes:attributes]];
  return 1;
}

/// canvas:moveTo(x, y) -> void
/// Method
/// Start a new subpath on the current path at x,y
///
/// Parameters:
///  * `x` - The X coordinate to move to
///  * `y` - The Y coordinate to move to
///
/// Returns:
///  * None
static int canvas_moveTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];

  [view.path moveTo:NSMakePoint([x doubleValue], [y doubleValue])];

  return 0;
}

/// canvas:putImageData(image, dx, dy) -> void
/// canvas:putImageData(image, dx, dy, sx, sy, sw, sh) -> void
/// Method
/// Paint data from the given area of the given ImageData (or the entire object if no source coordinates are given) onto the canvas
///
/// Parameters:
///  * `image` - The source ImageData from which to paint
///  * `dx`    - The destination X coordinate
///  * `dy`    - The destination Y coordinate
///  * `sx`    - [Optional] The X coordinate of the top-left corner of the rectangle from which the ImageData will be extracted
///  * `sy`    - [Optional] The Y coordinate of the top-left corner of the rectangle from which the ImageData will be extracted
///  * `sw`    - [Optional] The width of the rectangle from which the ImageData will be extracted
///  * `sh`    - [Optional] The height of the rectangle from which the ImageData will be extracted
///
/// Returns:
///  * None
static int canvas_putImageData(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSImage *image = [skin luaObjectAtIndex:2 toClass:"NSImage"];

  NSNumber *dx;
  NSNumber *dy;
  NSNumber *sx = @0.0;
  NSNumber *sy = @0.0;
  NSNumber *sw = @(image.size.width);
  NSNumber *sh = @(image.size.height);

  NSInteger args = lua_gettop(L);
  if (args == 4) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, "hs.image", LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

    dx = [skin toNSObjectAtIndex:3];
    dy = [skin toNSObjectAtIndex:4];
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, "hs.image", LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

    dx = [skin toNSObjectAtIndex:3];
    dy = [skin toNSObjectAtIndex:4];
    sx = [skin toNSObjectAtIndex:5];
    sy = [skin toNSObjectAtIndex:6];
    sw = [skin toNSObjectAtIndex:7];
    sh = [skin toNSObjectAtIndex:8];
  }

  CGRect dr = CGRectApplyAffineTransform(CGRectMake([dx doubleValue], [dy doubleValue], [sw doubleValue], [sh doubleValue]), view.state._transform);
  CGRect sr = CGRectMake([sx doubleValue] / image.size.width, [sy doubleValue] / image.size.height, [sw doubleValue] / image.size.width, [sh doubleValue] / image.size.height);

  if (!CGRectIsEmpty(dr) && !CGRectIsEmpty(sr)) {
    [view addLayer:createImageLayer(view.state, image, dr, sr)];
  }

  return 0;
}

/// canvas:quadraticCurveTo(cpx, cpy, x, y) -> void
/// Method
/// Add a quadratic bezier curve to the current subpath using the given control points
///
/// Parameters:
///  * `cpx` - The control point's X coordinate
///  * `cpy` - The control point's Y coordinate
///  * `x`   - The end point's X coordinate
///  * `y`   - The end point's Y coordinate
///
/// Returns:
///  * None
static int canvas_quadraticCurveTo(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *cpx = [skin toNSObjectAtIndex:2];
  NSNumber *cpy = [skin toNSObjectAtIndex:3];
  NSNumber *x   = [skin toNSObjectAtIndex:4];
  NSNumber *y   = [skin toNSObjectAtIndex:5];

  NSPoint cp = NSMakePoint([cpx doubleValue], [cpy doubleValue]);
  NSPoint to = NSMakePoint([x doubleValue], [y doubleValue]);

  [view.path quadraticCurveTo:to cp:cp];

  return 0;
}

/// canvas:rect(x, y, w, h) -> void
/// Method
/// Add a rectangular subpath to the current path
///
/// Parameters:
///  * `x` - The X coordinate of the rectangle's starting point
///  * `y` - The Y coordinate of the rectangle's starting point
///  * `w` - The width of the rectangle
///  * `h` - The height of the rectangle
///
/// Returns:
///  * None
static int canvas_rect(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];
  NSNumber *w = [skin toNSObjectAtIndex:4];
  NSNumber *h = [skin toNSObjectAtIndex:5];

  [view.path rect:NSMakeRect([x doubleValue], [y doubleValue], [w doubleValue], [h doubleValue])];

  return 0;
}

/// canvas:resetTransform() -> void
/// Method
/// Reset the current transformation matrix to the identity matrix
///
/// Parameters:
///  * None
///
/// Returns:
///  * None
static int canvas_resetTransform(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  view.state._transform = CGAffineTransformIdentity;

  return 0;
}

/// canvas:restore() -> void
/// Method
/// Restore a saved graphics state
///
/// Parameters:
///  * None
///
/// Returns:
///  * None
static int canvas_restore(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [view restore];

  return 0;
}

/// canvas:rotate(angle) -> void
/// Method
/// Add a rotation to the current transformation matrix
///
/// Parameters:
///  * `angle` - The clocwise angle to rotate, in radians (degrees * PI / 180)
///
/// Returns:
///  * None
static int canvas_rotate(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *angle = [skin toNSObjectAtIndex:2];

  view.state._transform = CGAffineTransformRotate(view.state._transform, [angle doubleValue]);

  return 0;
}

/// canvas:save() -> void
/// Method
/// Save the current graphics state
///
/// Parameters:
///  * None
///
/// Returns:
///  * None
static int canvas_save(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  [view save];

  return 0;
}

/// canvas:scale(x, y) -> void
/// Method
/// Scale the current transformation matrix by a factor of x, y
///
/// Parameters:
///  * `x` - The horizontal scaling factor to apply. A factor of 1 results in no scaling.
///  * `y` - The vertical scaling factor to apply. A factor of 1 results in no scaling.
///
/// Returns:
///  * None
static int canvas_scale(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];

  view.state._transform = CGAffineTransformScale(view.state._transform, [x doubleValue], [y doubleValue]);

  return 0;
}

// NOTE: Intentionally skipping for now
static int canvas_scrollPathIntoView(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"scrollPathIntoView is not implemented at this time"]];
  return 0;
}

static int canvas__setdirection(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *textDirectionName = [skin toNSObjectAtIndex:2];
  if ([TextDirections objectForKey:textDirectionName]) {
    view.state.direction = [TextDirections[textDirectionName] unsignedIntValue];
  } else {
    [skin logWarn:[NSString stringWithFormat:@"Unknown text direction \"%@\". Defaulting to left-to-right.", textDirectionName]];
    view.state.direction = TextDirectionLtr;
  }

  return 0;
}

static int canvas__setfillStyle(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  if (lua_isuserdata(L, 2)) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, GRADIENT_USERDATA_TAG, LS_TBREAK];

    EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
    CanvasGradient *gradient = [skin luaObjectAtIndex:2 toClass:GRADIENT_USERDATA_TAG];

    view.state.fillGradient = gradient;
    view.state._useGradientFill = YES;
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

    EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

    NSString *fillColorString = [skin toNSObjectAtIndex:2];

    view.state.fillColor = parseColorString(fillColorString); // Make accessible to getters (unused in actual drawing operations)
    view.state._fillColorStr = fillColorString;
    view.state._useGradientFill = NO;
  }

  return 0;
}

// NOTE: Intentionally skipping for now
static int canvas__setfilter(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"filter is not implemented at this time"]];
  return 0;
}

// TODO: Expand font parsing capabilities beyond <size>px <name>
static NSFont *parseFontString(NSString *str) {
  NSArray<NSString *> *parts = [str componentsSeparatedByString:@"px"];
  if ([parts count] != 2) {
    [LuaSkin logWarn:[NSString stringWithFormat:@"Invalid font string \"%@\"", str]];

    return [NSFont systemFontOfSize:NSFont.systemFontSize];
  }

  int sizeAsInt;
  [[NSScanner scannerWithString:[parts[0] stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet]] scanInt:&sizeAsInt];
  NSString *name = [parts[1] stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet];

  double size = (double) sizeAsInt;
  NSString *nameLC = [name localizedLowercaseString];
  if ([nameLC isEqualToString:@"system"] || [nameLC isEqualToString:@"system-ui"]) {
    return [NSFont systemFontOfSize:size];
  } else if ([nameLC isEqualToString:@"ui-monospace"] || [nameLC isEqualToString:@"monospace"]) {
    if (@available(macOS 10.15, *)) {
      return [NSFont monospacedSystemFontOfSize:size weight:NSFontWeightMedium];
    } else {
      return [NSFont fontWithName:@"Courier New" size:size];
    }
  } else if ([nameLC isEqualToString:@"ui-serif"] || [nameLC isEqualToString:@"serif"]) {
    return [NSFont fontWithName:@"Times New Roman" size:size];
  } else if ([nameLC isEqualToString:@"ui-sans-serif"] || [nameLC isEqualToString:@"sans-serif"]) {
    return [NSFont fontWithName:@"Arial" size:size];
  }

  return [NSFont fontWithName:name size:size];
}

static int canvas__setfont(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TTABLE | LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  BOOL isString = lua_type(L, 2) == LUA_TSTRING;
  NSString *fontString = isString ? [skin toNSObjectAtIndex:2] : nil;
  NSFont *font = isString
    ? parseFontString(fontString)
    : [skin luaObjectAtIndex:2 toClass:"NSFont"]; // FUTURE: Remove when font parsing is more fleshed out

  view.state.font = font;
  view.state._fontStr = fontString ? fontString : [NSString stringWithFormat:@"%dpx %@", (int)font.pointSize, font.displayName];
  view.state.baselineOffset = computeTextBaselineOffsetForFont(view.state.font, view.state._textBaseline);

  return 0;
}

static int canvas__setglobalAlpha(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *globalAlpha = [skin toNSObjectAtIndex:2];

  view.state.globalAlpha = [globalAlpha doubleValue];

  return 0;
}

static int canvas__setglobalCompositeOperation(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *globalCompositeOperationName = [skin toNSObjectAtIndex:2];
  if ([CompositeOperations objectForKey:globalCompositeOperationName]) {
    view.state._globalCompositeOperation = [CompositeOperations[globalCompositeOperationName] unsignedIntValue];
  } else {
    [skin logWarn:[NSString stringWithFormat:@"Unknown composite operation \"%@\". Defaulting to \"source-over\".", globalCompositeOperationName]];
    view.state._globalCompositeOperation = NSCompositingOperationSourceOver;
  }

  return 0;
}

static int canvas__setimageSmoothingEnabled(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBOOLEAN, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *imageSmoothingEnabled = [skin toNSObjectAtIndex:2];

  view.state.imageSmoothingEnabled = [imageSmoothingEnabled boolValue];

  return 0;
}

// TODO: Finish
static int canvas__setimageSmoothingQuality(__unused lua_State *L) {
  [LuaSkin logWarn:[NSString stringWithFormat:@"imageSmoothingQuality is not implemented at this time"]];
  return 0;
}

static int canvas__setlineCap(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *lineCapName = [skin toNSObjectAtIndex:2];
  if ([LineCapStyles objectForKey:lineCapName]) {
    view.state.lineCap = [LineCapStyles[lineCapName] intValue];
  } else {
    [skin logWarn:[NSString stringWithFormat:@"Unknown line cap style \"%@\". Defaulting to butt style.", lineCapName]];
    view.state.lineCap = kCGLineCapButt;
  }

  return 0;
}

/// canvas:setLineDash(segments) -> void
/// Method
/// Set the line dash pattern that is used when stroking lines
///
/// Parameters:
///  * `segments` - An array of numbers describing the offsets to use in the dash pattern
///
/// Returns:
///  * None
///
/// Notes:
///  * Set the line dash pattern to an empty array in order to resume using a solid line
static int canvas_setLineDash(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TTABLE, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSObject *lineDash = [skin toNSObjectAtIndex:2];

  if (![lineDash isKindOfClass:[NSArray class]]) {
    [skin logError:@"The object provided to setLineDash must be an array"];

    return 0;
  }

  NSArray<NSObject *> *lineDashArray = (NSArray<NSObject *> *) lineDash;

  for (NSObject *v in lineDashArray) {
    if (![v isKindOfClass:[NSNumber class]]) {
      [LuaSkin logError:@"All members of the array provided to setLineDash must be numbers"];

      return 0;
    }
  }

  view.state.lineDashPattern = [lineDashArray count] != 0
    ? nil
    : (NSArray<NSNumber *> *) lineDashArray;

  return 0;
}

static int canvas__setlineDashOffset(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *lineDashOffset = [skin toNSObjectAtIndex:2];

  view.state.lineDashOffset = [lineDashOffset doubleValue];

  return 0;
}

static int canvas__setlineJoin(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *lineJoinName = [skin toNSObjectAtIndex:2];
  if ([LineJoinStyles objectForKey:lineJoinName]) {
    view.state.lineJoin = [LineJoinStyles[lineJoinName] intValue];
  } else {
    [skin logWarn:[NSString stringWithFormat:@"Unknown line join style \"%@\". Defaulting to miter style.", lineJoinName]];
    view.state.lineJoin = kCGLineJoinMiter;
  }

  return 0;
}

static int canvas__setlineWidth(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *lineWidth = [skin toNSObjectAtIndex:2];

  view.state.lineWidth = [lineWidth doubleValue];

  return 0;
}

static int canvas__setmiterLimit(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *miterLimit = [skin toNSObjectAtIndex:2];

  view.state.miterLimit = [miterLimit doubleValue];

  return 0;
}

static int canvas__setshadowBlur(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *shadowBlur = [skin toNSObjectAtIndex:2];

  view.state.shadowBlur = [shadowBlur doubleValue];

  return 0;
}

static int canvas__setshadowColor(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *shadowColorString = [skin toNSObjectAtIndex:2];

  view.state.shadowColor = parseColorString(shadowColorString); // Make accessible to getters (unused in actual drawing operations)
  view.state._shadowColorStr = shadowColorString;

  return 0;
}

static int canvas__setshadowOffsetX(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *shadowOffsetX = [skin toNSObjectAtIndex:2];

  view.state.shadowOffset = NSMakeSize([shadowOffsetX doubleValue], view.state.shadowOffset.height);

  return 0;
}

static int canvas__setshadowOffsetY(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *shadowOffsetY = [skin toNSObjectAtIndex:2];

  view.state.shadowOffset = NSMakeSize(view.state.shadowOffset.width, [shadowOffsetY doubleValue]);

  return 0;
}

static int canvas__setstrokeStyle(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  if (lua_isuserdata(L, 2)) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, GRADIENT_USERDATA_TAG, LS_TBREAK];

    EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
    CanvasGradient *gradient = [skin luaObjectAtIndex:2 toClass:GRADIENT_USERDATA_TAG];

    view.state.strokeGradient = gradient;
    view.state._useGradientStroke = YES;
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING | LS_TTABLE, LS_TBREAK];

    EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

    NSString *strokeColorString = [skin toNSObjectAtIndex:2];

    view.state.strokeColor = parseColorString(strokeColorString); // Make accessible to getters (unused in actual drawing operations)
    view.state._strokeColorStr = strokeColorString;
    view.state._useGradientStroke = NO;
  }

  return 0;
}

static int canvas__settextAlign(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSString *textAlign = [skin toNSObjectAtIndex:2];
  if ([TextAlignments containsObject:textAlign]) {
    view.state.textAlign = (CATextLayerAlignmentMode) textAlign;
  } else {
    [skin logWarn:[NSString stringWithFormat:@"Unknown text alignment mode \"%@\". Defaulting to left alignment.", textAlign]];
    view.state.textAlign = kCAAlignmentLeft;
  }

  return 0;
}

static int canvas__settextBaseline(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  TextBaseline baseline;
  NSString *baselineName = [skin toNSObjectAtIndex:2];
  if ([TextBaselines objectForKey:baselineName]) {
    baseline = [TextBaselines[baselineName] unsignedIntValue];
  } else {
    [skin logWarn:[NSString stringWithFormat:@"Unknown text baseline \"%@\". Defaulting to alphabetic baseline.", baselineName]];
    baseline = TextBaselineAlphabetic;
  }

  view.state.baselineOffset = computeTextBaselineOffsetForFont(view.state.font, baseline);
  view.state._textBaseline = baseline;

  return 0;
}

/// canvas:setTransform(matrix) -> void
/// canvas:setTransform(m11, m12, m21, m22, dx, dy) -> void
/// Method
/// Set the current transformation matrix
///
/// Parameters:
///  * `matrix` - The matrix object to use
///  * `m11`    - Horizontal scale
///  * `m12`    - Vertical skew
///  * `m21`    - Horizontal skew
///  * `m22`    - Vertical scale
///  * `dx`     - Horizontal translation
///  * `dy`     - Vertical translation
///
/// Returns:
///  * None
///
/// Notes:
///  * This method overwrites the current transformation matrix. If you wish to modify the current transformation matrix use `transform` instead.
static int canvas_setTransform(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  if (lua_gettop(L) == 2) { // First form: setTransform(matrix)
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TTABLE, LS_TBREAK];

    NSDictionary<NSObject *, NSObject *> *matrix = [skin toNSObjectAtIndex:2];

    view.state._transform = CGAffineTransformMake(
      [matrix objectForKey:@"m11"] && [matrix[@"m11"] isKindOfClass:[NSNumber class]] ? [(NSNumber *) matrix[@"m11"] doubleValue] : 1.0,
      [matrix objectForKey:@"m12"] && [matrix[@"m12"] isKindOfClass:[NSNumber class]] ? [(NSNumber *) matrix[@"m12"] doubleValue] : 0.0,
      [matrix objectForKey:@"m21"] && [matrix[@"m21"] isKindOfClass:[NSNumber class]] ? [(NSNumber *) matrix[@"m21"] doubleValue] : 0.0,
      [matrix objectForKey:@"m22"] && [matrix[@"m22"] isKindOfClass:[NSNumber class]] ? [(NSNumber *) matrix[@"m22"] doubleValue] : 1.0,
      [matrix objectForKey:@"tX"] && [matrix[@"tX"] isKindOfClass:[NSNumber class]] ? [(NSNumber *) matrix[@"tX"] doubleValue] : 0.0,
      [matrix objectForKey:@"tY"] && [matrix[@"tY"] isKindOfClass:[NSNumber class]] ? [(NSNumber *) matrix[@"tY"] doubleValue] : 0.0
    );
  } else { // Second form: setTransform(m11, m12, m21, m22, dx, dy)
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

    view.state._transform = CGAffineTransformMake(
      [(NSNumber *) [skin toNSObjectAtIndex:2] doubleValue],
      [(NSNumber *) [skin toNSObjectAtIndex:3] doubleValue],
      [(NSNumber *) [skin toNSObjectAtIndex:4] doubleValue],
      [(NSNumber *) [skin toNSObjectAtIndex:5] doubleValue],
      [(NSNumber *) [skin toNSObjectAtIndex:6] doubleValue],
      [(NSNumber *) [skin toNSObjectAtIndex:7] doubleValue]
    );
  }

  return 0;
}

/**
 * Helper method for stroking paths
 */
static void stroke(EnhancedCanvasView *view, Path2D *path) {
  if (path.empty) {
    return;
  }

  CAShapeLayer *sl = [CAShapeLayer layer];

  CGPathRef strokePath = [path CGPathWithTransform:view.state._transform];
  sl.path = strokePath;
  CGPathRelease(strokePath);
  sl.frame = view.bounds;

  sl.lineWidth = view.state.lineWidth;
  sl.lineCap = (CAShapeLayerLineCap _Nonnull) LineCapStyleNames[[NSNumber numberWithInteger:view.state.lineCap]];
  sl.lineDashPattern = view.state.lineDashPattern;
  sl.lineDashPhase = view.state.lineDashOffset;
  sl.lineJoin = (CAShapeLayerLineJoin _Nonnull) LineJoinStyleNames[[NSNumber numberWithInteger:view.state.lineJoin]];
  sl.miterLimit = view.state.miterLimit;

  Path2D *clip = view.state._clip;
  if (clip) {
    CAShapeLayer *cl = [CAShapeLayer layer];
    CGPathRef clippingPath = [clip CGPath];
    cl.path = clippingPath;
    CGPathRelease(clippingPath);
    cl.fillColor = [[NSColor whiteColor] CGColor];
    cl.frame = [sl bounds];
    cl.fillRule = clip.fillRule;

    sl.mask = cl;
  }

  if (view.state._useGradientStroke) {
    sl.fillColor = nil;
    sl.strokeColor = [[NSColor whiteColor] CGColor];
    fillGradient(view, sl, view.state.strokeGradient);
  } else {
    sl.opacity = (float)view.state.globalAlpha;
    sl.fillColor = nil;
    sl.strokeColor = [view.state.strokeColor CGColor];
    sl.shadowOpacity = (float)view.state.shadowColor.alphaComponent;
    sl.shadowRadius = view.state.shadowBlur;
    sl.shadowOffset = view.state.shadowOffset;
    sl.shadowColor = [view.state.shadowColor CGColor];

    [view addLayer:sl];
  }
}

/// canvas:stroke([path]) -> void
/// Method
/// Stroke the current or given path with the current `strokeStyle`
///
/// Parameters:
///  * `path` - [Optional] The path to use instead of the current path
///
/// Returns:
///  * None
static int canvas_stroke(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  int args = lua_gettop(L);
  if (args == 2) {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TUSERDATA, PATH2D_USERDATA_TAG, LS_TBREAK];
  } else {
    [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];
  }

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  Path2D *path = args == 2 ? [skin luaObjectAtIndex:2 toClass:PATH2D_USERDATA_TAG] : view.path;

  stroke(view, [path copy]);
  return 0;
}

/// canvas:strokeRect(x, y, w, h) -> void
/// Method
/// Stroke the rectangle bounded by x,y, x+w,x+h with the current `strokeStyle`. This does not affect the current subpath.
///
/// Parameters:
///  * `x` - The X coordinate of the rectangle's starting point
///  * `y` - The Y coordinate of the rectangle's starting point
///  * `w` - The width of the rectangle
///  * `h` - The height of the rectangle
///
/// Returns:
///  * None
static int canvas_strokeRect(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];
  NSNumber *w = [skin toNSObjectAtIndex:4];
  NSNumber *h = [skin toNSObjectAtIndex:5];

  stroke(view, [Path2D rect:NSMakeRect([x doubleValue], [y doubleValue], [w doubleValue], [h doubleValue])]);
  return 0;
}

/// canvas:strokeText(text, x, y[, maxWidth]) -> void
/// Method
/// Strokes the given text at the specified coordinates with the current `strokeStyle`. An optional maximum width can be specified and text will be compressed to try and fit.
///
/// Parameters:
///  * 'text'     - The text to render
///  * `x`        - The X coordinate to start rendering the text at
///  * `y`        - The Y coordinate to start rendering the text at
///  * `maxWidth` - [Optional] The maximum width within which the text should fit
///
/// Returns:
///  * None
static int canvas_strokeText(lua_State *L) {
  renderText(L, true);

  return 0;
}

/// canvas:transform(m11, m12, m21, m22, dx, dy) -> void
/// Method
/// Multiply the current transformation matrix with the matrix described by this method's parameters
///
/// Parameters:
///  * `m11`    - Horizontal scale
///  * `m12`    - Vertical skew
///  * `m21`    - Horizontal skew
///  * `m22`    - Vertical scale
///  * `dx`     - Horizontal translation
///  * `dy`     - Vertical translation
///
/// Returns:
///  * None
///
/// Notes:
///  * This method modifies the current transformation matrix. If you wish to overwrite the current transformation matrix use `setTransform` instead.
static int canvas_transform(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  CGAffineTransform t = CGAffineTransformMake(
    [(NSNumber *) [skin toNSObjectAtIndex:2] doubleValue],
    [(NSNumber *) [skin toNSObjectAtIndex:3] doubleValue],
    [(NSNumber *) [skin toNSObjectAtIndex:4] doubleValue],
    [(NSNumber *) [skin toNSObjectAtIndex:5] doubleValue],
    [(NSNumber *) [skin toNSObjectAtIndex:6] doubleValue],
    [(NSNumber *) [skin toNSObjectAtIndex:7] doubleValue]
  );

  view.state._transform = CGAffineTransformConcat(view.state._transform, t);

  return 0;
}

/// canvas:translate(x, y) -> void
/// Method
/// Translate the current transformation matrix by a factor of x, y
///
/// Parameters:
///  * `x` - The amount to move horizontally
///  * `y` - The amount to move vertically
///
/// Returns:
///  * None
static int canvas_translate(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER, LS_TNUMBER, LS_TBREAK];

  EnhancedCanvasView *view = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  NSNumber *x = [skin toNSObjectAtIndex:2];
  NSNumber *y = [skin toNSObjectAtIndex:3];

  view.state._transform = CGAffineTransformTranslate(view.state._transform, [x doubleValue], [y doubleValue]);

  return 0;
}

/******************************************************************************\
 * hs.canvas compatibility                                                    *
\******************************************************************************/
/// EnhancedCanvas.useCustomAccessibilitySubrole([state]) -> boolean
/// Function
/// Get or set whether or not canvas objects use a custom accessibility subrole for the contaning system window.
///
/// Parameters:
///  * `state` - an optional boolean, default true, specifying whether or not canvas containers should use a custom accessibility subrole.
///
/// Returns:
///  * the current, possibly changed, value as a boolean
///
/// Notes:
///  * Under some conditions, it has been observed that Hammerspoon's `hs.window.filter` module will misidentify Canvas and Drawing objects as windows of the Hammerspoon application that it should consider when evaluating its filters. To eliminate this, `hs.canvas` objects (and previously `hs.drawing` objects, which are now deprecated and pass through to `hs.canvas`) were given a nonstandard accessibilty subrole to prevent them from being included. This has caused some issues with third party tools, like Yabai, which also use the accessibility subroles for determining what actions it may take with Hammerspoon windows.
///
///  * By passing `false` to this function, all canvas objects will revert to specifying the standard subrole for the containing windows by default and should work as expected with third party tools. Note that this may cause issues or slowdowns if you are also using `hs.window.filter`; a more permanent solution is being considered.
///
///  * If you need to control the subrole of canvas objects more specifically, or only for some canvas objects, see [canvas:_accessibilitySubrole](#_accessibilitySubrole).
static int canvas_useCustomAccessibilitySubrole(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  if (lua_gettop(L) == 1) {
    defaultCustomSubRole = (BOOL) lua_toboolean(L, 1);
  }
  lua_pushboolean(L, defaultCustomSubRole);
  return 1;
}

/// EnhancedCanvas.new(rect) -> canvasObject
/// Constructor
/// Create a new canvas object at the specified coordinates
///
/// Parameters:
///  * `rect` - A rect-table containing the co-ordinates and size for the canvas object
///
/// Returns:
///  * a new, empty, canvas object, or nil if the canvas cannot be created with the specified coordinates
///
/// Notes:
///  * The size of the canvas defines the visible area of the canvas -- any portion of a canvas element which extends past the canvas's edges will be clipped.
///  * a rect-table is a table with key-value pairs specifying the top-left coordinate on the screen for the canvas (keys `x`  and `y`) and the size (keys `h` and `w`) of the canvas. The table may be crafted by any method which includes these keys, including the use of an `hs.geometry` object.
static int canvas_new(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TTABLE, LS_TBREAK];

  HSCanvasWindow *canvasWindow = [[HSCanvasWindow alloc] initWithContentRect:[skin tableToRectAtIndex:1]
                                                                   styleMask:NSWindowStyleMaskBorderless
                                                                     backing:NSBackingStoreBuffered
                                                                       defer:YES];
  if (canvasWindow) {
    EnhancedCanvasView *canvasView = [[EnhancedCanvasView alloc] initWithFrame:canvasWindow.contentView.bounds];
    canvasView.wrapperWindow = canvasWindow;
    canvasWindow.contentView = canvasView;

    [skin pushNSObject:canvasView];
  } else {
    lua_pushnil(L);
  }
  return 1;
}

/// canvas:_accessibilitySubrole([subrole]) -> canvasObject | current value
/// Method
/// Get or set the accessibility subrole returned by `hs.canvas` objects.
///
/// Parameters:
///  * `subrole` - an optional string or explicit nil wihch specifies what accessibility subrole value should be returned when canvas objects are queried through the macOS accessibility framework. See Notes for a discussion of how this value is interpreted. Defaults to `nil`.
///
/// Returns:
///  * If an argument is specified, returns the canvasObject; otherwise returns the current value.
///
/// Notes:
///  * Most people will probably not need to use this method; See [EnhancedCanvas.useCustomAccessibilitySubrole](#useCustomAccessibilitySubrole) for a discussion as to why this method may be of use when Hammerspoon is being controlled through the accessibility framework by other applications.
///
///  * If a non empty string is specified as the argument to this method, the string will be returned whenever the canvas object's containing window is queried for its accessibility subrole.
///  * The other possible values depend upon the value registerd with [EnhancedCanvas.useCustomAccessibilitySubrole](#useCustomAccessibilitySubrole):
///    * If `useCustomAccessibilitySubrole` is set to true (the default):
///      * If an explicit `nil` (the default) is specified fror this method, the string returned when the canvas object's accessibility is queried will be the default macOS subrole for the canvas's window with the string ".Hammerspoon` appended to it.
///      * If the empty string is specified (e.g. `""`), then the default macOS subrole for the canvas's window will be returned.
///    * If `useCustomAccessibilitySubrole` is set to false:
///      * If an explicit `nil` (the default) is specified fror this method, then the default macOS subrole for the canvas's window will be returned.
///      * If the empty string is specified (e.g. `""`), the string returned when the canvas object's accessibility is queried will be the default macOS subrole for the canvas's window with the string ".Hammerspoon` appended to it.
static int canvas_accessibilitySubrole(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TSTRING | LS_TNIL | LS_TOPTIONAL, LS_TBREAK];
  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;

  if (lua_gettop(L) == 1) {
    [skin pushNSObject:canvasWindow.subroleOverride];
  } else {
    canvasWindow.subroleOverride = lua_isstring(L, 2) ? [skin toNSObjectAtIndex:2] : nil;
    lua_pushvalue(L, 1);
  }
  return 1;
}


/// canvas:show() -> canvasObject
/// Method
/// Displays the canvas object
///
/// Parameters:
///  * `fadeInTime` - An optional number of seconds over which to fade in the canvas object. Defaults to zero.
///
/// Returns:
///  * The canvas object
///
/// Notes:
///  * if the canvas is in use as an element in another canvas, this method will result in an error.
static int canvas_show(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;

  if ([canvasView isParentWindow]) {
    [canvasWindow makeKeyAndOrderFront:nil];
  } else {
    canvasView.hidden = NO;
  }

  lua_pushvalue(L, 1);
  return 1;
}

/// canvas:hide() -> canvasObject
/// Method
/// Hides the canvas object
///
/// Parameters:
///  * `fadeOutTime` - An optional number of seconds over which to fade out the canvas object. Defaults to zero.
///
/// Returns:
///  * The canvas object
static int canvas_hide(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;

  if ([canvasView isParentWindow]) {
    [canvasWindow orderOut:nil];
  } else {
    canvasView.hidden = YES;
  }

  lua_pushvalue(L, 1);
  return 1;
}

/// canvas:clickActivating([flag]) -> canvasObject | currentValue
/// Method
/// Get or set whether or not clicking on a canvas with a click callback defined should bring all of Hammerspoon's open windows to the front.
///
/// Parameters:
///  * `flag` - an optional boolean indicating whether or not clicking on a canvas with a click callback function defined should activate Hammerspoon and bring its windows forward. Defaults to true.
///
/// Returns:
///  * If an argument is provided, returns the canvas object; otherwise returns the current setting.
///
/// Notes:
///  * Setting this to false changes a canvas object's AXsubrole value and may affect the results of filters used with `hs.window.filter`, depending upon how they are defined.
static int canvas_clickActivating(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = canvasView.wrapperWindow;

  if (lua_type(L, 2) != LUA_TNONE) {
    if (lua_toboolean(L, 2)) {
      canvasWindow.styleMask &= (unsigned long)~NSWindowStyleMaskNonactivatingPanel;
    } else {
      canvasWindow.styleMask |= NSWindowStyleMaskNonactivatingPanel;
    }
    lua_pushvalue(L, 1);
  } else {
    lua_pushboolean(L, ((canvasWindow.styleMask & NSWindowStyleMaskNonactivatingPanel) != NSWindowStyleMaskNonactivatingPanel));
  }

  return 1;
}

/// canvas:topLeft([point]) -> canvasObject | currentValue
/// Method
/// Get or set the top-left coordinate of the canvas object
///
/// Parameters:
///  * `point` - An optional point-table specifying the new coordinate the top-left of the canvas object should be moved to
///
/// Returns:
///  * If an argument is provided, the canvas object; otherwise the current value.
///
/// Notes:
///  * a point-table is a table with key-value pairs specifying the new top-left coordinate on the screen of the canvas (keys `x`  and `y`). The table may be crafted by any method which includes these keys, including the use of an `hs.geometry` object.
static int canvas_topLeft(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TTABLE | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  if ([canvasView isParentWindow]) {
    HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;
    NSRect oldFrame = RectWithFlippedYCoordinate(canvasWindow.frame);

    if (lua_gettop(L) == 1) {
      [skin pushNSPoint:oldFrame.origin];
    } else {
      NSPoint newCoord = [skin tableToPointAtIndex:2];
      NSRect  newFrame = RectWithFlippedYCoordinate(NSMakeRect(newCoord.x, newCoord.y, oldFrame.size.width, oldFrame.size.height));
      [canvasWindow setFrame:newFrame display:YES animate:NO];
      lua_pushvalue(L, 1);
    }
  } else {
    return luaL_argerror(L, 1, "method unavailable for canvas as a subview");
  }
  return 1;
}

/// canvas:size([size]) -> canvasObject | currentValue
/// Method
/// Get or set the size of a canvas object
///
/// Parameters:
///  * `size` - An optional size-table specifying the width and height the canvas object should be resized to
///
/// Returns:
///  * If an argument is provided, the canvas object; otherwise the current value.
///
/// Notes:
///  * a size-table is a table with key-value pairs specifying the size (keys `h` and `w`) the canvas should be resized to. The table may be crafted by any method which includes these keys, including the use of an `hs.geometry` object.
///
///  * elements in the canvas that have the `absolutePosition` attribute set to false will be moved so that their relative position within the canvas remains the same with respect to the new size.
///  * elements in the canvas that have the `absoluteSize` attribute set to false will be resized so that their relative size with respect to the canvas remains the same with respect to the new size.
static int canvas_size(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TTABLE | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  if ([canvasView isParentWindow]) {
    HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;

    NSRect oldFrame = canvasWindow.frame;

    if (lua_gettop(L) == 1) {
      [skin pushNSSize:oldFrame.size];
    } else {
      NSSize newSize  = [skin tableToSizeAtIndex:2];
      NSRect newFrame = NSMakeRect(oldFrame.origin.x, oldFrame.origin.y + oldFrame.size.height - newSize.height, newSize.width, newSize.height);

      [canvasWindow setFrame:newFrame display:YES animate:NO];
      lua_pushvalue(L, 1);
    }
  } else {
    return luaL_argerror(L, 1, "method unavailable for canvas as a subview");
  }
  return 1;
}

/// canvas:level([level]) -> canvasObject | currentValue
/// Method
/// Sets the window level more precisely than sendToBack and bringToFront.
///
/// Parameters:
///  * `level` - an optional level, specified as a number or as a string, specifying the new window level for the canvasObject. If it is a string, it must match one of the keys in [EnhancedCanvas.windowLevels](#windowLevels).
///
/// Returns:
///  * If an argument is provided, the canvas object; otherwise the current value.
static int canvas_level(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER | LS_TSTRING | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  if ([canvasView isParentWindow]) {
    HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;

    if (lua_gettop(L) == 1) {
      lua_pushinteger(L, [canvasWindow level]);
    } else {
      lua_Integer targetLevel;
      if (lua_type(L, 2) == LUA_TNUMBER) {
        [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG,
                LS_TNUMBER | LS_TINTEGER,
                LS_TBREAK];
        targetLevel = lua_tointeger(L, 2);
      } else {
        cg_windowLevels(L);
        if (lua_getfield(L, -1, [(NSString *) [skin toNSObjectAtIndex:2] UTF8String]) == LUA_TNUMBER) {
          targetLevel = lua_tointeger(L, -1);
          lua_pop(L, 2); // value and cg_windowLevels() table
        } else {
          lua_pop(L, 2); // wrong value and cg_windowLevels() table
          return luaL_error(L, [[NSString stringWithFormat:@"unrecognized window level: %@", [skin toNSObjectAtIndex:2]] UTF8String]);
        }
      }

      targetLevel = (targetLevel < CGWindowLevelForKey(kCGMinimumWindowLevelKey)) ? CGWindowLevelForKey(kCGMinimumWindowLevelKey) : ((targetLevel > CGWindowLevelForKey(kCGMaximumWindowLevelKey)) ? CGWindowLevelForKey(kCGMaximumWindowLevelKey) : targetLevel);
      [canvasWindow setLevel:targetLevel];
      lua_pushvalue(L, 1);
    }
  } else {
    return luaL_argerror(L, 1, "method unavailable for canvas as a subview");
  }

  return 1;
}

/// canvas:wantsLayer([flag]) -> canvasObject | currentValue
/// Method
/// Get or set whether or not the canvas object should be rendered by the view or by Core Animation.
///
/// Parameters:
///  * `flag` - optional boolean (default false) which indicates whether the canvas object should be rendered by the containing view (false) or by Core Animation (true).
///
/// Returns:
///  * If an argument is provided, the canvas object; otherwise the current value.
///
/// Notes:
///  * This method can help smooth the display of small text objects on non-Retina monitors.
static int canvas_wantsLayer(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBOOLEAN | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];

  if (lua_type(L, 2) != LUA_TNONE) {
    [canvasView setWantsLayer:(BOOL)lua_toboolean(L, 2)];
    canvasView.needsDisplay = YES;
    lua_pushvalue(L, 1);
  } else {
    lua_pushboolean(L, (BOOL)[canvasView wantsLayer]);
  }

  return 1;
}

static int canvas_behavior(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TNUMBER | LS_TOPTIONAL, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  if ([canvasView isParentWindow]) {
    HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;

    if (lua_gettop(L) == 1) {
      lua_pushinteger(L, (lua_Integer) [canvasWindow collectionBehavior]);
    } else {
      [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG,
              LS_TNUMBER | LS_TINTEGER,
              LS_TBREAK];

      NSInteger newLevel = lua_tointeger(L, 2);
      @try {
        [canvasWindow setCollectionBehavior:(NSWindowCollectionBehavior)newLevel];
      }
      @catch ( NSException *theException ) {
        return luaL_error(L, "%s: %s", [[theException name] UTF8String], [[theException reason] UTF8String]);
      }

      lua_pushvalue(L, 1);
    }
  } else {
    return luaL_argerror(L, 1, "method unavailable for canvas as a subview");
  }

  return 1;
}

/// canvas:delete() -> none
/// Method
/// Destroys the canvas object, optionally fading it out first (if currently visible).
///
/// Parameters:
///  * `fadeOutTime` - An optional number of seconds over which to fade out the canvas object. Defaults to zero.
///
/// Returns:
///  * None
///
/// Notes:
///  * This method is automatically called during garbage collection, notably during a Hammerspoon termination or reload, with a fade time of 0.
static int canvas_delete(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = canvasView.wrapperWindow;

  lua_pushcfunction(L, canvas_gc);
  lua_pushvalue(L, 1);
  // FIXME: Can we convert this lua_pcall() to a LuaSkin protectedCallAndError?
  if (lua_pcall(L, 1, 0, 0) != LUA_OK) {
    [skin logBreadcrumb:[NSString stringWithFormat:@"%s:error invoking _gc for delete method:%s", CANVAS_USERDATA_TAG, lua_tostring(L, -1)]];
    lua_pop(L, 1);
    [canvasWindow close]; // the least we can do is close the canvas if an error occurs with __gc
  }

  lua_pushnil(L);
  return 1;
}

/// canvas:isShowing() -> boolean
/// Method
/// Returns whether or not the canvas is currently being shown.
///
/// Parameters:
///  * None
///
/// Returns:
///  * a boolean indicating whether or not the canvas is currently being shown (true) or is currently hidden (false).
///
/// Notes:
///  * This method only determines whether or not the canvas is being shown or is hidden -- it does not indicate whether or not the canvas is currently off screen or is occluded by other objects.
///  * See also [canvas:isOccluded](#isOccluded).
static int canvas_isShowing(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;
  if ([canvasView isParentWindow]) {
    lua_pushboolean(L, [canvasWindow isVisible]);
  } else {
    lua_pushboolean(L, !canvasView.hidden && [canvasWindow isVisible]);
  }
  return 1;
}

/// canvas:isOccluded() -> boolean
/// Method
/// Returns whether or not the canvas is currently occluded (hidden by other windows, off screen, etc).
///
/// Parameters:
///  * None
///
/// Returns:
///  * a boolean indicating whether or not the canvas is currently being occluded.
///
/// Notes:
///  * If any part of the canvas is visible (even if that portion of the canvas does not contain any canvas elements), then the canvas is not considered occluded.
///  * a canvas which is completely covered by one or more opaque windows is considered occluded; however, if the windows covering the canvas are not opaque, then the canvas is not occluded.
///  * a canvas that is currently hidden or with a height of 0 or a width of 0 is considered occluded.
///  * See also [canvas:isShowing](#isShowing).
static int canvas_isOccluded(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TUSERDATA, CANVAS_USERDATA_TAG, LS_TBREAK];

  EnhancedCanvasView   *canvasView   = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  HSCanvasWindow *canvasWindow = (HSCanvasWindow *)canvasView.window;
  if ([canvasView isParentWindow]) {
    lua_pushboolean(L, ([canvasWindow occlusionState] & NSWindowOcclusionStateVisible) != NSWindowOcclusionStateVisible);
  } else {
    lua_pushboolean(L, canvasView.hidden || (([canvasWindow occlusionState] & NSWindowOcclusionStateVisible) != NSWindowOcclusionStateVisible));
  }
  return 1;
}

/// EnhancedCanvas.compositeTypes[]
/// Constant
/// A table containing the possible compositing rules for elements within the canvas.
///
/// The available types are as  described in the MDN documentation for CanvasRenderingContext2D: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
static int pushCompositeTypes(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin pushNSObject:CompositeOperations];
  return 1;
}

/// EnhancedCanvas.windowBehaviors[]
/// Constant
/// Array of window behavior labels for determining how a canvas or drawing object is handled in Spaces and Exposé
///
/// * `default`                   - The window can be associated to one space at a time.
/// * `canJoinAllSpaces`          - The window appears in all spaces. The menu bar behaves this way.
/// * `moveToActiveSpace`         - Making the window active does not cause a space switch; the window switches to the active space.
///
/// Only one of these may be active at a time:
///
/// * `managed`                   - The window participates in Spaces and Exposé. This is the default behavior if windowLevel is equal to NSNormalWindowLevel.
/// * `transient`                 - The window floats in Spaces and is hidden by Exposé. This is the default behavior if windowLevel is not equal to NSNormalWindowLevel.
/// * `stationary`                - The window is unaffected by Exposé; it stays visible and stationary, like the desktop window.
///
/// The following have no effect on `hs.canvas` or `hs.drawing` objects, but are included for completness and are expected to be used by future additions.
///
/// Only one of these may be active at a time:
///
/// * `participatesInCycle`       - The window participates in the window cycle for use with the Cycle Through Windows Window menu item.
/// * `ignoresCycle`              - The window is not part of the window cycle for use with the Cycle Through Windows Window menu item.
///
/// Only one of these may be active at a time:
///
/// * `fullScreenPrimary`         - A window with this collection behavior has a fullscreen button in the upper right of its titlebar.
/// * `fullScreenAuxiliary`       - Windows with this collection behavior can be shown on the same space as the fullscreen window.
///
/// Only one of these may be active at a time (Available in OS X 10.11 and later):
///
/// * `fullScreenAllowsTiling`    - A window with this collection behavior be a full screen tile window and does not have to have `fullScreenPrimary` set.
/// * `fullScreenDisallowsTiling` - A window with this collection behavior cannot be made a fullscreen tile window, but it can have `fullScreenPrimary` set.  You can use this setting to prevent other windows from being placed in the window’s fullscreen tile.
static int pushCollectionTypeTable(lua_State *L) {
  lua_newtable(L);
    lua_pushinteger(L, NSWindowCollectionBehaviorDefault);
    lua_setfield(L, -2, "default");
    lua_pushinteger(L, NSWindowCollectionBehaviorCanJoinAllSpaces);
    lua_setfield(L, -2, "canJoinAllSpaces");
    lua_pushinteger(L, NSWindowCollectionBehaviorMoveToActiveSpace);
    lua_setfield(L, -2, "moveToActiveSpace");
    lua_pushinteger(L, NSWindowCollectionBehaviorManaged);
    lua_setfield(L, -2, "managed");
    lua_pushinteger(L, NSWindowCollectionBehaviorTransient);
    lua_setfield(L, -2, "transient");
    lua_pushinteger(L, NSWindowCollectionBehaviorStationary);
    lua_setfield(L, -2, "stationary");
    lua_pushinteger(L, NSWindowCollectionBehaviorParticipatesInCycle);
    lua_setfield(L, -2, "participatesInCycle");
    lua_pushinteger(L, NSWindowCollectionBehaviorIgnoresCycle);
    lua_setfield(L, -2, "ignoresCycle");
    lua_pushinteger(L, NSWindowCollectionBehaviorFullScreenPrimary);
    lua_setfield(L, -2, "fullScreenPrimary");
    lua_pushinteger(L, NSWindowCollectionBehaviorFullScreenAuxiliary);
    lua_setfield(L, -2, "fullScreenAuxiliary");
    lua_pushinteger(L, NSWindowCollectionBehaviorFullScreenAllowsTiling);
    lua_setfield(L, -2, "fullScreenAllowsTiling");
    lua_pushinteger(L, NSWindowCollectionBehaviorFullScreenDisallowsTiling);
    lua_setfield(L, -2, "fullScreenDisallowsTiling");
  return 1;
}

/// EnhancedCanvas.windowLevels
/// Constant
/// A table of predefined window levels usable with [canvas:level](#level)
///
/// Predefined levels are:
///  * _MinimumWindowLevelKey - lowest allowed window level
///  * desktop
///  * desktopIcon            - [canvas:sendToBack](#sendToBack) is equivalent to this level - 1
///  * normal                 - normal application windows
///  * tornOffMenu
///  * floating               - equivalent to [canvas:bringToFront(false)](#bringToFront); where "Always Keep On Top" windows are usually set
///  * modalPanel             - modal alert dialog
///  * utility
///  * dock                   - level of the Dock
///  * mainMenu               - level of the Menubar
///  * status
///  * popUpMenu              - level of a menu when displayed (open)
///  * overlay
///  * help
///  * dragging
///  * screenSaver            - equivalent to [canvas:bringToFront(true)](#bringToFront)
///  * assistiveTechHigh
///  * cursor
///  * _MaximumWindowLevelKey - highest allowed window level
///
/// Notes:
///  * These key names map to the constants used in CoreGraphics to specify window levels and may not actually be used for what the name might suggest. For example, tests suggest that an active screen saver actually runs at a level of 2002, rather than at 1000, which is the window level corresponding to kCGScreenSaverWindowLevelKey.
///  * Each window level is sorted separately and [canvas:orderAbove](#orderAbove) and [canvas:orderBelow](#orderBelow) only arrange windows within the same level.
///  * If you use Dock hiding (or in 10.11, Menubar hiding) please note that when the Dock (or Menubar) is popped up, it is done so with an implicit orderAbove, which will place it above any items you may also draw at the Dock (or MainMenu) level.
///
///  * A canvas object with a [canvas:draggingCallback](#draggingCallback) function can only accept drag-and-drop items when its window level is at `EnhancedCanvas.windowLevels.dragging` or lower.
///  * A canvas object with a [canvas:mouseCallback](#mouseCallback) function can only reliably receive mouse click events when its window level is at `EnhancedCanvas.windowLevels.desktopIcon` + 1 or higher.
static int cg_windowLevels(lua_State *L) {
  lua_newtable(L);
    lua_pushinteger(L, CGWindowLevelForKey(kCGMinimumWindowLevelKey));           lua_setfield(L, -2, "_MinimumWindowLevelKey");
    lua_pushinteger(L, CGWindowLevelForKey(kCGDesktopWindowLevelKey));           lua_setfield(L, -2, "desktop");
    lua_pushinteger(L, CGWindowLevelForKey(kCGNormalWindowLevelKey));            lua_setfield(L, -2, "normal");
    lua_pushinteger(L, CGWindowLevelForKey(kCGFloatingWindowLevelKey));          lua_setfield(L, -2, "floating");
    lua_pushinteger(L, CGWindowLevelForKey(kCGTornOffMenuWindowLevelKey));       lua_setfield(L, -2, "tornOffMenu");
    lua_pushinteger(L, CGWindowLevelForKey(kCGDockWindowLevelKey));              lua_setfield(L, -2, "dock");
    lua_pushinteger(L, CGWindowLevelForKey(kCGMainMenuWindowLevelKey));          lua_setfield(L, -2, "mainMenu");
    lua_pushinteger(L, CGWindowLevelForKey(kCGStatusWindowLevelKey));            lua_setfield(L, -2, "status");
    lua_pushinteger(L, CGWindowLevelForKey(kCGModalPanelWindowLevelKey));        lua_setfield(L, -2, "modalPanel");
    lua_pushinteger(L, CGWindowLevelForKey(kCGPopUpMenuWindowLevelKey));         lua_setfield(L, -2, "popUpMenu");
    lua_pushinteger(L, CGWindowLevelForKey(kCGDraggingWindowLevelKey));          lua_setfield(L, -2, "dragging");
    lua_pushinteger(L, CGWindowLevelForKey(kCGScreenSaverWindowLevelKey));       lua_setfield(L, -2, "screenSaver");
    lua_pushinteger(L, CGWindowLevelForKey(kCGMaximumWindowLevelKey));           lua_setfield(L, -2, "_MaximumWindowLevelKey");
    lua_pushinteger(L, CGWindowLevelForKey(kCGOverlayWindowLevelKey));           lua_setfield(L, -2, "overlay");
    lua_pushinteger(L, CGWindowLevelForKey(kCGHelpWindowLevelKey));              lua_setfield(L, -2, "help");
    lua_pushinteger(L, CGWindowLevelForKey(kCGUtilityWindowLevelKey));           lua_setfield(L, -2, "utility");
    lua_pushinteger(L, CGWindowLevelForKey(kCGDesktopIconWindowLevelKey));       lua_setfield(L, -2, "desktopIcon");
    lua_pushinteger(L, CGWindowLevelForKey(kCGCursorWindowLevelKey));            lua_setfield(L, -2, "cursor");
    lua_pushinteger(L, CGWindowLevelForKey(kCGAssistiveTechHighWindowLevelKey)); lua_setfield(L, -2, "assistiveTechHigh");
  return 1;
}

static int canvas_tostring(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  EnhancedCanvasView *obj = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
  NSString *title;
  if ([obj isParentWindow]) {
    title = NSStringFromRect(RectWithFlippedYCoordinate(obj.window.frame));
  } else {
    title = NSStringFromRect(obj.frame);
  }
  [skin pushNSObject:[NSString stringWithFormat:@"%s: %@ (%p)", CANVAS_USERDATA_TAG, title, lua_topointer(L, 1)]];
  return 1;
}

static int canvas_eq(lua_State* L) {
// can't get here if at least one of us isn't a userdata type, and we only care if both types are ours,
// so use luaL_testudata before the macro causes a lua error
  if (luaL_testudata(L, 1, CANVAS_USERDATA_TAG) && luaL_testudata(L, 2, CANVAS_USERDATA_TAG)) {
    LuaSkin *skin = [LuaSkin sharedWithState:L];
    EnhancedCanvasView *obj1 = [skin luaObjectAtIndex:1 toClass:CANVAS_USERDATA_TAG];
    EnhancedCanvasView *obj2 = [skin luaObjectAtIndex:2 toClass:CANVAS_USERDATA_TAG];
    lua_pushboolean(L, [obj1 isEqualTo:obj2]);
  } else {
    lua_pushboolean(L, NO);
  }
  return 1;
}

static int canvas_gc(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  EnhancedCanvasView *view = get_objectFromUserdata(__bridge_transfer EnhancedCanvasView, L, 1, CANVAS_USERDATA_TAG);

  if (view) {
    if (![view isParentWindow]) {
      [view removeFromSuperview];
    }
    view.selfRef = [skin luaUnref:canvasRefTable ref:view.selfRef];

    NSDockTile *tile     = [[NSApplication sharedApplication] dockTile];
    NSView     *tileView = tile.contentView;
    if (tileView && [view isEqualTo:tileView]) {
      tile.contentView = nil;
    }

    HSCanvasWindow *theWindow = view.wrapperWindow;
    if (theWindow) {
      [theWindow close];
    }
    view.wrapperWindow = nil;
  }

  // Remove the Metatable so future use of the variable in Lua won't think its valid
  lua_pushnil(L);
  lua_setmetatable(L, 1);
  return 0;
}

/******************************************************************************\
 * EnhancedCanvasView helper methods                                          *
\******************************************************************************/
// These must not throw a lua error to ensure LuaSkin can safely be used from Objective-C
// delegates and blocks.
static int pushEnhancedCanvasView(lua_State *L, id obj) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  EnhancedCanvasView *value = obj;
  if (value.selfRef == LUA_NOREF) {
    void** valuePtr = lua_newuserdata(L, sizeof(EnhancedCanvasView *));
    *valuePtr = (__bridge_retained void *)value;
    luaL_getmetatable(L, CANVAS_USERDATA_TAG);
    lua_setmetatable(L, -2);
    value.selfRef = [skin luaRef:canvasRefTable];
  }
  [skin pushLuaRef:canvasRefTable ref:value.selfRef];
  return 1;
}

static id toEnhancedCanvasViewFromLua(lua_State *L, int idx) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  EnhancedCanvasView *value;
  if (luaL_testudata(L, idx, CANVAS_USERDATA_TAG)) {
    value = get_objectFromUserdata(__bridge EnhancedCanvasView, L, idx, CANVAS_USERDATA_TAG);
  } else {
    [skin logError:[NSString stringWithFormat:@"expected %s object, found %s", CANVAS_USERDATA_TAG, lua_typename(L, lua_type(L, idx))]];
  }
  return value;
}

// Implement CanvasRenderingContext2D API https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
static const luaL_Reg canvas_metaLib[] = {
// Public members
  {"arc",                  canvas_arc},
  {"arcTo",                canvas_arcTo},
  {"beginPath",            canvas_beginPath},
  {"bezierCurveTo",        canvas_bezierCurveTo},
  {"clearRect",            canvas_clearRect},
  {"clip",                 canvas_clip},
  {"closePath",            canvas_closePath},
  {"createConicGradient",  canvas_createConicGradient},
  {"createImageData",      canvas_createImageData},
  {"createLinearGradient", canvas_createLinearGradient},
  {"createPattern",        canvas_createPattern},
  {"createRadialGradient", canvas_createRadialGradient},
  {"drawFocusIfNeeded",    canvas_drawFocusIfNeeded},
  {"drawImage",            canvas_drawImage},
  {"ellipse",              canvas_ellipse},
  {"fill",                 canvas_fill},
  {"fillRect",             canvas_fillRect},
  {"fillText",             canvas_fillText},
  {"getContextAttributes", canvas_getContextAttributes},
  {"getImageData",         canvas_getImageData},
  {"getLineDash",          canvas_getLineDash},
  {"getTransform",         canvas_getTransform},
  {"isPointInPath",        canvas_isPointInPath},
  {"isPointInStroke",      canvas_isPointInStroke},
  {"lineTo",               canvas_lineTo},
  {"measureText",          canvas_measureText},
  {"moveTo",               canvas_moveTo},
  {"putImageData",         canvas_putImageData},
  {"quadraticCurveTo",     canvas_quadraticCurveTo},
  {"rect",                 canvas_rect},
  {"resetTransform",       canvas_resetTransform},
  {"restore",              canvas_restore},
  {"rotate",               canvas_rotate},
  {"save",                 canvas_save},
  {"scale",                canvas_scale},
  {"scrollPathIntoView",   canvas_scrollPathIntoView},
  {"setLineDash",          canvas_setLineDash},
  {"setTransform",         canvas_setTransform},
  {"stroke",               canvas_stroke},
  {"strokeRect",           canvas_strokeRect},
  {"strokeText",           canvas_strokeText},
  {"transform",            canvas_transform},
  {"translate",            canvas_translate},
// "Private" members
  // canvas.direction
  {"_getdirection",                canvas__getdirection},
  {"_setdirection",                canvas__setdirection},
  // canvas.filter
  {"_getfilter",                   canvas__getfilter},
  {"_setfilter",                   canvas__setfilter},
  // canvas.font
  {"_getfont",                     canvas__getfont},
  {"_setfont",                     canvas__setfont},
  // canvas.fillStyle
  {"_getfillStyle",                canvas__getfillStyle},
  {"_setfillStyle",                canvas__setfillStyle},
  // canvas.globalAlpha
  {"_getglobalAlpha",              canvas__getglobalAlpha},
  {"_setglobalAlpha",              canvas__setglobalAlpha},
  // canvas.globalCompositeOperation
  {"_getglobalCompositeOperation", canvas__getglobalCompositeOperation},
  {"_setglobalCompositeOperation", canvas__setglobalCompositeOperation},
  // canvas.imageSmoothingEnabled
  {"_getimageSmoothingEnabled",    canvas__getimageSmoothingEnabled},
  {"_setimageSmoothingEnabled",    canvas__setimageSmoothingEnabled},
  // canvas.imageSmoothingQuality
  {"_getimageSmoothingQuality",    canvas__getimageSmoothingQuality},
  {"_setimageSmoothingQuality",    canvas__setimageSmoothingQuality},
  // canvas.lineCap
  {"_getlineCap",                  canvas__getlineCap},
  {"_setlineCap",                  canvas__setlineCap},
  // canvas.lineDashOffset
  {"_getlineDashOffset",           canvas__getlineDashOffset},
  {"_setlineDashOffset",           canvas__setlineDashOffset},
  // canvas.lineJoin
  {"_getlineJoin",                 canvas__getlineJoin},
  {"_setlineJoin",                 canvas__setlineJoin},
  // canvas.lineWidth
  {"_getlineWidth",                canvas__getlineWidth},
  {"_setlineWidth",                canvas__setlineWidth},
  // canvas.miterLimit
  {"_getmiterLimit",               canvas__getmiterLimit},
  {"_setmiterLimit",               canvas__setmiterLimit},
  // canvas.shadowBlur
  {"_getshadowBlur",               canvas__getshadowBlur},
  {"_setshadowBlur",               canvas__setshadowBlur},
  // canvas.shadowColor
  {"_getshadowColor",              canvas__getshadowColor},
  {"_setshadowColor",              canvas__setshadowColor},
  // canvas.shadowOffsetX
  {"_getshadowOffsetX",            canvas__getshadowOffsetX},
  {"_setshadowOffsetX",            canvas__setshadowOffsetX},
  // canvas.shadowOffsetY
  {"_getshadowOffsetY",            canvas__getshadowOffsetY},
  {"_setshadowOffsetY",            canvas__setshadowOffsetY},
  // canvas.strokeStyle
  {"_getstrokeStyle",              canvas__getstrokeStyle},
  {"_setstrokeStyle",              canvas__setstrokeStyle},
  // canvas.textAlign
  {"_gettextAlign",                canvas__gettextAlign},
  {"_settextAlign",                canvas__settextAlign},
  // canvas.baselineOffset
  {"_gettextBaseline",             canvas__gettextBaseline},
  {"_settextBaseline",             canvas__settextBaseline},
// Misc. Canvas/Window operations (in addition to CanvasRenderingContext2D API implementation methods)
  {"behavior",              canvas_behavior},
  {"clickActivating",       canvas_clickActivating},
  {"delete",                canvas_delete},
  {"hide",                  canvas_hide},
  {"isOccluded",            canvas_isOccluded},
  {"isShowing",             canvas_isShowing},
  {"level",                 canvas_level},
  {"show",                  canvas_show},
  {"size",                  canvas_size},
  {"topLeft",               canvas_topLeft},
  {"wantsLayer",            canvas_wantsLayer},

  {"_accessibilitySubrole", canvas_accessibilitySubrole},

  {"__tostring",            canvas_tostring},
  {"__eq",                  canvas_eq},
  {"__gc",                  canvas_gc},

  {NULL,                    NULL}
};

// Functions for returned object when module loads
static luaL_Reg canvas_moduleLib[] = {
  {"new",                           canvas_new},
  {"useCustomAccessibilitySubrole", canvas_useCustomAccessibilitySubrole},

  {NULL,                   NULL}
};

/******************************************************************************\
 * Register library methods                                                   *
\******************************************************************************/
int luaopen_EnhancedCanvas_internal(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  // Register "helper object" libraries first
  luaopen_EnhancedCanvas_CanvasGradient(L);

  // Register canvas library last so its module functions are available to Lua
  canvasRefTable = [skin registerLibraryWithObject:CANVAS_USERDATA_TAG
                                         functions:canvas_moduleLib
                                     metaFunctions:nil    // or canvas_moduleMetaLib
                                   objectFunctions:canvas_metaLib];

  registerHelpers(skin, CANVAS_USERDATA_TAG, pushEnhancedCanvasView, toEnhancedCanvasViewFromLua);

  [Constants initialize];
  [Colors initialize];

  pushCompositeTypes(L);      lua_setfield(L, -2, "compositeTypes");
  pushCollectionTypeTable(L); lua_setfield(L, -2, "windowBehaviors");
  cg_windowLevels(L);         lua_setfield(L, -2, "windowLevels");

  // in case we're reloaded, return to default state
  defaultCustomSubRole = YES;

  return 1;
}
