@import Darwin.C.math;

#pragma once
#pragma clang diagnostic push
// Not all of these methods are used in each individual .m file that includes this, so suppress warnings about unused functions
#pragma clang diagnostic ignored "-Wunused-function"

/******************************************************************************\
 * Common helper macros                                                       *
\******************************************************************************/
#define get_objectFromUserdata(objType, L, idx, tag) (objType*)*((void**)luaL_checkudata(L, idx, tag))

/******************************************************************************\
 * Common helper enums                                                        *
\******************************************************************************/
typedef enum TextBaseline : NSUInteger {
  TextBaselineTop,
  TextBaselineHanging,
  TextBaselineMiddle,
  TextBaselineAlphabetic,
  TextBaselineIdeographic,
  TextBaselineBottom,
} TextBaseline;

/******************************************************************************\
 * CSS Color Keywords                                                         *
\******************************************************************************/
static NSCache<NSString *, NSColor *> *CachedColors = nil;
static NSDictionary<NSString *, NSColor *> *CSSColorMap = nil;

static const char *ColorNumberRE = "-?\\d+(?:\\.\\d+)?";

/******************************************************************************\
 * Shared helper methods                                                      *
\******************************************************************************/
static void registerHelpers(LuaSkin *skin, const char *className, pushNSHelperFunction pushHelper, luaObjectHelperFunction objectHelper) {
  if (![skin registerPushNSHelper:pushHelper forClass:className]) {
    [skin logError:[NSString stringWithFormat:@"An error occurred while registering the push helper for %s", className]];
  }
  if (![skin registerLuaObjectHelper:objectHelper forClass:className withUserdataMapping:className]) {
    [skin logError:[NSString stringWithFormat:@"An error occurred while registering the lua object helper for %s", className]];
  }
}

static NSDictionary *createReverseMappedDictionary(NSDictionary *dict) {
  NSMutableDictionary *reverse = [NSMutableDictionary dictionaryWithCapacity:[dict count]];
  [dict enumerateKeysAndObjectsUsingBlock:^(id key, id value, __unused BOOL *stop) {
    [reverse setValue:key forKey:value];
  }];
  return reverse;
}

static double computeTextBaselineOffsetForFont(NSFont *font, TextBaseline baseline) {
  double ascender = font.ascender;
  double capHeight = font.capHeight;
  double descender = font.descender;

  switch (baseline) {
    case TextBaselineTop:
      return -descender;
    case TextBaselineHanging:
      return ascender - capHeight - (descender / 2);
    case TextBaselineMiddle:
      return capHeight;
    case TextBaselineIdeographic:
      return ascender - (descender / 2);
    case TextBaselineBottom:
      return ascender - descender;
    case TextBaselineAlphabetic:
      /* falls through */
    default:
      return ascender;
  }
}

// NOTE: This is an approximation based on the table found here: https://stackoverflow.com/a/41435650
static int fontPtHeightToFontPxHeight(double fontHeightInPt) {
  return (int) lroundf((float) (fontHeightInPt * 4.0 / 3.0)); // Get nearest pixel height to actual calculated value
}

/******************************************************************************\
 * Color parsing helper methods                                               *
\******************************************************************************/
/**
 * Parse a hex color string in one of the following formats to an RGBA color:
 * - rgb
 * - rgba
 * - rrggbb
 * - rrggbbaa
 */
static NSColor *parseHexColorString(NSString *hex) {
  uint32_t r = 0;
  uint32_t g = 0;
  uint32_t b = 0;
  uint32_t a = 255;

  BOOL syntaxError = NO;

  if ([[NSScanner scannerWithString:hex] scanHexInt:NULL]) {
    NSUInteger l = [hex length];
    if (l == 3 || l == 4) { // rgb, rgba
      [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(0, 1)]] scanHexInt:&r];
      [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(1, 1)]] scanHexInt:&g];
      [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(2, 1)]] scanHexInt:&b];
      r *= 0x11;
      g *= 0x11;
      b *= 0x11;

      if (l == 4) {
        [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(3, 1)]] scanHexInt:&a];
        a *= 0x11;
      }
    } else if (l == 6 || l == 8) { // rrggbb, rrggbbaa
      [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(0, 2)]] scanHexInt:&r];
      [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(2, 2)]] scanHexInt:&g];
      [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(4, 2)]] scanHexInt:&b];

      if (l == 8) {
        [[NSScanner scannerWithString:[hex substringWithRange:NSMakeRange(6, 2)]] scanHexInt:&a];
      }
    } else {
      syntaxError = YES;
    }
  } else {
    syntaxError = YES;
  }

  if (syntaxError) {
    [LuaSkin logWarn:[NSString stringWithFormat:@"Invalid hexadecimal string #%@, returning solid black", hex]];

    return [NSColor blackColor];
  }

  NSColor *res = [NSColor colorWithCalibratedRed:((double) r / 255.0) green:((double) g / 255.0) blue:((double) b / 255.0) alpha:((double) a / 255.0)];

  [CachedColors setObject:res forKey:hex];

  return res;
}

/**
 * Attempt to get rgb[a]/hsl[a] color arguments from a string as an array of strings containing the individual arguments
 */
static NSArray<NSString *> *getColorFunctionArguments(NSString *str, BOOL includesAlpha, NSString *pattern) {
  NSError *err = NULL;
  NSRegularExpression *re = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&err];
  if (err) {
    [LuaSkin logWarn:@"Invalid regular expression %@"];

    return nil;
  }

  if (![re numberOfMatchesInString:str options:0 range:NSMakeRange(0, str.length)]) {
    return nil;
  }

  NSRange notFound = NSMakeRange(NSNotFound, 0);
  NSRange start = [str rangeOfString:@"("];
  NSRange end = [str rangeOfString:@")"];

  if (NSEqualRanges(start, notFound) || NSEqualRanges(end, notFound)) {
    return nil;
  }

  NSUInteger startIdx = start.location + start.length;
  NSUInteger endIdx = end.location;

  NSArray<NSString *> *args = [
    [
      [
        str substringWithRange:NSMakeRange(startIdx, endIdx - startIdx)
      ] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" ,/"]
    ] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]
  ];

  NSUInteger length = [args count];
  uint32_t expected = includesAlpha ? 4 : 3;

  if (length != expected) {
    return nil;
  }

  return args;
}

/**
 * Parse an RGB[A] function string in one of the following formats into an RGBA color:
 * - rgb[a](R, G, B[, A])
 * - rgb[a](R G B[ / A])
 */
static NSColor *parseRGBAFunctionString(NSString *rgba) {
  double r = 0;
  double g = 0;
  double b = 0;
  double a = 1.0;

  BOOL includesAlpha = [rgba hasPrefix:@"rgba"];
  NSArray<NSString *> *args = getColorFunctionArguments(
    rgba,
    includesAlpha,
    [NSString stringWithFormat:@"^rgba?\\(%s%%?,?\\s*?%s%%?,?\\s*?%s%%?(?:(?:,?\\s*?|\\s*?/\\s*?)%s%%?)?\\)$", ColorNumberRE, ColorNumberRE, ColorNumberRE, ColorNumberRE]
  );

  if (args == nil) {
    [LuaSkin logWarn:[NSString stringWithFormat:@"Unable to parse rgb[a](...) color string %@", rgba]];

    return [NSColor blackColor];
  }

  [[NSScanner scannerWithString:args[0]] scanDouble:&r];
  if ([args[0] hasSuffix:@"%"]) {
    r /= 100.0;
  } else {
    r /= 255.0;
  }
  [[NSScanner scannerWithString:args[1]] scanDouble:&g];
  if ([args[1] hasSuffix:@"%"]) {
    g /= 100.0;
  } else {
    g /= 255.0;
  }
  [[NSScanner scannerWithString:args[2]] scanDouble:&b];
  if ([args[2] hasSuffix:@"%"]) {
    b /= 100.0;
  } else {
    b /= 255.0;
  }

  if (includesAlpha) {
    [[NSScanner scannerWithString:args[3]] scanDouble:&a];
    if ([args[3] hasSuffix:@"%"]) {
      a /= 100.0;
    }
  }

  NSColor *res = [NSColor colorWithCalibratedRed:r green:g blue:b alpha:a];

  [CachedColors setObject:res forKey:rgba];

  return res;
}

/**
 * Parse an HSL[A] function string in one of the following formats into an RGBA color:
 * - hsl[a](H, S, L[, A])
 * - hsl[a](H S L[ / A])
 */
static NSColor *parseHSLAFunctionString(NSString *hsla) {
  double h = 0;
  double s = 0;
  double l = 0;
  double a = 1.0;

  BOOL includesAlpha = [hsla hasPrefix:@"hsla"];
  NSArray<NSString *> *args = getColorFunctionArguments(
    hsla,
    includesAlpha,
    [NSString stringWithFormat:@"^hsla?\\(%s(?:deg|g?rad|turn)?,?\\s*?%s%%?,?\\s*?%s%%?(?:(?:,?\\s*?|\\s*?/\\s*?)%s%%?)?\\)$", ColorNumberRE, ColorNumberRE, ColorNumberRE, ColorNumberRE]
  );

  if (args == nil) {
    [LuaSkin logWarn:[NSString stringWithFormat:@"Unable to parse hsl[a](...) color string %@", hsla]];

    return [NSColor blackColor];
  }

  [[NSScanner scannerWithString:args[0]] scanDouble:&h];
  if ([args[0] hasSuffix:@"grad"]) {
    h /= 400.0;
  } else if ([args[0] hasSuffix:@"rad"]) {
    h /= 2.0;
  } else if ([args[0] hasSuffix:@"turn"]) {
    // Do nothing
  } else { // Interpret as degrees
    h /= 360.0;
  }
  h = fmod(h, 1.0);
  if (h < 0.0) {
    h = 1.0 + h;
  }

  [[NSScanner scannerWithString:args[1]] scanDouble:&s];
  if ([args[1] hasSuffix:@"%"]) {
    s /= 100.0;
  }
  [[NSScanner scannerWithString:args[2]] scanDouble:&l];
  if ([args[2] hasSuffix:@"%"]) {
    l /= 100.0;
  }

  if (includesAlpha) {
    [[NSScanner scannerWithString:args[3]] scanDouble:&a];
    if ([args[3] hasSuffix:@"%"]) {
      a /= 100.0;
    }
  }

  // Convert HSL to HSB
  double b = l + s * MIN(l, 1.0 - l);
  s = (b == 0.0) ? 0.0 : 2 * (1 - (l / b));

  NSColor *res = [NSColor colorWithCalibratedHue:h saturation:s brightness:b alpha:a];

  [CachedColors setObject:res forKey:hsla];

  return res;
}

/**
 * Attempt to convert the given lua object into an NSColor. Capable of processing various dicts, color arrays, or hex color strings
 */
static NSColor *parseColorString(NSString *str) {
  NSColor *cached = [CachedColors objectForKey:str];
  if (cached) {
    return cached;
  }

  if ([str hasPrefix:@"#"]) {
    return parseHexColorString([str substringFromIndex:1]);
  } else if ([str hasPrefix:@"rgb"]) {
    return parseRGBAFunctionString(str);
  } else if ([str hasPrefix:@"hsl"]) {
    return parseHSLAFunctionString(str);
  } else if ([CSSColorMap objectForKey:str]) {
    return CSSColorMap[str];
  } else {
    // TODO: Add parsing for system colors?
    [LuaSkin logWarn:@"Expected a valid CSS color value (https://developer.mozilla.org/en-US/docs/Web/CSS/color_value)"];
  }

  return [NSColor blackColor];
}

@interface Colors : NSObject
@end

@implementation Colors
+ (void)initialize {
  if (CachedColors == nil) {
    CachedColors = [[NSCache alloc] init];
    CachedColors.name = @"CachedColors";
    CachedColors.countLimit = 2048; // Probably overkill
  }

  if (CSSColorMap == nil) {
    // CSS Colors with aliases
    NSColor *aqua           = parseHexColorString(@"00ffff");
    NSColor *darkgrey       = parseHexColorString(@"a9a9a9");
    NSColor *darkslategrey  = parseHexColorString(@"2f4f4f");
    NSColor *dimgrey        = parseHexColorString(@"696969");
    NSColor *fuschia        = parseHexColorString(@"ff00ff");
    NSColor *grey           = parseHexColorString(@"808080");
    NSColor *lightgrey      = parseHexColorString(@"d3d3d3");
    NSColor *lightslategrey = parseHexColorString(@"778899");
    NSColor *slategrey      = parseHexColorString(@"708090");

    CSSColorMap = @{
    // "transparent"
      @"transparent"          : [NSColor clearColor],
    // CSS Level 1 Keywords
      @"black"                : parseHexColorString(@"000000"),
      @"silver"               : parseHexColorString(@"c0c0c0"),
      @"gray"                 : grey,
      @"white"                : parseHexColorString(@"ffffff"),
      @"maroon"               : parseHexColorString(@"800000"),
      @"red"                  : parseHexColorString(@"ff0000"),
      @"purple"               : parseHexColorString(@"800080"),
      @"fuschia"              : fuschia,
      @"green"                : parseHexColorString(@"008000"),
      @"lime"                 : parseHexColorString(@"00ff00"),
      @"olive"                : parseHexColorString(@"808000"),
      @"yellow"               : parseHexColorString(@"ffff00"),
      @"navy"                 : parseHexColorString(@"000080"),
      @"blue"                 : parseHexColorString(@"0000ff"),
      @"teal"                 : parseHexColorString(@"008080"),
      @"aqua"                 : aqua,
    // CSS Level 2 Keywords
      @"orange"               : parseHexColorString(@"ffa500"),
    // CSS Level 3 Keywords
      @"aliceblue"            : parseHexColorString(@"f0f8ff"),
      @"antiquwhite"          : parseHexColorString(@"faebd7"),
      @"aquamarine"           : parseHexColorString(@"7fffd4"),
      @"azure"                : parseHexColorString(@"f0ffff"),
      @"beige"                : parseHexColorString(@"f5f5dc"),
      @"bisque"               : parseHexColorString(@"ffe4c4"),
      @"blanchedalmond"       : parseHexColorString(@"ffebcd"),
      @"blueviolet"           : parseHexColorString(@"8a2be2"),
      @"brown"                : parseHexColorString(@"a52a2a"),
      @"burlywood"            : parseHexColorString(@"deb887"),
      @"cadetblue"            : parseHexColorString(@"5f9ea0"),
      @"cartreuse"            : parseHexColorString(@"7fff00"),
      @"chocolate"            : parseHexColorString(@"d2691e"),
      @"coral"                : parseHexColorString(@"ff7f50"),
      @"cornflowerblue"       : parseHexColorString(@"6495ed"),
      @"cornsilk"             : parseHexColorString(@"fff8dc"),
      @"crimson"              : parseHexColorString(@"dc143c"),
      @"cyan"                 : aqua, // Synonym of "aqua"
      @"darkblue"             : parseHexColorString(@"00008b"),
      @"darkcyan"             : parseHexColorString(@"008b8b"),
      @"darkgoldenrod"        : parseHexColorString(@"b8860b"),
      @"darkgray"             : darkgrey,
      @"darkgreen"            : parseHexColorString(@"006400"),
      @"darkgrey"             : darkgrey,
      @"darkkhaki"            : parseHexColorString(@"bdb76b"),
      @"darkmagenta"          : parseHexColorString(@"8b008b"),
      @"darkolivegreen"       : parseHexColorString(@"556b2f"),
      @"darkorange"           : parseHexColorString(@"ff8c00"),
      @"darkorchid"           : parseHexColorString(@"9932cc"),
      @"darkred"              : parseHexColorString(@"8b0000"),
      @"darksalmon"           : parseHexColorString(@"e9967a"),
      @"darkseagreen"         : parseHexColorString(@"8fbc8f"),
      @"darkslateblue"        : parseHexColorString(@"483d8b"),
      @"darkslategray"        : darkslategrey,
      @"darkslategrey"        : darkslategrey,
      @"darkturquoise"        : parseHexColorString(@"00ced1"),
      @"darkviolet"           : parseHexColorString(@"9400d3"),
      @"deeppink"             : parseHexColorString(@"ff1493"),
      @"deepskyblue"          : parseHexColorString(@"00bfff"),
      @"dimgray"              : dimgrey,
      @"dimgrey"              : dimgrey,
      @"dodgerblue"           : parseHexColorString(@"1e90ff"),
      @"firebrick"            : parseHexColorString(@"b22222"),
      @"floralwhite"          : parseHexColorString(@"fffaf0"),
      @"forestgreen"          : parseHexColorString(@"228b22"),
      @"gainsboro"            : parseHexColorString(@"dcdcdc"),
      @"ghostwhite"           : parseHexColorString(@"f8f8ff"),
      @"gold"                 : parseHexColorString(@"ffd700"),
      @"goldenrod"            : parseHexColorString(@"daa520"),
      @"greenyellow"          : parseHexColorString(@"adff2f"),
      @"grey"                 : grey,
      @"honeydew"             : parseHexColorString(@"f0fff0"),
      @"hotpink"              : parseHexColorString(@"ff69b4"),
      @"indianred"            : parseHexColorString(@"cd5c5c"),
      @"indigo"               : parseHexColorString(@"4b0082"),
      @"ivory"                : parseHexColorString(@"fffff0"),
      @"khaki"                : parseHexColorString(@"f0e68c"),
      @"lavender"             : parseHexColorString(@"e6e6fa"),
      @"lavenderblush"        : parseHexColorString(@"fff0f5"),
      @"lawngreen"            : parseHexColorString(@"7cfc00"),
      @"lemonchiffon"         : parseHexColorString(@"fffacd"),
      @"lightblue"            : parseHexColorString(@"add8e6"),
      @"lightcoral"           : parseHexColorString(@"f08080"),
      @"lightcyan"            : parseHexColorString(@"e0ffff"),
      @"lightgoldenrodyellow" : parseHexColorString(@"fafad2"),
      @"lightgray"            : lightgrey,
      @"lightgreen"           : parseHexColorString(@"90ee90"),
      @"lightgrey"            : lightgrey,
      @"lightpink"            : parseHexColorString(@"ffb6c1"),
      @"lightsalmon"          : parseHexColorString(@"ffa07a"),
      @"lightseagreen"        : parseHexColorString(@"20b2aa"),
      @"lightskyblue"         : parseHexColorString(@"87cefa"),
      @"lightslategray"       : lightslategrey,
      @"lightslategrey"       : lightslategrey,
      @"lightsteelblue"       : parseHexColorString(@"b0c4de"),
      @"lightyellow"          : parseHexColorString(@"ffffe0"),
      @"limegreen"            : parseHexColorString(@"32cd32"),
      @"linen"                : parseHexColorString(@"faf0e6"),
      @"magenta"              : fuschia, // Synonym of "fuschia"
      @"mediumaquamarine"     : parseHexColorString(@"66cdaa"),
      @"mediumblue"           : parseHexColorString(@"0000cd"),
      @"mediumorchid"         : parseHexColorString(@"ba55d3"),
      @"mediumpurple"         : parseHexColorString(@"9370db"),
      @"mediumseagreen"       : parseHexColorString(@"3cb371"),
      @"mediumslateblue"      : parseHexColorString(@"7b68ee"),
      @"mediumspringgreen"    : parseHexColorString(@"00fa9a"),
      @"mediumturquoise"      : parseHexColorString(@"48d1cc"),
      @"mediumvioletred"      : parseHexColorString(@"c71585"),
      @"midnightblue"         : parseHexColorString(@"191970"),
      @"mintcream"            : parseHexColorString(@"f5fffa"),
      @"mistyrose"            : parseHexColorString(@"ffe4e1"),
      @"moccasin"             : parseHexColorString(@"ffe4b5"),
      @"navajowhite"          : parseHexColorString(@"ffdead"),
      @"oldlace"              : parseHexColorString(@"fdf5e6"),
      @"olivedrab"            : parseHexColorString(@"6b8e23"),
      @"orangered"            : parseHexColorString(@"ff4500"),
      @"orchid"               : parseHexColorString(@"da70d6"),
      @"palegoldenrod"        : parseHexColorString(@"eee8aa"),
      @"palegreen"            : parseHexColorString(@"98fb98"),
      @"paleturquoise"        : parseHexColorString(@"afeeee"),
      @"palevioletred"        : parseHexColorString(@"db7093"),
      @"papayawhip"           : parseHexColorString(@"ffefd5"),
      @"peachpuff"            : parseHexColorString(@"ffdab9"),
      @"peru"                 : parseHexColorString(@"cd853f"),
      @"pink"                 : parseHexColorString(@"ffc0cb"),
      @"plum"                 : parseHexColorString(@"dda0dd"),
      @"powderblue"           : parseHexColorString(@"b0e0e6"),
      @"rosybrown"            : parseHexColorString(@"bc8f8f"),
      @"royalblue"            : parseHexColorString(@"4169e1"),
      @"saddlebrown"          : parseHexColorString(@"8b4513"),
      @"salmon"               : parseHexColorString(@"fa8072"),
      @"sandybrown"           : parseHexColorString(@"f4a460"),
      @"seagreen"             : parseHexColorString(@"2e8b57"),
      @"seashell"             : parseHexColorString(@"fff5ee"),
      @"sienna"               : parseHexColorString(@"a0522d"),
      @"skyblue"              : parseHexColorString(@"87ceeb"),
      @"slateblue"            : parseHexColorString(@"6a5acd"),
      @"slategray"            : slategrey,
      @"slategrey"            : slategrey,
      @"snow"                 : parseHexColorString(@"fffafa"),
      @"springgreen"          : parseHexColorString(@"00ff7f"),
      @"steelblue"            : parseHexColorString(@"4682b4"),
      @"tan"                  : parseHexColorString(@"d2b48c"),
      @"thistle"              : parseHexColorString(@"d8bfd8"),
      @"tomato"               : parseHexColorString(@"ff6347"),
      @"turquoise"            : parseHexColorString(@"40e0d0"),
      @"violet"               : parseHexColorString(@"ee82ee"),
      @"wheat"                : parseHexColorString(@"f5deb3"),
      @"whitesmoke"           : parseHexColorString(@"f5f5f5"),
      @"yellowgreen"          : parseHexColorString(@"9acd32"),
    // CSS Level 4
      @"rebeccapurple"        : parseHexColorString(@"663399"),
    };
  }
}
@end
#pragma clang diagnostic pop
