local Array = require('Array')
local Symbol = require('Symbol')

local Members = Symbol('Members')

local function toString(enum)
	return Array.join(enum[Members])
end

-- Create an enumeration of named members. Optional reverse-mapping of named members can be disabled as well
local function Enum(members, reverseMappable)
	if not Array.isArrayLike(members) then
		error('Enum members must be an Array or Array-like structure')
	end

	local reverse = reverseMappable ~= nil and reverseMappable or true

	local enum = {
		[Members] = members,
	}

	for i,v in ipairs(members) do
		enum[v] = i
		if reverse then
			enum[i] = v
		end
	end

	return setmetatable(enum, {
		__index = self,
		__tostring = toString,
	})
end

return Enum
