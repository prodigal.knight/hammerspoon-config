--- === FileWatcher ===
---
--- hs.pathwatcher wrapper for simple watching of multiple paths with independent callbacks
---
--- Download: None

local Array = require('Array')
local Symbol = require('Symbol')
local utils = require('utils')

local pathwatcher = hs.pathwatcher

local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'FileWatcher'
obj.version = '1.0'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

obj.flags = {}
-- Change flags
Array.forEach(
	{
		'eventIdsWrapped',
		'historyDone',
		'itemChangeOwner',
		'itemCreated',
		'itemFinderInfoMod',
		'itemInodeMetaMod',
		'itemIsDir',
		'itemIsFile',
		'itemIsHardlink',
		'itemIsLastHardlink',
		'itemIsSymlink',
		'itemModified',
		'itemRemoved',
		'itemRenamed',
		'itemXattrMod',
		'kernelDropped',
		'mount',
		'mustScanSubDirs',
		'ownEvent',
		'rootChanged',
		'unmount',
		'userDropped',
	},
	function(flag)
		obj.flags[flag] = flag
	end
)

local started = false

-- Basic file/path watcher
local Watcher = {}
Watcher.ID = utils.IDGenerator:new()

-- Returns the function that is called whenever a file/folder in the path is changed
function Watcher.handleChanges(watcher)
	-- Only call the watcher handler if one or more of the flags for this change match the expected flags
	return function(paths, flags)
		for i = 1, #paths do
			if #watcher.filters == 0 or watcher.filters:some(function(filter) return flags[i][filter] end) then
				watcher.handler(watcher, paths[i], flags[i])
			end
		end
	end
end

function Watcher:new(path, handler, filters)
	local watcher = {
		id = Watcher.ID:next(),
		path = path,
		handler = handler,
		filters = filters,

		watcher = nil,
		running = false,
	}
	self.__index = self
	local res = setmetatable(watcher, self)
	res.watcher = pathwatcher.new(path, Watcher.handleChanges(res))
	return res
end

function Watcher:__tostring()
	return string.format('Watcher { <id>: %d, <path>: %s, <filters>: %s }', self.id, self.path, self.filters:join())
end

--- Watcher:start()
--- Method
--- Start this watcher
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function Watcher:start()
	obj.logger:f('Watching "%s"', self.path)
	self.watcher:start()
end

--- Watcher:stop()
--- Method
--- Stop this watcher
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function Watcher:stop()
	obj.logger:f('Stopped watching "%s"', self.path)
	self.watcher:stop()
end

local watchers = Array:new()

--- FileWatcher:watch(path, handler)
--- FileWatcher:watch(path, filters, handler)
--- Method
--- Create a watcher on path for the given event flags with the given handler
--- It's recommended that the handler be wrapped in a debouncing callback so that it isn't called multiple times in quick succession, e.g.: When Sublime Text saves a file it actually deletes the old file and moves the temporary file that it writes to as changes are made into its place, causing 2 changes in quick succession.
---
--- Parameters:
---  * path    - The string path to the file or folder you want to watch for changes. This can be an absolute or relative path, it will be converted to an absolute path internally.
---  * filters - (Optional) The change flags that you want to listen for
---  * handler - A callback that accepts 3 arguments: the watcher that it is part of, the file path that was changed, and the flags for that change; and returns nil
---
--- Returns:
---  * A Watcher object
function obj:watch(path, filters, hndlr)
	if not path then
		error('No path provided')
	end

	local handler = utils.default(hndlr, filters) -- Allow a handler with no filters

	if not handler then
		error('No change handler provided for watcher')
	elseif type(handler) ~= 'function' then
		error('Change handler must be a function')
	end

	if handler == filters then -- If no filters were provided, default to an empty array
		filters = {}
	end

	local absPath = utils.path.absolute(utils.env.replace(path))

	-- Check to see if there are existing watchers for this path, warn if there are
	local existing = watchers:find(function(watcher) return watcher.path == absPath end)
	if existing then
		obj.logger:wf('There is already an existing watcher for "%s" with ID %d, you should merge its definition with the other', path, existing.id)
	end

	-- Ignore unknown flags, warn the user about them
	local unknownFlags = Array.filter(filters, function(f) return not obj.flags[f] end)
	if #unknownFlags > 0 then
		obj.logger:wf('Unknown flag(s) in filter: %o. These will be ignored at runtime.', unknownFlags)
	end
	local knownFlags = Array.filter(filters, function(f) return not unknownFlags:includes(f) end)

	-- Create the watcher, add it to the watcher array, and return it
	local watcher = Watcher:new(absPath, handler, knownFlags)

	watchers:push(watcher)

	if started then
		watcher:start() -- Also start the watcher if it's added after the spoon has been started
	end

	return watcher
end

--- FileWatcher:init()
--- Method
--- Initialize this spoon
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:init()
	obj.logger = obj.createLogger(obj.name, obj.logLevel)
end

--- FileWatcher:start()
--- Method
--- Starts the FileWatcher spoon and any watchers that were created before starting
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:start()
	obj.logger:df('Starting...')

	if #watchers then
		obj.logger:vf('Starting watchers')
		watchers:forEach(function(watcher)
			watcher:start()
		end)
	else
		obj.logger:vf('No watchers registered yet, new watchers will be started immediately upon creation')
	end

	started = true

	obj.logger:df('Started')
end

--- FileWatcher:destroy()
--- Method
--- Explicitly destroy this FileWatcher and its registered watchers for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
	obj.logger:df('Destroying...')

	started = false

	obj.logger:vf('Stopping watchers')
	watchers:forEach(function(watcher)
		watcher:stop()
	end)

	watchers = nil

	obj.logger:vf('Destroyed')
	obj.logger = nil

	obj.createLogger = nil
end

return obj
