local Array = require('Array')
local Object = require('Object')
local Logger = require('StyledLogger')
local Symbol = require('Symbol')

local eventProps = hs.eventtap.event.properties
local keyMap = hs.keycodes.map
local currentLayout = hs.keycodes.currentLayout
local menuGlyphs = hs.application.menuGlyphs
local window = hs.window

local log = Logger:new('HwEvents', Logger.Levels.Debug)

local Flags = Symbol('Flags')
local Passive = Symbol('Passive')
local PropagationStopped = Symbol('PropagationStopped')
local RawEvent = Symbol('RawEvent')
local RawFlags = Symbol('RawFlags')

local LocationStandard = 0
local LocationLeft = 1
local LocationRight = 2
local LocationNumpad = 3

local lastFlags = 0

local Event = {}

-- Event flag masks as taken from hs.eventtap.event.rawFlagMasks, plus undocumented ones as `flagMaskN`
local flagMasks = {
	deviceLeftControl =         0x00000001,
	deviceLeftShift =           0x00000002,
	deviceRightShift =          0x00000004,
	deviceLeftCommand =         0x00000008,
	deviceRightCommand =        0x00000010,
	deviceLeftAlternate =       0x00000020,
	deviceRightAlternate =      0x00000040,
	deviceAlphaShiftStateless = 0x00000080,
	nonCoalesced =              0x00000100,
	flagMask10 =                0x00000200,
	flagMask11 =                0x00000400,
	flagMask12 =                0x00000800,
	flagMask13 =                0x00001000,
	deviceRightControl =        0x00002000,
	flagMask15 =                0x00004000,
	flagMask16 =                0x00008000,
	alphaShift =                0x00010000,
	shift =                     0x00020000,
	control =                   0x00040000,
	alternate =                 0x00080000,
	command =                   0x00100000,
	numericPad =                0x00200000,
	help =                      0x00400000,
	secondaryFn =               0x00800000,
	alphaShiftStateless =       0x01000000,
	flagMask26 =                0x02000000,
	flagMask27 =                0x04000000,
	flagMask28 =                0x08000000,
	flagMask29 =                0x10000000,
	flagMask30 =                0x20000000,
	flagMask31 =                0x40000000,
	flagMask32 =                0x80000000,
}

-- Individual glyphs for various special keys
local keySymbols = {
	tab         = menuGlyphs[0x02],
	shift       = menuGlyphs[0x05],
	ctrl        = menuGlyphs[0x06],
	alt         = menuGlyphs[0x07],
	space       = menuGlyphs[0x09],
	delete      = menuGlyphs[0x0A],
	enter       = menuGlyphs[0x0B], -- Return glyph makes more sense than actual Enter glyph
	cmd         = menuGlyphs[0x11],
	backspace   = menuGlyphs[0x17],
	escape      = menuGlyphs[0x1B],
	pageup      = menuGlyphs[0x62],
	capslock    = menuGlyphs[0x63],
	arrowleft   = menuGlyphs[0x64],
	arrowright  = menuGlyphs[0x65],
	arrowup     = menuGlyphs[0x68],
	arrowdown   = menuGlyphs[0x6A],
	pagedown    = menuGlyphs[0x6B],
	contextmenu = menuGlyphs[0x6D],
	power       = menuGlyphs[0x6E],
	f1          = menuGlyphs[0x6F],
	f2          = menuGlyphs[0x70],
	f3          = menuGlyphs[0x71],
	f4          = menuGlyphs[0x72],
	f5          = menuGlyphs[0x73],
	f6          = menuGlyphs[0x74],
	f7          = menuGlyphs[0x75],
	f8          = menuGlyphs[0x76],
	f9          = menuGlyphs[0x77],
	f10         = menuGlyphs[0x78],
	f11         = menuGlyphs[0x79],
	f12         = menuGlyphs[0x7A],
	f13         = menuGlyphs[0x87],
	f14         = menuGlyphs[0x88],
	f15         = menuGlyphs[0x89],
	f16         = menuGlyphs[0x8F],
	f17         = menuGlyphs[0x90],
	f18         = menuGlyphs[0x91],
	f19         = menuGlyphs[0x92],
}

-- Key code to key map based on Mac internals and https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
local codeToKey = {
	[0x00] = 'a',
  [0x01] = 's',
  [0x02] = 'd',
  [0x03] = 'f',
  [0x04] = 'h',
  [0x05] = 'g',
  [0x06] = 'z',
  [0x07] = 'x',
  [0x08] = 'c',
  [0x09] = 'v',
  [0x0B] = 'b',
  [0x0C] = 'q',
  [0x0D] = 'w',
  [0x0E] = 'e',
  [0x0F] = 'r',
  [0x10] = 'y',
  [0x11] = 't',
  [0x12] = '1',
  [0x13] = '2',
  [0x14] = '3',
  [0x15] = '4',
  [0x16] = '6',
  [0x17] = '5',
  [0x18] = '=',
  [0x19] = '9',
  [0x1A] = '7',
  [0x1B] = '-',
  [0x1C] = '8',
  [0x1D] = '0',
  [0x1E] = ']',
  [0x1F] = 'o',
  [0x20] = 'u',
  [0x21] = '[',
  [0x22] = 'i',
  [0x23] = 'p',
	[0x24] = 'Enter',
  [0x25] = 'l',
  [0x26] = 'j',
  [0x27] = "'",
  [0x28] = 'k',
  [0x29] = ';',
  [0x2A] = '\\',
  [0x2B] = ',',
  [0x2C] = '/',
  [0x2D] = 'n',
  [0x2E] = 'm',
  [0x2F] = '.',
	[0x30] = 'Tab',
	[0x31] = ' ',
  [0x32] = '`',
	[0x33] = 'Backspace',
	[0x34] = 'Enter',
	[0x35] = 'Escape',
	[0x36] = 'Meta',
	[0x37] = 'Meta',
	[0x38] = 'Shift',
	[0x39] = 'CapsLock',
	[0x3A] = 'Alt',
	[0x3B] = 'Control',
	[0x3C] = 'Shift',
	[0x3D] = 'Alt',
	[0x3E] = 'Control',
	[0x3F] = 'Fn',
	[0x40] = 'F17',
	[0x41] = '.',
	[0x43] = '*',
	[0x45] = '+',
	[0x47] = 'Clear',
	[0x48] = 'AudioVolumeUp',
	[0x49] = 'AudioVolumeDown',
	[0x4A] = 'AudioVolumeMute',
	[0x4B] = '/',
	[0x4C] = 'Enter',
	[0x4E] = '-',
	[0x4F] = 'F18',
	[0x50] = 'F19',
  [0x51] = '=',
	[0x52] = '0',
	[0x53] = '1',
	[0x54] = '2',
	[0x55] = '3',
	[0x56] = '4',
	[0x57] = '5',
	[0x58] = '6',
	[0x59] = '7',
	[0x5A] = 'F20',
	[0x5B] = '8',
	[0x5C] = '9',
	[0x5F] = 'Separator',
	[0x60] = 'F5',
	[0x61] = 'F6',
	[0x62] = 'F7',
	[0x63] = 'F3',
	[0x64] = 'F8',
	[0x65] = 'F9',
	[0x66] = 'Eisu',
	[0x67] = 'F11',
	[0x68] = 'KanjiMode',
	[0x69] = 'F13',
	[0x6A] = 'F16',
	[0x6B] = 'F14',
	[0x6D] = 'F10',
	[0x6E] = 'ContextMenu',
	[0x6F] = 'F12',
	[0x71] = 'F15',
	[0x72] = 'Help',
	[0x73] = 'Home',
	[0x74] = 'PageUp',
	[0x75] = 'Delete',
	[0x76] = 'F4',
	[0x77] = 'End',
	[0x78] = 'F2',
	[0x79] = 'PageDown',
	[0x7A] = 'F1',
	[0x7B] = 'ArrowLeft',
	[0x7C] = 'ArrowRight',
	[0x7D] = 'ArrowDown',
	[0x7E] = 'ArrowUp',
	[0xB3] = 'Fn',
}

-- Map of key code to its location on a standard keyboard
local keyLocationMap = {
	[0x36] = LocationRight,  -- Right Meta
	[0x37] = LocationLeft,   -- Left Meta
	[0x38] = LocationLeft,   -- Left Shift
	[0x3A] = LocationLeft,   -- Left Alt
	[0x3B] = LocationLeft,   -- Left Control
	[0x3C] = LocationRight,  -- Right Shift
	[0x3E] = LocationRight,  -- Right Control
	[0x41] = LocationNumpad, -- Numpad Decimal
	[0x43] = LocationNumpad, -- Numpad Multiply
	[0x45] = LocationNumpad, -- Numpad Plus
	[0x47] = LocationNumpad, -- Numpad Clear (Num Lock)
	[0x4B] = LocationNumpad, -- Numpad Divide
	[0x4C] = LocationNumpad, -- Numpad Enter
	[0x4E] = LocationNumpad, -- Numpad Minus
	[0x52] = LocationNumpad, -- Numpad 0
	[0x53] = LocationNumpad, -- Numpad 1
	[0x54] = LocationNumpad, -- Numpad 2
	[0x55] = LocationNumpad, -- Numpad 3
	[0x56] = LocationNumpad, -- Numpad 4
	[0x57] = LocationNumpad, -- Numpad 5
	[0x58] = LocationNumpad, -- Numpad 6
	[0x59] = LocationNumpad, -- Numpad 7
	[0x5B] = LocationNumpad, -- Numpad 8
	[0x5C] = LocationNumpad, -- Numpad 9
	[0x5F] = LocationNumpad, -- Numpad Separator
}

-- Character remapping based on layout. If you have a standard QWERTY keyboard this doesn't affect you
local layoutRemap = {
	Dvorak = { -- a, m shared with Qwerty
		['-'] = '[',
		['='] = ']',
		['q'] = "'",
		['w'] = ',',
		['e'] = '.',
		['r'] = 'p',
		['t'] = 'y',
		['y'] = 'f',
		['u'] = 'g',
		['i'] = 'c',
		['o'] = 'r',
		['p'] = 'l',
		['['] = '/',
		[']'] = '=',
		['s'] = 'o',
		['d'] = 'e',
		['f'] = 'u',
		['g'] = 'i',
		['h'] = 'd',
		['j'] = 'h',
		['k'] = 't',
		['l'] = 'n',
		[';'] = 's',
		["'"] = '-',
		['z'] = ';',
		['x'] = 'q',
		['c'] = 'j',
		['v'] = 'k',
		['b'] = 'x',
		['n'] = 'b',
		[','] = 'w',
		['.'] = 'v',
		['/'] = 'z',
	},
}

-- Vaguely mimic browser events
local Event = {}

-- Shortcut event constructors
function Event.keydown(type, raw)
	return Event.KeyboardEvent(type, raw)
end

function Event.keyup(type, raw)
	return Event.KeyboardEvent(type, raw)
end

function Event.mousedown(type, raw)
	return Event.MouseEvent(type, raw)
end

function Event.mouseup(type, raw)
	return Event.MouseEvent(type, raw)
end

function Event.mousemove(type, raw)
	return Event.MouseEvent(type, raw)
end

function Event.wheel(type, raw)
	return Event.WheelEvent(type, raw)
end

-- Keydown, Keyup events roughly based on https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent
function Event.KeyboardEvent(type, raw)
	local code = raw:getKeyCode()
	local keyPressed = codeToKey[code] or 'Unidentified'
	local layout = currentLayout()
	local key = layoutRemap[layout] and layoutRemap[layout][keyPressed] or keyPressed or keyPressed
	local location = keyLocationMap[code] or LocationStandard

	if keyPressed == 'Unidentified' then
		log:debug('Unknown key code: %d', code)
	else
		log:verbose('Key %s pressed (%d)', keyPressed, code)
	end

	local event = {
		code = code,
		keyPressed = keyPressed,
		key = key,
		location = location,
	}

	return Event:new(type, event, raw)
end

-- Mousedown, Mouseup, Mousemove events roughly based on https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent
function Event.MouseEvent(type, raw, event)
	local depressed = Array:new()
	for i = 0, 31 do
		if raw:getButtonState(i) then
			depressed:push(math.pow(2, i))
		end
	end

	local position = raw:location()
  local screenPos = hs.mouse.getRelativePosition()

	event = event or {}

	event.button = raw:getProperty(eventProps.mouseEventButtonNumber)
	event.buttons = depressed:reduce(function(acc, btn) return acc + btn end, 0)
	event.clientX = position.x
	event.clientY = position.y
	event.movementX = raw:getProperty(eventProps.mouseEventDeltaX)
	event.movementY = raw:getProperty(eventProps.mouseEventDeltaY)
  event.screen = hs.mouse.getCurrentScreen()
	event.screenX = screenPos.x
	event.screenY = screenPos.y
	event.x = position.x
	event.y = position.y

	return Event:new(type, event, raw)
end

-- WheelEvent roughly based on https://developer.mozilla.org/en-US/docs/Web/API/WheelEvent
function Event.WheelEvent(type, raw)
	return Event.MouseEvent(type, raw, {
		deltaX = raw:getProperty(eventProps.scrollWheelEventFixedPtDeltaAxis1),
		deltaY = raw:getProperty(eventProps.scrollWheelEventFixedPtDeltaAxis2),
		deltaZ = raw:getProperty(eventProps.scrollWheelEventFixedPtDeltaAxis3),
		deltaMode = 0,
	})
end

-- Base Event object, common to all event-specific constructors above
function Event:new(type, event, raw)
	event.type = type

	local flags = raw:rawFlags()

	event.altKey = flags & flagMasks.alternate ~= 0
	event.ctrlKey = flags & flagMasks.control ~= 0
	event.metaKey = flags & flagMasks.command ~= 0
	event.shiftKey = flags & flagMasks.shift ~= 0

	event.cancelable = true
	event.defaultPrevented = false
	event[Flags] = {}
	if event.altKey then event[Flags].alt = true end
	if event.ctrlKey then event[Flags].ctrl = true end
	if event.metaKey then event[Flags].cmd = true end
	if event.shiftKey then event[Flags].shift = true end
	event[Passive] = false
	event[PropagationStopped] = false
	event[RawEvent] = raw
	event[RawFlags] = flags

	self.__index = self
	return setmetatable(event, self)
end

-- Prevent default behavior (including letting the OS see the event)
function Event:preventDefault()
	if self.cancelable and not self[Passive] then
		self.defaultPrevented = true
	end
end

-- Prevent this event from being seen by further listeners
function Event:stopPropagation()
	if not self[Passive] then
		self[PropagationStopped] = true
	end
end

local listeners = {}

local HwEvents = {}

-- Basic event listener object with a callback function
local Listener = {}

function Listener:new(event, cb, opts)
	local listener = {
		event = event,
		cb = cb,
		opts = opts,
	}
	self.__index = self
	return setmetatable(listener, self)
end

-- Destroy this listener
function Listener:off()
	HwEvents.off(self)
end

-- Emit the given event to this listener's callback
-- NOTE: Rename as `run`?
function Listener:emit(evt)
	evt[Passive] = self.opts.passive

	self.cb(evt);

	evt[Passive] = false

	if self.opts.once then
		self:off()
	end
end

local types = hs.eventtap.event.types
-- Event types that this module will recognize as composites of hs.eventtap.event.types
local eventTypes = {
	mousedown = {types.leftMouseDown,types.rightMouseDown,types.otherMouseDown},
	mouseup = {types.leftMouseUp,types.rightMouseUp,types.otherMouseUp},
	mousemove = {types.mouseMoved,types.leftMouseDragged,types.rightMouseDragged,types.outherMouseDragged},
	keydown = {types.keyDown},
	keyup = {types.keyUp},
	wheel = {types.scrollWheel},
}
-- Reverse map for determining which type of event to emit in this module based on the raw hs.eventtap.event type
local eventTypeMap = {}
Object.entries(eventTypes):forEach(function(entry)
	Array.forEach(entry[2], function(type)
		eventTypeMap[type] = entry[1]
	end)
end)

-- Run the listeners for the given event type, if any
local function runEventListeners(type, raw)
	local _listeners = listeners[type]
	if _listeners then
		local evt = Event[type](type, raw)

		for i = #_listeners, 1, -1 do -- Process newer listeners first ("mimicking" DOM bubbling behavior)
			local listener = _listeners[i]

			local status, res = pcall(listener.emit, listener, evt)

			if not status then -- Don't care if listeners throw errors, move on to the next listener
				log:error(res)
			end

			if evt[PropagationStopped] then
				break
			end
		end

		return evt.defaultPrevented, res or {}
	else
		return false, {}
	end
end

-- Listen for modifier changes and emit keydown/keyup events as appropriate
local ModifierEventTap = hs.eventtap.new({types.flagsChanged}, function(raw)
	local flags = raw:rawFlags()
	local down = flags > lastFlags
	lastFlags = flags

	return runEventListeners(down and 'keydown' or 'keyup', raw)
end)
-- Listen for all other changes and emit the correct event type
local EventTap = hs.eventtap.new({'all'}, function(raw)
	local type = eventTypeMap[raw:getType()]

	if not type then
		return false, {}
	end

	return runEventListeners(type, raw)
end)

-- If the end user has provided a non-"canonical" name for a key then return the "canonical" name instead
local function toCanonicalName(modifier)
	local mod = string.lower(modifier)

	if mod == 'meta' or mod == 'super' or mod == 'command' or mod == keySymbols.cmd then
		return 'cmd'
	elseif mod == 'control' or mod == keySymbols.ctrl then
		return 'ctrl'
	elseif mod == 'option' or mod == keySymbols.alt then
		return 'alt'
	elseif mod == keySymbols.shift then
		return 'shift'
	end

	return mod
end

-- Ensure the given key spec is valid. Return a canonicalized version.
local function ensureValidSpec(spec)
	local modifiers, key = table.unpack(spec)

	if not key then
		return Array:new(), modifiers
	end

	return Array.map(modifiers, toCanonicalName), key
end

HwEvents.rawFlagMasks = flagMasks
HwEvents.Symbols = {
	RawEvent = RawEvent,
	RawFlags = RawFlags,
}

local assignedHotkeySpecs = {}

-- Similar to hs.hotkey.bind, except that it uses hs.eventtap listeners under the covers and can actually override OS shortcuts
-- TODO: Add ability to trigger on keyup instead of keydown
function HwEvents.bind(modifiers, key, cb)
	return HwEvents.bindSpec({modifiers, key}, cb)
end

-- Similar to hs.hotkey.bindSpec, except that it uses hs.eventtap listeners under the covers and can actually override OS shortcuts
-- TODO: Add ability to trigger on keyup instead of keydown
function HwEvents.bindSpec(spec, cb)
	local modifiers, key = ensureValidSpec(spec)
	local rep = modifiers:concat(key):map(function(k) return keySymbols[string.lower(k)] or string.lower(k) end):join('')

  if assignedHotkeySpecs[rep] then
    error(string.format('There is already an assigned hotkey callback for %s', rep))
  end

  assignedHotkeySpecs[rep] = true

	log:debug('Binding hotkey: %s', rep)

	return HwEvents.on('keydown', function(evt)
		if evt.key ~= key then
			return
		end

		local flags = Object.keys(evt[Flags])

		if #modifiers ~= #flags or not Array.every(modifiers, function(mod) return flags:includes(mod) end) then
			return
		end

		evt:preventDefault()
		evt:stopPropagation()

		cb()
	end)
end

-- Add a listener for a given event (mousedown, mouseup, mousemove, keydown, keyup, wheel) which will accept an Event object from above and act on it.
function HwEvents.on(event, cb, options)
	if not eventTypes[event] then
		error(string.format('Unknown or invalid event type "%s"', event))
	end

	if not EventTap:isEnabled() then
		EventTap:start()
	end
	if (event == 'keydown' or event == 'keyup') and not ModifierEventTap:isEnabled() then
		ModifierEventTap:start() -- Don't start this until necessary
	end

	if not listeners[event] then
		listeners[event] = Array:new()
	end

	local res = Listener:new(event, cb, options or {})

	listeners[event]:push(res)

	return res
end

-- Destroy the given listener, or any listeners for the given event with the given callback function
function HwEvents.off(listenerOrEvent, cb)
	local isListener = type(listenerOrEvent) == 'table'
	local event = isListener and listenerOrEvent.event or listenerOrEvent
	local fn = isListener and listenerOrEvent.cb or cb
	local _listeners = listeners[event]

	if not _listeners then
		return false
	end

	local len = #_listeners

	if len == 0 then
		return false
	end

	listeners[event] = _listeners:filter(function(listener) return listener.cb ~= fn end)

	if #listeners[event] == 0 then
		listeners[event] = nil

		if #Object.keys(listeners) == 0 then
			EventTap:stop()
			ModifierEventTap:stop()
		end

		return true
	end

	return #listeners[event] ~= len
end

return HwEvents
