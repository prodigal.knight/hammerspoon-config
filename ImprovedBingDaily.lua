--- === ImprovedBingDaily ===
---
--- Use the daily Bing picture as your wallpaper, automatically.
---
--- Download: None

local Array = require('Array')
local utils = require('utils')

local http = hs.http
local screenWatcher = hs.screen.watcher
local task = hs.task
local timer = hs.timer

local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'ImprovedBingDaily'
obj.version = '1.0'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

--- ImprovedBingDaily.interval
--- Variable
--- Adjusts how often the current daily Bing picture is queried for update.
--- Defaults to 600 seconds (10 minutes)
obj.interval = 600

--- ImprovedBingDaily.downloadDestinationFolderPath
--- Variable
--- Choose where to save downloaded images, relative to your home folder
--- Defaults to ~/.Trash so that downloaded images are automatically cleaned up periodically and don't fill your disk
obj.downloadDestinationFolderPath = '.Trash'

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

local userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac Os X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4'
local imageLocationUrl = 'https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1'
local imageUrlBase = 'https://www.bing.com'

local last = nil
local startDate = nil
local download = nil

local interval = nil
local screenChangeWatcher = nil

local setDesktopBackground

-- Returns the callback for when the image has been downloaded
local function onImageDownloaded(filePath)
	-- Callback for `curl` result
	return function(code, stdout, stderr)
		if code == 0 then
			setDesktopBackground(filePath)
		else
			obj.logger:wf('An error occurred while downloading the Bing Daily Image')
			obj.logger:df('stdout:\n%s', stdout)
			obj.logger:df('stderr:\n%s', stderr)
		end

		download = nil
	end
end

-- Create a task to download the daily Bing picture with `curl`
local function downloadBingDailyImage(filePath, imagePath)
	obj.logger:df('Downloading daily image from ' .. imageUrlBase .. imagePath)
	if download then
		download:terminate()
		download = nil
	end
	download = task.new('/usr/bin/curl', onImageDownloaded(filePath), {'-A', userAgent, imageUrlBase .. imagePath, '-o', filePath})
	download:start()
end

-- Set the background on all screens
local function setBackgroundOnAllScreens(filePath)
	obj.logger:vf('Setting desktop background image to "%s" on all screens', filePath)
	for i,screen in ipairs(hs.screen.allScreens()) do
		screen:desktopImageURL('file://' .. filePath)
	end
end

-- Set the desktop background on all screens, if it has changed. Download the image from Bing if it does not exist locally.
setDesktopBackground = function(filePath, imagePath, imageStartDate)
	if filePath == last then
		obj.logger:df('Image path not changed, skipping')

		return
	end

	if not utils.fileExists(filePath) then
		if imagePath then
			downloadBingDailyImage(filePath, imagePath)
		else
			obj.logger:wf('Was unable to download Bing Daily Image')
		end

		return
	end

	obj.logger:vf('Setting last background image path to "%s"', filePath)
	last = filePath
	obj.logger:vf('Setting date for background image (%s)', imageStartDate)
	startDate = imageStartDate

	setBackgroundOnAllScreens(filePath)
end

-- Check Bing for daily picture details, if the current date is different from the date of the last downloaded image
local function checkDailyImage()
	local date = os.date('%Y%m%d')

	if date == startDate then
		obj.logger:vf("Current date not changed from previous image's start date (%s), skipping check for new image", date)

		return
	end

	obj.logger:vf('Sending request to check latest Bing Daily Image')

	http.asyncGet(imageLocationUrl, { ['User-Agent'] = userAgent }, function(status, body, headers)
		obj.logger:vf('Got response')

		if status == 200 then
			local success, result = pcall(hs.json.decode, body)

			if success then
				local image = result.images[1]
				local imageUrl = image.url
				local imageStartDate = image.startdate

				local imagePath = Array.find(hs.http.urlParts(imageUrl).queryItems, function(v) return v.id ~= nil end).id
				local filePath = utils.env.replace(utils.path.join('${HOME}', obj.downloadDestinationFolderPath, imagePath))

				if filePath ~= last then
					setDesktopBackground(filePath, imageUrl, imageStartDate)
				else
					obj.logger:vf('Current daily image is same as last, skipping change')
				end
			else
				obj.logger:wf('Unable to parse body as JSON: %o', result)
			end
		else
			obj.logger:wf('Got response error: {\n  Code: %d\n  Headers: %s\n  Body: %s\n}', code, headers, body)
		end
	end)
end

--- ImprovedBingDaily:init()
--- Method
--- Initialize this spoon
---
--- Parameters
---  * None
---
--- Returns:
---  * nil
function obj:init()
	obj.logger = obj.createLogger(obj.name, obj.logLevel)
end

--- ImprovedBingDaily:start()
--- Method
--- Start the check/download interval and a screen change watcher (in case screens are added/removed while the spoon is running)
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:start()
	obj.logger:df('Starting...')

	if interval ~= nil then
		interval:stop()
		interval = nil
	end

	obj.logger:df('Creating interval to check/download Bing Daily Image')
	interval = timer.doEvery(obj.interval, checkDailyImage)

	interval:fire()

	obj.logger:df('Creating screen watcher to set background image as screens are added/removed')
	screenChangeWatcher = screenWatcher.new(utils.debounce(0.5, function()
		obj.logger:df('A change in active screens was detected, refreshing screen backgrounds')
		if last then
			setBackgroundOnAllScreens(last)
		else
			obj.logger:wf('Last image not known, skipping')
		end
	end))

	screenChangeWatcher:start()

	obj.logger:df('Started')
end

--- ImprovedBingDaily:destroy()
--- Method
--- Explicitly destroy this ImprovedBingDaily and interval/screen watcher for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
	obj.logger:df('Destroying...')

	obj.logger:vf('Stopping screen change watcher')
	screenChangeWatcher:stop();
	screenChangeWatcher = nil

	obj.logger:vf('Stopping interval')
	interval:stop()
	interval = nil

	obj.logger:vf('Destroyed')
	obj.logger = nil

	obj.createLogger = nil
end

return obj
