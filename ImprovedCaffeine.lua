--- === Caffeine ===
---
--- Prevent the screen from going to sleep
---
--- Download: None

local HwEvents = require('HwEvents')
local utils = require('utils')

local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'ImprovedCaffeine'
obj.version = '1.0'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

local menubar = nil

local hotkeys = {}

local function updateMenubar()
  local displayIdle = hs.caffeinate.get('displayIdle')
  local systemIdle = hs.caffeinate.get('systemIdle')
  local system = hs.caffeinate.get('system')

  menubar:setMenu({
    { title = 'Display Idle', fn = obj.toggleDisplayIdle, checked = displayIdle, shortcut = 'd' },
    { title = 'System Idle', fn = obj.toggleSystemIdle, checked = systemIdle, shortcut = 'i' },
    { title = 'System Sleep', fn = obj.toggleSystemSleep, checked = system, shortcut = 's' },
  })

  local on = displayIdle or systemIdle or system

  menubar:setIcon(utils.path.absolute(on and './resources/caffeine-on.pdf' or './resources/caffeine-off.pdf'))
end

local function toggle(setting, state)
  if state == nil then
    state = not hs.caffeinate.get(setting) -- Invert current state in order to set new state
  end

  hs.caffeinate.set(setting, state)

  updateMenubar()
end

function obj.lockScreen()
  hs.caffeinate.lockScreen()
end

function obj.startScreensaver()
  hs.caffeinate.startScreensaver()
end

function obj.toggleDisplayIdle()
  toggle('displayIdle')
end

function obj.enableDisplayIdle()
  toggle('displayIdle', false)
end

function obj.disableDisplayIdle()
  toggle('displayIdle', true)
end

function obj.toggleSystemIdle()
  toggle('systemIdle')
end

function obj.enableSystemIdle()
  toggle('systemIdle', false)
end

function obj.disableSystemIdle()
  toggle('systemIdle', true)
end

function obj.toggleSystemSleep()
  toggle('system')
end

function obj.enableSystemSleep()
  toggle('system', false)
end

function obj.disableSystemSleep()
  toggle('system', true)
end

local hotkeyActions = {
  lock = function() obj.lockScreen() end,
  screensaver = function() obj.startScreensaver() end,
  display = function() obj.toggleDisplayIdle() end,
  system = function() obj.toggleSystemIdle() end,
  sleep = function() obj.toggleSystemSleep() end,
}

--- ImprovedCaffeine:bindHotkeys(mapping)
--- Method
--- Bind hotkeys for ImprovedCaffeine
---
--- Parameters:
---  * mapping - A table containing the hotkey bindSpecs for the following (as described in hs.hotkey.bindSpec):
---   * lock    - Lock the screen
---   * display - Toggle hs.caffeinate display idle
---   * system  - Toggle hs.caffeinate system idle
---   * sleep   - Toggle hs.caffeinate system sleep
---
--- Returns:
---  * The ImprovedCaffeine object
function obj:bindHotkeys(mapping)
  self.logger:df('Binding hotkeys')
  self.logger:vf('Given mapping: %o', mapping)

  for action, spec in pairs(mapping) do
    if hotkeys[action] then
      hotkeys[action]:off()
      hotkeys[action] = nil
    end

    if hotkeyActions[action] then
      self.logger:vf('Binding spec %o for action %s', spec, action)
      hotkeys[action] = HwEvents.bindSpec(spec, hotkeyActions[action])
    else
      self.logger:wf('Unknown action: %s', action)
    end
  end

  return self
end

--- ImprovedCaffeine:init()
--- Method
--- Initialize this spoon
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:init()
  obj.logger = obj.createLogger(obj.name, obj.logLevel)
end

--- ImprovedCaffeine:start(mapping)
--- Method
--- Start the ImprovedCaffeine spoon, creating menubar icon, hotkey bindings, etc.
---
--- Parameters:
---  * mapping - (Optional) Hotkey mapping as described in ImprovedCaffeine:bindHotkeys(mapping). Overrides any hotkeys bound in a separate call to ImprovedCaffeine:bindHotkeys(mapping)
---
--- Returns:
---  * nil
function obj:start(mapping)
  obj.logger:df('Starting...')

  obj.logger:df('Creating menubar icon')
  menubar = hs.menubar.new()

  if mapping then
    obj:bindHotkeys(mapping)
  end

  updateMenubar()

  obj.logger:df('Started')
end

--- ImprovedCaffeine:destroy()
--- Method
--- Explicitly destroy this ImprovedCaffeine, menubar icon, hotkey bindings, etc. for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
  self.logger:df('Destroying...')

  self.logger:vf('Unbinding hotkeys')
  for k,hotkey in pairs(hotkeys) do
    hotkey:off()
  end
  hotkeys = nil

  self.logger:vf('Destroying menubar icon')
  if menubar then
    menubar:delete()
  end
  menubar = nil

  self.logger:vf('Destroyed')
  self.logger = nil

  self.createLogger = nil
end

return obj
