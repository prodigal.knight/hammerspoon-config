--- === ImprovedHCalendar ===
---
--- An improved variant of the HCalendar spoon, with more customization options and smarter rendering techniques
---
--- Download: None

local Canvas = require('EnhancedCanvas')
local HwEvents = require('HwEvents')
local utils = require('utils')

local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'ImprovedHCalendar'
obj.version = '1.0'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

obj.Corners = {
  topleft = 1,
  topright = 2,
  bottomleft = 3,
  bottomright = 4,
}

--- ImprovedHCalendar.utc
--- Variable
--- Controls whether the calendar uses local time or UTC to determine progress through the month
--- Defaults to false
obj.utc = false

--- ImprovedHCalendar.dayWidth
--- Variable
--- Control the width, in pixels, of a day in the calendar
--- Defaults to 24
obj.dayWidth = 24

--- ImprovedHCalendar.corner
--- Variable
--- Control which corner of the screen to display the calendar in
--- Defaults to `obj.Corners.bottomleft`
obj.corner = obj.Corners.bottomleft

--- ImprovedHCalendar.level
--- Variable
--- Control the level of the screen at which the calendar renders (above icons/below windows, above windows, etc.)
--- Defaults to `hs.canvas.windowLevels.desktopIcon`
obj.level = hs.canvas.windowLevels.desktopIcon

--- ImprovedHCalendar.titleFormat
--- Variable
--- Control the format of the calendar title
--- Defaults to "%B %Y" (e.g.: July 2021)
obj.titleFormat = '%B %Y'

--- ImprovedHCalendar.interval
--- Variable
--- Control the rate at which the calendar redraws itself
--- Defaults to 300 seconds (5 minutes)
obj.interval = 300

--- ImprovedHCalendar.showProgress
--- Variable
--- Control whether or not the centerline is shown as a solid line from start of month to current time, or just as the current day's portion of the line
obj.showProgress = false

--- ImprovedHCalendar.ui
--- Variable
--- Control UI rendering values for various items
obj.ui = {
  padding = 10,
  margin = 10,

  normalAlpha = 0.25,
  hoveredAlpha = 0.75,

  bgColor = '#000',

  title = {
    color = '#fff',
    font = '18px system',
  },
  days = {
    color = '#fff',
    font = '13px monospace',
  },

  currentDayBgColor = '#ffffff80',
  currentDayColor = '#00BAFF',
  currentDayMidlineColor = '#00BAFF',
  weekendColor = '#ff8080',
  midlineColor = '#00BAFF',
}

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

local SECONDS_PER_DAY = 86400

local canvas
local frame = { x = 0, y = 0, w = 0, h = 0 }
local hovered = false
local interval
local mousemoveListener
local primary

local DaysPerMonth = {
  function() return 31 end, -- January
  function(date) return utils.isLeapYear(date.year) and 29 or 28 end, -- February
  function() return 31 end, -- March
  function() return 30 end, -- April
  function() return 31 end, -- May
  function() return 30 end, -- June
  function() return 31 end, -- July
  function() return 31 end, -- August
  function() return 30 end, -- September
  function() return 31 end, -- October
  function() return 30 end, -- November
  function() return 31 end, -- December
}
local Weekdays = {'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'}

local function getCalendarTopLeft(frame, width, height)
  local margin = obj.ui.margin

  if obj.corner == obj.Corners.topleft then
    return margin, margin
  end

  local padding = obj.ui.padding

  local h = margin + height
  local w = margin + (padding * 2) + width

  if obj.corner == obj.Corners.bottomleft then
    return margin, frame.h - h
  elseif obj.corner == obj.Corners.topright then
    return frame.w - w, margin
  elseif obj.corner == obj.Corners.bottomright then
    return frame.w - w, frame.h - h
  else
    -- Unknown corner, put it in the default position, bottomleft
    return margin, frame.h - h
  end
end

-- Draw calendar
local function draw()
  obj.logger:df('(Re)drawing canvas')
  local ctx = canvas

  if not ctx then -- Create canvas if it doesn't exist
    obj.logger:vf('Creating new canvas')
    ctx = Canvas.new(primary:frame())

    ctx:behavior(hs.canvas.windowBehaviors.canJoinAllSpaces | hs.canvas.windowBehaviors.stationary)
    ctx:level(obj.level)
    ctx:show()

    canvas = ctx
  end

  local ctxFrame = ctx:getFrame()
  ctx:clearRect(0, 0, ctxFrame.w, ctxFrame.h)
  ctx:save()

  local ui = obj.ui
  local padding = ui.padding
  local title = ui.title
  local days = ui.days
  local midline = ui.midline
  local dayWidth = obj.dayWidth

  ctx.font = title.font
  local titleHeight = ctx:measureText('a').h

  ctx.font = days.font
  local dayHeight = ctx:measureText('a').h

  local fmtPrefix = obj.utc and '!' or ''
  local date = os.date(fmtPrefix .. '*t')

  local daysInMonth = DaysPerMonth[date.month](date)
  local width = daysInMonth * dayWidth + (padding * 2)
  local midlineHeight = math.floor(dayHeight / 4)
  local dayBgHeight = (dayHeight + midlineHeight) * 2
  local height = (padding * 3) + titleHeight + dayBgHeight

  ctx.globalAlpha = hovered and ui.hoveredAlpha or ui.normalAlpha

  local ctxFrame = ctx:getFrame()
  local x, y = getCalendarTopLeft(ctxFrame, width, height)

  -- Track calendar background position/size for hover effects
  frame = { x = ctxFrame.x + x, y = ctxFrame.y + y, w = width, h = height }

  ctx:translate(x, y)

  -- Draw background rectangle
  ctx.fillStyle = ui.bgColor
  ctx:fillRect(0, 0, width, height)

  ctx:translate(padding, padding)

  ctx.textBaseline = 'bottom'

  -- Draw title
  ctx.fillStyle = title.color
  ctx.font = title.font
  ctx:translate(0, titleHeight)
  ctx:fillText(os.date(fmtPrefix .. obj.titleFormat), 0, 0)

  ctx:translate(0, padding--[[ + titleHeight--]])

  local dayOfWeek = date.wday + 7 + (-date.day % 7) % 7 -- Calculate day of week for first day of month
  local midlineTop = dayHeight + (midlineHeight * 0.75)

  ctx:save()
  ctx.textAlign = 'center'
  ctx.font = days.font
  -- Draw days for month
  for day = 1, daysInMonth do
    -- FUTURE: Separate drawing instructions for each of: today, weekday, weekend?
    local weekday = Weekdays[(dayOfWeek + day - 1) % 7 + 1]
    local isToday = day == date.day
    local color = (weekday == 'Sa' or weekday == 'Su') and ui.weekendColor or days.color

    ctx.fillStyle = color

    if isToday then
      -- Draw highlighted background
      ctx.fillStyle = ui.currentDayBgColor
      ctx:fillRect(0, 0, dayWidth, dayBgHeight + padding / 2)

      -- Set color for current day text
      ctx.fillStyle = ui.currentDayColor
    end

    ctx:save()
    ctx:translate(0, dayHeight)
    -- Draw day abbreviation
    ctx:fillText(weekday, 0, 0, dayWidth)
    -- Draw day of month number
    ctx:fillText(tostring(day), 0, dayHeight + (midlineHeight * 2), dayWidth)
    ctx:restore()

    if isToday and not obj.showProgress then -- If "progress through month" not shown, draw just today's midline as the currentDayMidlineColor
      ctx.fillStyle = ui.currentDayMidlineColor
    else
      ctx.fillStyle = color
    end
    -- Draw midline
    ctx:fillRect(0, midlineTop, dayWidth, midlineHeight)

    ctx:translate(dayWidth, 0)
  end
  ctx:restore()

  -- Draw progress bar (if the user wants it)
  if obj.showProgress then
    local midlineWidth = (date.day + (((((date.hour * 60) + date.min) * 60) + date.sec) / SECONDS_PER_DAY) - 1) * dayWidth

    ctx.fillStyle = ui.midlineColor
    ctx:fillRect(0, midlineTop, midlineWidth, midlineHeight)
  end

  ctx:restore()
end

--- ImprovedHCalendar:init()
--- Method
--- Initialize this spoon
---
--- Parameters
---  * None
---
--- Returns:
---  * nil
function obj:init()
  obj.logger = obj.createLogger(obj.name, obj.logLevel)
  primary = hs.screen.primaryScreen()
end

--- ImprovedHCalendar:start()
--- Method
--- Create the canvas to draw on and start the redraw interval
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:start()
  obj.logger:df('Starting...')

  if interval ~= nil then
    interval:stop()
    interval = nil
  end

  obj.logger:df('Creating interval to redraw calendar')
  interval = hs.timer.doEvery(obj.interval, draw)
  interval:start()

  obj.logger:df('Scheduling initial deferred draw to canvas')
  utils.defer(draw)

  obj.logger:df('Creating mousemove listener for hover effects')
  mousemoveListener = HwEvents.on('mousemove', function(evt)
    if evt.screen ~= primary or not canvas then -- Ignore mouse movements on other screens or if the calendar hasn't rendered yet
      if hovered then
        canvas:level(obj.level)
        hovered = false

        draw()
      end

      return
    end

    local orig = hovered

    -- If cursor is inside calendar background frame, then apply hovered effects
    if utils.between(evt.screenX, frame.x, frame.x + frame.w) and utils.between(evt.screenY, frame.y, frame.y + frame.h) then
      canvas:level(hs.canvas.windowLevels.utility) -- Raise above windows but keep below dock/menubar/etc.
      hovered = true
    else
      canvas:level(obj.level) -- Lower back to original level
      hovered = false
    end

    if hovered ~= orig then -- Only redraw if hovered state has changed, otherwise the computer gets *really slow* while hovering
      draw()
    end
  end)

  obj.logger:df('Started')
end

--- ImprovedHCalendar:destroy()
--- Method
--- Explicitly destroy this ImprovedHCalendar and interval/canvas for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
  obj.logger:df('Destroying...')

  obj.logger:vf('Destroying mousemove listener')
  if mousemoveListener then
    mousemoveListener:off()
    mousemoveListener = nil
  end

  obj.logger:vf('Stopping redraw interval')
  if interval then
    interval:stop()
    interval = nil
  end

  obj.logger:vf('Destroying canvas')
  if canvas then
    canvas:delete()
    canvas = nil
  end

  obj.logger:vf('Destroyed')
  obj.logger = nil

  obj.createLogger = nil
end

return obj
