--- === ImprovedTurboBoost ===
---
--- An enhanced spoon to load/unload the Turbo Boost Disable kernel extension, based on ImprovedTurboBoost by Diego Zamboni
--- from https://github.com/rugarciap/Turbo-Boost-Switcher.
---
--- Note: this spoon by default uses sudo to load/unload the kernel
--- extension, so for it to work from Hammerspoon, you need to
--- configure sudo to be able to load/unload the extension without a
--- password, or configure the load_kext_cmd and unload_kext_cmd
--- variables to use some other mechanism that prompts you for the
--- credentials.
---
--- For example, the following configuration (stored in
--- /etc/sudoers.d/Improvedturboboost) can be used to allow loading and
--- unloading the module without a password:
--- ```
--- Cmnd_Alias    TURBO_OPS = /sbin/kextunload /Applications/Turbo Boost Switcher.app/Contents/Resources/DisableTurboBoost.64bits.kext, /usr/bin/kextutil /Applications/Turbo Boost Switcher.app/Contents/Resources/DisableTurboBoost.64bits.kext
---
--- %admin ALL=(ALL) NOPASSWD: TURBO_OPS
--- ```
---
--- If you use this, please support the author of Turbo Boost Disabler
--- by purchasing the Pro version of the app!
---
--- Download: [https://github.com/Hammerspoon/Spoons/raw/master/Spoons/ImprovedTurboBoost.spoon.zip](https://github.com/Hammerspoon/Spoons/raw/master/Spoons/ImprovedTurboBoost.spoon.zip)

local HwEvents = require('HwEvents')
local utils = require('utils')

local obj={}
obj.__index = obj

-- Metadata
obj.name = "ImprovedTurboBoost"
obj.version = "1.0"
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

--- ImprovedTurboBoost.disable_on_start
--- Variable
--- Boolean to indicate whether Turbo Boost should be disabled when the Spoon starts.
--- Defaults to `false`.
obj.disable_on_start = false

--- ImprovedTurboBoost.reenable_on_stop
--- Variable
--- Boolean to indicate whether Turbo Boost should be reenabled when the Spoon stops.
--- Defaults to `true`.
obj.reenable_on_stop = true

--- ImprovedTurboBoost.kext_paths
--- Variable
--- List with paths to check for the DisableTurboBoost.kext file. The first one to be found is used by default unless DisableTurboBoost.kext_path is set explicitly.
--- Defaults to `{"/Applications/Turbo Boost Switcher.app/Contents/Resources/DisableTurboBoost.64bits.kext", "/Applications/tbswitcher_resources/DisableTurboBoost.64bits.kext"}`
obj.kext_paths_to_try = {
  '/Applications/Turbo Boost Switcher.app/Contents/Resources/DisableTurboBoost.64bits.kext',
  '/Applications/tbswitcher_resources/DisableTurboBoost.64bits.kext',
}

--- ImprovedTurboBoost.kext_path
--- Variable
--- Where the DisableTurboBoost.kext file is located.
---
---Defaults to whichever one of `ImprovedTurboBoost.kext_paths` exists.
obj.kext_path = nil

--- ImprovedTurboBoost.load_kext_cmd
--- Variable
--- Command to execute to load the DisableTurboBoost kernel extension. This command must execute with root privileges and either query the user for the credentials, or be configured (e.g. with sudo) to run without prompting. The string "%s" in this variable gets replaced with the value of ImprovedTurboBoost.kext_path.
--- Defaults to `"/usr/bin/sudo /usr/bin/kextutil '%s'"`
obj.load_kext_cmd = "/usr/bin/sudo /usr/bin/kextutil '%s'"

--- ImprovedTurboBoost.unload_kext_cmd
--- Variable
--- Command to execute to unload the DisableTurboBoost kernel extension. This command must execute with root privileges and either query the user for the credentials, or be configured (e.g. with sudo) to run without prompting. The string "%s" in this variable gets replaced with the value of ImprovedTurboBoost.kext_path.
--- Defaults to `"/usr/bin/sudo /sbin/kextunload '%s'"`
obj.unload_kext_cmd = "/usr/bin/sudo /sbin/kextunload '%s'"

--- ImprovedTurboBoost.check_kext_cmd
--- Variable
--- Command to execute to check whether the DisableTurboBoost kernel extension is loaded.
--- Defaults to `"/usr/sbin/kextstat | grep com.rugarciap.DisableTurboBoost"`
obj.check_kext_cmd = '/usr/sbin/kextstat | grep com.rugarciap.DisableTurboBoost'

--- ImprovedTurboBoost.enabled_icon_path
--- Variable
--- Where to find the icon to use for the "Enabled" icon. Default value uses the icon from the Turbo Boost application:
--- Defaults to `"/Applications/Turbo Boost Switcher.app/Contents/Resources/icon.tiff"`
obj.enabled_icon_path = '/Applications/Turbo Boost Switcher.app/Contents/Resources/icon.tiff'

--- ImprovedTurboBoost.disabled_icon_path
--- Variable
--- Where to find the icon to use for the "Disabled" icon. Default value uses the icon from the Turbo Boost application:
--- Defaults to `"/Applications/Turbo Boost Switcher.app/Contents/Resources/icon_off.tiff"`
obj.disabled_icon_path = '/Applications/Turbo Boost Switcher.app/Contents/Resources/icon_off.tiff'

--- ImprovedTurboBoost.cmd_runner
--- Variable
--- A function to run commands that require elevated privileges
--- Defaults to a wrapper for `hs.execute` that returns arguments in the order they would be returned by `os.execute`
obj.cmd_runner = function(cmd) local out,status,type,rc = hs.execute(cmd) return status,out,type,rc end

local menuBarItem = nil
local wakeupWatcher = nil

local hotkeys = {}

--- ImprovedTurboBoost:setState(state, notify)
--- Method
--- Sets whether Turbo Boost should be disabled (kernel extension loaded) or enabled (normal state, kernel extension not loaded).
---
--- Parameters:
---  * state - A boolean, false if Turbo Boost should be disabled
---    (load kernel extension), true if it should be enabled (unload
---    kernel extension if loaded).
---  * notify - Optional boolean indicating whether a notification
---    should be produced. If not given, the value of
---    ImprovedTurboBoost.notify is used.
---
--- Returns:
---  * Boolean indicating new state
function obj.setState(state, notify)
  local curstatus = obj.status()
  if curstatus ~= state then
    local cmd = string.format(state and obj.unload_kext_cmd or obj.load_kext_cmd, obj.kext_path)
    obj.logger:df('Will execute command "%s"', cmd)
    local st,out,ty,rc = obj.cmd_runner(cmd)
    if not st then
      obj.logger:ef('Error executing "%s" (code %s). Output: %s', cmd, out, tostring(rc))
    else
      obj.setDisplay(state)
    end
  end
  return obj.status()
end

--- ImprovedTurboBoost:status()
--- Method
--- Check whether Turbo Boost is enabled
---
--- Parameters:
---  * None
---
--- Returns:
---  * true if Turbo Boost is enabled (kernel ext not loaded), false otherwise.
function obj.status()
  local cmd = obj.check_kext_cmd
  local st--[[,out,ty,rc--]] = os.execute(cmd) -- Doesn't require elevated privileges to run
  return (not st)
end

--- ImprovedTurboBoost:toggle()
--- Method
--- Toggle ImprovedTurboBoost status
---
--- Parameters:
---  * None
---
--- Returns:
---  * New ImprovedTurboBoost status, after the toggle
function obj.toggle()
  obj.setState(not obj.status())
  return obj.status()
end

local hotkeyActions = {
  toggle = function() obj.logger:vf('Toggle hotkey pressed') obj.toggle() end,
}

--- ImprovedTurboBoost:bindHotkeys(mapping)
--- Method
--- Binds hotkeys for ImprovedTurboBoost
---
--- Parameters:
---  * mapping - A table containing hotkey objifier/key details for the following items:
---   * toggle - Toggle the current status (i.e. turn Turbo Boost off if it is on, and vice versa)
function obj:bindHotkeys(mapping)
  self.logger:df('Binding hotkeys')
  self.logger:vf('Given mapping: %o', mapping)

  for action, spec in pairs(mapping) do
    if hotkeys[action] then
      hotkeys[action]:off()
      hotkeys[action] = nil
    end

    if hotkeyActions[action] then
      self.logger:vf('Binding spec %o for action %s', spec, action)
      hotkeys[action] = HwEvents.bindSpec(spec, hotkeyActions[action])
    else
      self.logger:wf('Unknown action: %s', action)
    end
  end

  return self
end

--- ImprovedTurboBoost:init()
--- Method
--- Initialize this spoon
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:init()
  obj.logger = obj.createLogger(obj.name, obj.logLevel)

  -- Load-time initialization of kext_path, so that it can be overriden by an
  -- explicit assignment later.
  for i,path in ipairs(obj.kext_paths_to_try) do
    if hs.fs.attributes(path) then
      obj.kext_path = path
    end
  end
end

--- ImprovedTurboBoost:start()
--- Method
--- Starts ImprovedTurboBoost
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:start(mapping)
  obj.logger:df('Starting...')

  obj.logger:df('Creating menubar item')
  menuBarItem = hs.menubar.new()
  menuBarItem:setClickCallback(self.clicked)

  obj.logger:df('Querying status for menubar item')
  obj.setDisplay(obj.status())

  obj.logger:df('Creating wakeup watcher')
  wakeupWatcher = hs.caffeinate.watcher.new(self.wokeUp):start()

  if mapping then
    obj:bindHotkeys(mapping)
  end

  if self.disable_on_start then
    obj.logger:df('Disabling turbo boost')
    obj.setState(false)
  end
end

--- ImprovedTurboBoost:destroy()
--- Method
--- Explicitly destroys this ImprovedTurboBoost and its menu bar item/wakeup watcher for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
  obj.logger:df('Destroying...')

  if self.reenable_on_stop then
    obj.logger:vf('Re-enabling turbo boost')
    obj.setState(true)
  end

  self.logger:vf('Unbinding hotkeys')
  for k,hotkey in pairs(hotkeys) do
    hotkey:off()
  end
  hotkeys = nil

  obj.logger:vf('Removing menubar item')
  if menuBarItem then
    menuBarItem:delete()
  end
  menuBarItem = nil

  obj.logger:vf('Stopping wake watcher')
  if wakeupWatcher then
    wakeupWatcher:stop()
  end
  wakeupWatcher = nil

  obj.logger:vf('Destroyed')
  obj.logger = nil

  obj.createLogger = nil
end

function obj.setDisplay(state)
  menuBarItem:setIcon(state and obj.enabled_icon_path or obj.disabled_icon_path)
end

function obj.clicked()
  utils.defer(function() obj.setDisplay(obj.toggle()) end)
end

-- This function is called when the machine wakes up and, if the
-- module was loaded, it unloads/reloads it to disable Turbo Boost
-- again
function obj.wokeUp(event)
  obj.logger:df('In obj.wokeUp, event = %d', event)
  if event == hs.caffeinate.watcher.systemDidWake then
    obj.logger:d('  Received systemDidWake event!')
    hs.timer.doAfter(0, function()
      if not obj:status() then
        obj.logger:df('  Toggling ImprovedTurboBoost on and back off')
        hs.timer.doAfter(0.5, function() obj:setState(true) end)
        hs.timer.doAfter(2.0, function() obj:setState(false) end)
      end
    end)
  end
end

return obj
