local Array = require('Array')
local Object = require('Object')

local JSON = {}

--------------------------------------------------------------------------------
--                                 Stringify                                  --
--------------------------------------------------------------------------------

local escapes = {
	['\\'] = '\\',
	['\"'] = '\"',
	['\b'] = 'b',
	['\f'] = 'f',
	['\n'] = 'n',
	['\r'] = 'r',
	['\t'] = 't',
}

local stringify

local function escape(c)
	return '\\' .. escapes[c] or string.format('u%04x', c:byte())
end

local function encode_object(val, replacer, stack)
	local res = Array:new()

	for k,v in pairs(val) do
		local keyType = type(k)
		if keyType == 'string' or keyType == 'number' then
			res:push(string.format(
				'%s:%s',
				stringify(k, tostring, stack),
				stringify(v, replacer, stack)
			))
		end
		-- Ignore invalid key types
	end

	return '{' .. res:join() .. '}'
end

local function encode_array(val, replacer, stack)
	local arr = Array
		.map(val, function(v) return stringify(v, replacer, stack) end)
		:join()
	return '[' .. arr .. ']'
end

local function encode_table(val, replacer, stack)
	local res

	-- Circular reference?
	if stack:includes(val) then
		error('Circular reference')
	end

	stack:push(val)

	if Array.isArray(val) then
		res = encode_array(val, replacer, stack)
	elseif Array.isArrayLike(val) then
		-- Treat as array
		for k in pairs(val) do
			if type(k) ~= 'number' then
				res = encode_object(val, replacer, stack)
			end
		end
		if not res then
			res = encode_array(val, replacer, stack)
		end
	else
		-- Treat as object
		res = encode_object(val, replacer, stack)
	end

	stack:pop()

	return res
end

local function encode_string(val)
	return '"' .. val:gsub('[%z\1-\31\\"]', escape) .. '"'
end

local function encode_number(val)
	if val ~= val or val <= -math.huge or val >= math.huge then
		error('unexpected number value "' .. tostring(val) .. '"')
	end
	return string.format('%.14g', val)
end

local encoding_map = {
	['nil'] = function() return 'null' end,
	['table'] = encode_table,
	['string'] = encode_string,
	['number'] = encode_number,
	['boolean'] = tostring,
	['function'] = function() return '"<function>"' end,
	['userdata'] = function() return '"<userdata>"' end,
	['thread'] = function() return '"<thread>"' end,
}

stringify = function(val, replacer, stack)
	local value
	if replacer then
		value = replacer(val)
	else
		value = val
	end
	local t = type(value)
	local f = encoding_map[t]
	if f then
		return f(value, replacer, stack)
	end
	error('unexpected type "' .. t .. '"')
end

function JSON.stringify(val, replacer, indentation)
	return stringify(val, replacer, Array:new())
end

--------------------------------------------------------------------------------
--                                   Parse                                    --
--------------------------------------------------------------------------------

local inv_escapes = {
	['/'] = '/',
}
for k,v in pairs(escapes) do
	inv_escapes[v] = k
end

local parse

local space_chars = Array:new(' ', '\t', '\r', '\n')
local delim_chars = Array:new(' ', '\t', '\r', ']', '}', ',')
local escape_chars = Array:new('\\', '/', '"', 'b', 'f', 'n', 'r', 't', 'u')
local literals = Array:new('true', 'false', 'null')

local literal_map = {
	['true'] = true,
	['false'] = false,
	['null'] = nil,
}

local function next_char(str, idx, set, negate)
	negate = negate or false
	for i = idx, #str do
		if set:includes(str:sub(i, i)) ~= negate then
			return i
		end
	end
	return #str + 1
end

local function parse_error(str, idx, msg)
	local line_count = 1
	local col_count = 1
	for i = 1, idx - 1 do
		col_count = col_count + 1
		if str:sub(i, i) == '\n' then
			line_count = line_count + 1
			col_count = 1
		end
	end
	error(string.format('%s at line %d col %d', msg, line_count, col_count))
end

local function codepoint_to_utf8(n)
	-- http://scripts.sil.org/cms/scripts/page.php?site_id/nrs&id=iws-appendixa
	local f = math.floor

	if n < 0x7F then
		return string.char(n)
	elseif n < 0x7FF then
		return string.char(f(n / 64) + 192, n % 64 + 128)
	elseif n < 0xFFFF then
		return string.char(f(n / 4096) + 224, f(n % 4096 / 64) + 128, n % 64 + 128)
	elseif n <= 0x10FFFF then
		return string.char(f(n / 262144) + 240, f(n % 262144 / 4096) + 128, f(n % 4096 / 64) + 128, n % 64 + 128)
	end
	error(string.format('invalid unicode codepoint %x', n))
end

function parse_unicode_escape(s)
	local n1 = tonumber(s:sub(1, 4), 16)
	local n2 = tonumber(s:sub(7, 10), 16)
	-- Surrogate pair?
	if n2 then
		return codepoint_to_utf8((n1 - 0xd800) * 0x400 + (n2 - 0xdc00) + 0x10000)
	else
		return codepoint_to_utf8(n1)
	end
end

local function parse_string(str, i)
	local res = ''
	local j = i + 1
	local k = j

	while k <= #str do
		local x = str:byte(j)

		if x < 32 then
			parse_error(str, j, 'control character in string')
		elseif x == 92 then -- `\`: Escape
			res = res .. str:sub(k, j - 1)
			j = j + 1
			local c = str:sub(j, j)
			if c == 'u' then
				local hex =
					str:match('^[dD][89aAbB]%x%x\\u%x%x%x%x', j + 1) or
					str:match('^%x%x%x%x', j + 1) or
					parse_error(str, j - 1, 'invalid unicode escape in string')
				res = res .. parse_unicode_escape(hex)
				j = j + #hex
			else
				if not escape_chars:includes(c) then
					parse_error(str, j - 1, 'invalid escape char "' .. c .. '" in string')
				end
				res = res .. inv_escapes[c]
			end
			k = j + 1
		elseif x == 34 then -- `"`: End of string
			res = res .. str:sub(k, j - 1)
			return res, j + 1
		end

		j = j + 1
	end

	parse_error(str, i, 'expected closing quote for string')
end

local function parse_number(str, i)
	local x = next_char(str, i, delim_chars)
	local s = str:sub(i, x - 1)
	local n = tonumber(s)
	if not n then
		parse_error(str, i, 'invalid number "' .. s .. '"')
	end
	return n, x
end

local function parse_literal(str, i)
	local x = next_char(str, i, delim_chars)
	local word = str:sub(i, x - 1)
	if not literals:includes(word) then
		parse_error(str, i, 'invalid literal "' .. word .. '"')
	end
	return literal_map[word], x
end

local function parse_array(str, i)
	local res = Array:new()
	local n = 1
	i = i + 1
	while 1 do
		local x
		i = next_char(str, i, space_chars, true)
		-- Empty / end of array?
		if str:sub(i, i) == ']' then
			i = i + 1
			break
		end
		-- Read token
		x, i = parse(str, i)
		res[n] = x
		n = n + 1
		-- Next token
		i = next_char(str, i, space_chars, true)
		local chr = str:sub(i, i)
		i = i + 1
		if chr == ']' then break end
		if chr ~= ',' then parse_error(str, i, 'expected "]" or ","') end
	end
	return res, i
end

local function parse_object(str, i)
	local res = {}
	i = i + 1
	while 1 do
		local key, val
		i = next_char(str, i, space_chars, true)
		-- Empty / end of object?
		if str:sub(i, i) == '}' then
			i = i + 1
			break
		end
		-- Read key
		if str:sub(i, i) ~= '"' then
			parse_error(str, i, 'expected string for object key')
		end
		key, i = parse(str, i)
		-- Read ':' delimiter
		i = next_char(str, i, space_chars, true)
		if str:sub(i, i) ~= ':' then
			parse_error(str, i, 'expected ":" after key')
		end
		i = next_char(str, i + 1, space_chars, true)
		-- Read value
		val, i = parse(str, i)
		-- Set
		res[key] = val
		-- Next token
		i = next_char(str, i, space_chars, true)
		local chr = str:sub(i, i)
		i = i + 1
		if chr == '}' then break end
		if chr ~= ',' then parse_error(str, i, 'expected "}" or ","') end
	end
	return res, i
end

local char_func_map = {
	['"'] = parse_string,
	['0'] = parse_number,
	['1'] = parse_number,
	['2'] = parse_number,
	['3'] = parse_number,
	['4'] = parse_number,
	['5'] = parse_number,
	['6'] = parse_number,
	['7'] = parse_number,
	['8'] = parse_number,
	['9'] = parse_number,
	['-'] = parse_number,
	['t'] = parse_literal,
	['f'] = parse_literal,
	['n'] = parse_literal,
	['['] = parse_array,
	['{'] = parse_object,
}

parse = function(str, idx)
	local chr = str:sub(idx, idx)
	local f = char_func_map[chr]
	if f then
		return f(str, idx)
	end
	parse_error(str, idx, 'unexpected character "' .. chr .. '"')
end

function JSON.parse(str, reviver)
	if type(str) ~= 'string' then
		str = tostring(str)
	end
	local res, idx = parse(str, next_char(str, 1, space_chars, true))
	idx = next_char(str, idx, space_chars, true)
	if idx <= #str then
		parse_error(str, idx, 'trailing garbage')
	end
	return res
end

return JSON
