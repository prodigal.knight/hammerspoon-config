all:
	cd EnhancedCanvas && $(MAKE)
	cd Snapshot       && $(MAKE)
	cd StatusOverview && $(MAKE)

clean:
	cd EnhancedCanvas && $(MAKE) clean
	cd Snapshot       && $(MAKE) clean
	cd StatusOverview && $(MAKE) clean

force:
	cd EnhancedCanvas && $(MAKE) force
	cd Snapshot       && $(MAKE) force
	cd StatusOverview && $(MAKE) force
