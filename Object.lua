local Array = require('Array')

-- Object utility functions inspired by JS Object
local Object = {}

-- Get the entries of an object as an array of arrays containing key-value pairs
function Object.entries(obj)
	local res = Array:new()

	for k,v in pairs(obj) do
		res:push(Array:new(k,v))
	end

	return res
end

-- Get the keys of an object as an array
function Object.keys(obj)
	local res = Array:new()

	for k,v in pairs(obj) do
		res:push(k)
	end

	return res
end

-- Get the values of an object as an array
function Object.values(obj)
	local res = Array:new()

	for k,v in pairs(obj) do
		res:push(v)
	end

	return res
end

-- Convert an array of arrays of key-value pairs into an object
function Object.fromEntries(arr)
	local res = {}

	arr:forEach(function(entry)
		res[entry[1]] = entry[2]
	end)

	return res
end

-- Assign all properties from the given objects to the target object, returning the target. Conflicting properties are always overridden by later objects.
function Object.assign(target, ...)
	for i,obj in ipairs(table.pack(...)) do
		for k,v in pairs(obj) do
			target[k] = v
		end
	end

	return target
end

return Object
