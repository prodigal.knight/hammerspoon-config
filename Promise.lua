local Array = require('Array')
local Logger = require('StyledLogger')
local Symbol = require('Symbol')
local utils = require('utils')

local Pending = Symbol('pending')
local Resolved = Symbol('resolved')
local Rejected = Symbol('rejected')

local log = Logger:new('Promise')

local Promise = {}
Promise.ID = utils.IDGenerator:new()
Promise.prototype = {}

local UNHANDLED_PROMISES = Array:new()

-- Check if the value given is a Promise
local function isPromise(value)
  return type(value) == 'table' and value.prototype == Promise.prototype
end

-- Check if the value given looks like a Promise
local function isPromiseLike(value)
  return type(value) == 'table' and type(value.next) == 'function'
end

-- Create a Promise that will be resolved with the given value. If the given value is a Promise, no new Promise is created.
function Promise.resolve(value)
  if isPromise(value) then
    return value
  end

  local promise = Promise:new()

  utils.defer(function() promise:__resolve(value) end)

  return promise
end

-- Create a Promise that will be rejected with the given value
function Promise.reject(value)
  local promise = Promise:new()

  utils.defer(function() promise:__reject(value) end)

  return promise
end

-- Create a Promise that is resolved when all given promises resolve, or immediately rejected if one or more reject.
function Promise.all(promises)
  local len = promises and #promises or 0

  if len == 0 then
    return Promise.resolve({})
  elseif len == 1 then
    return promises[1]:next(function(res) return Array:new(res) end)
  end

  local all = Promise:new()

  local results = Array:new(len)
  local done = 0

  for idx, promise in ipairs(promises) do
    if not isPromiseLike(promise) then
      results[idx] = promise
      done = done + 1

      if done == len then
        all:__resolve(results)
      end
    else
      promise:next(
        function(result)
          done = done + 1

          results[idx] = result

          if done < len then
            return
          end

          all:__resolve(results)
        end,
        function(err)
          all:__reject(err)
        end
      )
    end
  end

  return all
end

-- Create a Promise that is resolved when any of the given promises resolves, or rejected if none do
function Promise.any(promises)
  local len = promises and #promises or 0

  if len == 0 then
    return Promise.resolve()
  elseif len == 1 then
    return promises[1]
  end

  local any = Promise:new()

  local done = 0

  for idx, promise in ipairs(promises) do
    if not isPromiseLike(promise) then
      any:__resolve(promise)

      break
    else
      promise:next(
        function(result)
          any:__resolve(result)
        end,
        function()
          done = done + 1

          if done < len then
            return
          end

          any:__reject('No promises resolved')
        end
      )
    end
  end

  return any
end

-- Create a Promise that is resolved with an array of promise results when all given promises have fulfilled (whether resolved or rejected)
function Promise.allSettled(promises)
  local len = promises and #promises or 0

  if len == 0 then
    return Promise.resolve({})
  end

  local allSettled = Promise:new()

  local results = Array:new(len)
  local done = 0

  for idx, promise in ipairs(promises) do
    if not isPromiseLike(promise) then
      results[idx] = { status = 'fulfilled', value = promise }
      done = done + 1

      if done == len then
        allSettled:__resolve(results)
      end
    else
      promise
        :next(
          function(result)
            results[idx] = { status = 'fulfilled', value = result }
          end,
          function(err)
            results[idx] = { status = 'rejected', reason = err }
          end
        )
        :finally(function()
          done = done + 1

          if done < len then
            return
          end

          allSettled:__resolve(results)
        end)
    end
  end

  return allSettled
end

-- Create a Promise that is resolved/rejected with the result of the very first promise to resolve/reject
function Promise.race(promises)
  if not promises then
    return Promise.reject('Argument is nil')
  elseif not Array.isArrayLike(promises) then
    return Promise.reject('Argument of Promise.race is not iterable')
  end

  local len = #promises

  if len == 0 then
    return Promise.resolve()
  end

  local race = Promise:new()

  for idx, promise in ipairs(promises) do
    if not isPromiseLike(promise) then
      race:__resolve(promise)

      break
    else
      promise:next(
        function(result)
          race:__resolve(result)
        end,
        function(err)
          race:__reject(err)
        end
      )
    end
  end

  return race
end

-- Create a new Promise
-- NOTE: Dropped from spec: the ability to add fulfillment callbacks in the constructor
function Promise:new()
  local promise = {
    __id = Promise.ID:next(),

    state = Pending,
    value = nil,
    chain = Array:new(),
    rejectionHandlerId = nil
  }
  self.__index = self
  return setmetatable(promise, self)
end

-- Add callbacks for Promise resolution, rejection (optional)
function Promise:next(onResolved, onRejected)
  local next = Promise:new()

  self.chain:push({
    promise = next,
    onResolved = onResolved,
    onRejected = onRejected,
  })

  if self.state ~= Pending then
    utils.defer(function() self:__handle() end)
  end

  if self.rejectionHandlerId then
    next.rejectionHandlerId = self.rejectionHandlerId
  else
    UNHANDLED_PROMISES:push(true)
    next.rejectionHandlerId = #UNHANDLED_PROMISES
  end

  return next
end

-- Add a rejection handler
function Promise:catch(onRejected)
  return self:next(nil, onRejected)
end

-- Add a handler that is called regardless of Promise resolution/rejection
function Promise:finally(onFinished)
  return self:next(onFinished, onFinished)
end

-- Create a String representation of this Promise
function Promise:toString()
  return self:__tostring()
end

-- Create a String representation of this Promise
function Promise:__tostring()
  if self.state == Pending then
    return string.format(
      'Promise { <id>: %s, <state>: "%s", <children>: %s }',
      self.__id,
      self.state,
      self:__getChildren()
    )
  end

  return string.format(
    'Promise { <id>: %s, <state>: "%s", <%s>: %s, <children>: %s }',
    self.__id,
    self.state,
    self:__getStateValueRepr(),
    self.value,
    self:__getChildren()
  )
end

-- Private helper methods. DO NOT CALL THESE DIRECTLY
function Promise:__getStateValueRepr()
  return self.state == Resolved and 'value' or 'reason'
end

function Promise:__getChildren()
  return '[' .. self.chain:map(function(el) return el.promise.__id end):join(', ') .. ']'
end

function Promise:__resolve(value)
  self:__set(Resolved, value)
end

function Promise:__reject(value)
  self:__set(Rejected, value)
end

function Promise:__set(state, value)
  if self.state ~= Pending then -- A promise can only be resolved/rejected once
    return
  end

  if value == self and state == Resolved then
    self.value = Rejected
    self.value = 'cannot resolve to self'
  elseif isPromise(value) then
    if value.rejectionHandlerId then
      UNHANDLED_PROMISES[value.rejectionHandlerId] = nil
    end
    value.rejectionHandlerId = self.rejectionHandlerId

    if value.state == Pending then
      value:next(
        function(res)
          self:__resolve(res)

          return result
        end,
        function(err)
          self:__reject(err)
          value.rejectionHandlerID = nil

          return err
        end
      )

      return
    else
      self.state = value.state
      self.value = value.value
    end
  elseif isPromiseLike(value) then
    local first = true
    local status, res = pcall(
      value.next,
      function(value) self:__resolve(value) end,
      function(err) self:__reject(err) end
    )
    if not status and first then
      self:__reject(res)
    end

    return
  else
    self.state = state
    self.value = value
  end

  self:__handle()
end

function Promise:__handle()
  local rejected = self.state == Rejected

  self.chain:forEach(function(chained)
    local promise = chained.promise
    local onResolved = chained.onResolved
    local onRejected = chained.onRejected

    if rejected then
      if onRejected then
        local status, result = pcall(onRejected, self.value)

        if status then
          promise:__resolve(result)

          if self.rejectionHandlerId then
            UNHANDLED_PROMISES[self.rejectionHandlerId] = nil
          end
        else
          promise:__reject(result)
        end
      else
        promise:__reject(self.value)
      end
    elseif onResolved then
      local status, result = pcall(onResolved, self.value)

      if status then
        promise:__resolve(result)
      else
        promise:__reject(result)
      end
    else
      promise:__resolve(self.value)
    end
  end)

  self.chain = Array:new()
  if self.state == Rejected then
    local trace = debug.traceback()
    utils.defer(function()
      if UNHANDLED_PROMISES[self.rejectionHandlerId] then
        UNHANDLED_PROMISES[self.rejectionHandlerId] = nil
        log:error('Unhandled rejection: %s\n%s', self.value, trace)
      end
    end)
  end
end

return Promise
