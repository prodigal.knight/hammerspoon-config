local Array = require('Array')
local Canvas = require('EnhancedCanvas')
local HwEvents = require('HwEvents')
local Logger = require('StyledLogger')
local Object = require('Object')
local Promise = require('Promise')
local utils = require('utils')

local log = Logger:new('Reloadable')

local PromiseInterceptor = {}

function PromiseInterceptor:new(fn)
  local interceptor = {
    chain = Array:new(),
    fn = fn,
    active = nil,
  }
  self.__index = self
  return setmetatable(interceptor, self)
end

function PromiseInterceptor:next(onResolved, onRejected)
  self.chain:push({onResolved, onRejected})

  if self.active then
    self.active = self.active:next(onResolved, onRejected)
  end

  return self
end

function PromiseInterceptor:catch(onRejected)
  return self:next(nil, onRejected)
end

function PromiseInterceptor:finally(onFinished)
  return self:next(onFinished, onFinished)
end

function PromiseInterceptor:run()
  self.value = nil
  self.loaded = false

  local success, promise = pcall(self.fn)

  if not success then
    promise = Promise.reject(promise)
  end

  self.chain:forEach(function(step)
    promise = promise:next(table.unpack(step))
  end)

  self.active = promise
end

local Module = {}

function Module:new(modpath, promise)
  local module = {
    modpath = modpath,
    promise = promise,

    loaded = false,
    module = nil,
  }
  module.promise:next(
    function(res)
      local name, spoonOrModule = table.unpack(res)

      module.loaded = true
      module.module = spoonOrModule

      return res
    end,
    function(err)
      log:warn(err)

      return Promise.reject(err)
    end
  )
  self.__index = self
  return setmetatable(module, self)
end

function Module:load()
  utils.defer(function() self.promise:run() end)
end

function Module:reload()
  self:unload()
  self:load()
end

function Module:unload()
  if self.loaded and self.module.destroy then
    self.module:destroy()
  end

  self.loaded = false
  self.module = nil

  if package.loaded[self.modpath] then
    package.loaded[self.modpath] = nil -- Force a reload of the original module file instead of using the cached value
  end
  if spoon and spoon[self.modpath] then
    spoon[self.modpath] = nil -- Force a reload of the original spoon instead of using the cached value
  end
end

local modules = {}

local loaded = false

local function intercept(name, modpath, fn)
  if not loaded then -- Register a shutdown callback to destroy relodable modules
    loaded = true

    modules['All'] = Module:new(nil, PromiseInterceptor:new(function()
      log:info('Reloading all modules')
      Object.values(modules):forEach(function(module)
        if module.modpath ~= nil then
          module:reload()
        end
      end)

      return {'All', nil}
    end))
    modules['All'].loaded = true

    hs.shutdownCallback = function()
      for k,module in pairs(modules) do
        module:unload()
      end
    end
  end

  local module = Module:new(modpath, PromiseInterceptor:new(fn))

  modules[name] = module

  module:reload()

  return module.promise
end

local Reloadable = {}

function Reloadable.spoon(name)
  return intercept(name, name, function()
    local spoon = hs.loadSpoon(name, false) -- Do not load spoons into `hs.spoons` to be accessible anywhere

    return spoon and Promise.resolve({name, spoon}) or Promise.reject('Unable to find spoon ' .. name)
  end)
end

function Reloadable.module(name, path)
  local modpath = path or name

  return intercept(name, modpath, function()
    local module = require(modpath)

    if module then
      module.createLogger = utils.createLogger -- All my local modules have a createLogger member that takes a function
      module:init()                            -- Mimic Hammerspoon spoon loading API

      return Promise.resolve({name, module})
    end

    return Promise.reject('Unable to find module ' .. name)
  end)
end

function Reloadable.reload(name)
  local module = modules[name]

  if not module then
    log:error('Unknown module "%s". Make sure you have spelled the module name correctly.', name)

    return
  end

  module:reload()
end

local Picker = {
  splashColor = '#00000080',
  bgColor = '#1c1c1c',
  headerColor = '#f8f8f2',
  buttonLoadedModuleBgColor = '#636050',
  buttonUnoadedModuleBgColor = '#636050',
  buttonLoadedModuleTextColor = '#f8f8f2',
  buttonUnloadedModuleTextColor = '#f8f8f2',
  buttonLoadedModuleHoveredBgColor = '#f8f8f2',
  buttonUnoadedModuleHoveredBgColor = '#f8f8f2',
  buttonLoadedModuleHoveredTextColor = '#636050',
  buttonUnloadedModuleHoveredTextColor = '#636050',
}

local activePicker
local screen
local keyboardFocusIdx = 0
local hoveredButtonIdx = -1

local function getButtonBackgroundColorForState(loaded, focused)
  if loaded then
    return focused and Picker.buttonLoadedModuleHoveredBgColor or Picker.buttonLoadedModuleBgColor
  else
    return focused and Picker.buttonUnloadedModuleHoveredBgColor or Picker.buttonUnloadedModuleBgColor
  end
end

local function getButtonTextColorForState(loaded, focused)
  if loaded then
    return focused and Picker.buttonLoadedModuleHoveredTextColor or Picker.buttonLoadedModuleTextColor
  else
    return focused and Picker.buttonUnloadedModuleHoveredTextColor or Picker.buttonUnloadedModuleTextColor
  end
end

local function draw(ctx)
  local headerFont = '24px system'
  local headerText = 'Select a module/spoon to reload:'

  local moduleFont = '18px system'
  local moduleNames = Object.keys(modules):sort()

  local frame = ctx:getFrame()
  ctx:clearRect(0, 0, frame.w, frame.h)
  ctx.fillStyle = Picker.splashColor
  ctx:fillRect(0, 0, frame.w, frame.h)

  ctx:save()
  ctx.textBaseline = 'bottom'

  local centerX = frame.w / 2
  local centerY = frame.h / 2

  ctx.font = headerFont
  local headerRect = ctx:measureText(headerText)

  ctx:save()

  ctx.font = moduleFont
  local moduleRects = moduleNames:map(function(name) return ctx:measureText(name) end)

  ctx:restore()

  local width = math.max(350, headerRect.w, table.unpack(moduleRects:map(function(measure) return measure.w end)))
  local height = moduleRects:reduce(function(acc, measure) return acc + measure.h end, headerRect.h)
  local moduleButtonHeight = (height - headerRect.h) / #moduleRects

  local xPadding = utils.clamp(width * 0.025, 10, 25)
  local yPadding = utils.clamp(height * 0.025, 10, 25)

  local totalWidth = width + xPadding * 4
  local totalHeight = height + yPadding * (3 * (#moduleNames + 1))

  ctx:translate(centerX - (totalWidth / 2), centerY - (totalHeight / 2))

  ctx.fillStyle = Picker.bgColor
  ctx:fillRect(0, 0, totalWidth, totalHeight)

  ctx:translate(xPadding, yPadding * 1.5)

  ctx.fillStyle = Picker.headerColor
  ctx.textAlign = 'center'
  ctx:fillText(headerText, 0, headerRect.h, width + xPadding * 2)

  ctx:translate(0, headerRect.h + yPadding * 1.5)

  ctx.font = moduleFont
  local moduleTextHeight = ctx:measureText('a').h
  local buttons = moduleNames:map(function(name, idx)
    local module = modules[name]

    local bWidth = width + xPadding * 2
    local bHeight = moduleButtonHeight + yPadding * 2

    local focused = idx == keyboardFocusIdx or idx == hoveredButtonIdx

    ctx.fillStyle = getButtonBackgroundColorForState(module.loaded, focused)
    ctx:fillRect(0, 0, width + xPadding * 2, moduleButtonHeight + yPadding * 2)

    local moduleRect = moduleRects[idx]
    ctx.fillStyle = getButtonTextColorForState(module.loaded, focused)
    ctx:fillText(name, xPadding, yPadding + moduleTextHeight, width)

    local matrix = ctx:getTransform()
    local rect = { x = matrix.tX, y = matrix.tY, w = bWidth, h = bHeight }

    ctx:translate(0, moduleButtonHeight + yPadding * 3)

    return {
      module = module,
      rect = rect,
    }
  end)

  ctx:restore()

  return buttons
end

local function addListeners(canvas, buttons)
  local hoveredButton

  local keydown
  local keyup
  local mousedown
  local mousemove
  local mouseup

  local cleanup = function()
    keydown:off()
    keyup:off()
    mousedown:off()
    mousemove:off()
    mouseup:off()
    canvas:delete()
    activePicker = nil
  end

  keydown = HwEvents.on('keydown', function(evt)
    local key = evt.key
    local goup = key == 'ArrowUp' or key == 'ArrowLeft'
    local godown = key == 'ArrowDown' or key == 'ArrowRight'

    if goup or godown or key == 'Enter' or key == 'Escape' then
      evt:preventDefault()
      evt:stopPropagation()
    end

    if not goup and not godown then
      return
    end

    if goup then
      keyboardFocusIdx = keyboardFocusIdx - 1
      if keyboardFocusIdx < 1 then
        keyboardFocusIdx = #buttons
      end
    elseif godown then
      keyboardFocusIdx = keyboardFocusIdx + 1
      if keyboardFocusIdx > #buttons then
        keyboardFocusIdx = 1
      end
    end

    draw(canvas)
  end)

  keyup = HwEvents.on('keyup', function(evt)
    local key = evt.key

    if key == 'Escape' or key == 'Enter' then
      evt:preventDefault()
      evt:stopPropagation()

      cleanup()
    end

    if key == 'Enter' then
      buttons[keyboardFocusIdx].module:reload()
    end
  end)

  local getButtonUnderCursor = function(evt)
    return buttons:findIndex(function(button) return
      utils.between(evt.screenX, button.rect.x, button.rect.x + button.rect.w) and
      utils.between(evt.screenY, button.rect.y, button.rect.y + button.rect.h)
    end)
  end

  mousedown = HwEvents.on('mousedown', function(evt)
    if evt.button ~= 0 then -- Left mouse button
      return
    end

    evt:preventDefault()
    evt:stopPropagation()

    local idx = getButtonUnderCursor(evt)

    if idx ~= hoveredButtonIdx then
      hoveredButtonIdx = idx
      draw(canvas)
    end
  end)

  mouseup = HwEvents.on('mouseup', function(evt)
    if evt.button ~= 0 then -- Left mouse button
      return
    end

    local idx = getButtonUnderCursor(evt)

    if buttons[idx] then
      evt:preventDefault()
      evt:stopPropagation()

      cleanup()

      buttons[idx].module:reload()
    end
  end)

  mousemove = HwEvents.on('mousemove', function(evt)
    if evt.screen ~= screen then
      return
    end

    local idx = getButtonUnderCursor(evt)

    if idx ~= hoveredButtonIdx then
      hoveredButtonIdx = idx
      draw(canvas)
    end
  end)

  canvas:behavior(hs.canvas.windowBehaviors.canJoinAllSpaces | hs.canvas.windowBehaviors.stationary)
  canvas:show()
  canvas:bringToFront(true)
end

function Reloadable.showPicker()
  if activePicker then
    log:warn('There is already an active picker, not drawing another')

    return
  end

  screen = hs.mouse.getCurrentScreen()

  local frame = screen:fullFrame()

  local canvas = Canvas.new(frame)

  local buttons = draw(canvas)

  addListeners(canvas, buttons)

  activePicker = canvas
end

return Reloadable
