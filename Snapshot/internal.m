@import LuaSkin;

static const char *USERDATA_TAG = "Snapshot";

static NSImage *getSnapshotForWindow(CGWindowID windowID) {
  @autoreleasepool {
    CGRect windowRect = CGRectNull; // Get smallest rect containing window
    const void **values = (const void **)(void *)&windowID;
    CFArrayRef targetWindow = CFArrayCreate(NULL, values, 1, NULL);
    CGImageRef snapshot = CGWindowListCreateImageFromArray(
      windowRect,
      targetWindow,
      kCGWindowImageBoundsIgnoreFraming
    );
    CFRelease(targetWindow);

    NSImage *image = nil;

    if (snapshot) {
      image = [[NSImage alloc] initWithCGImage:snapshot size:windowRect.size];
      CFRelease(snapshot);
    }

    return image;
  }
}

static int forWindow(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TNUMBER, LS_TBREAK];

  CGWindowID windowID = (CGWindowID) lua_tointeger(L, 1);

  [skin pushNSObject:getSnapshotForWindow(windowID)];

  return 1;
}

// Functions for returned object when module loads
static luaL_Reg moduleLib[] = {
  {"forWindow", forWindow},

  {NULL,        NULL}
};

int luaopen_Snapshot_internal(lua_State* L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin registerLibrary:USERDATA_TAG functions:moduleLib metaFunctions:nil];

  return 1;
}
