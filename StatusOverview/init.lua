--- === StatusOverview ===
---
--- Display a quick view of CPU/RAM/Volume usage
---
--- Download: None

local Array = require('Array')
local Canvas = require('EnhancedCanvas')
local HwEvents = require('HwEvents')
local Object = require('Object')
local utils = require('utils')

local __StatusOverview = require('StatusOverview.internal');

local obj = {}
obj.__self = obj

-- Metadata
obj.name = 'StatusOverview'
obj.version = '1.0'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

obj.Corners = {
  topleft = 1,
  topright = 2,
  bottomleft = 3,
  bottomright = 4,
}

--- StatusOverview.corner
--- Variable
--- Control which corner of the screen to display the calendar in
--- Defaults to `obj.Corners.bottomleft`
obj.corner = obj.Corners.bottomright

--- StatusOverview.level
--- Variable
--- Control the level of the screen at which the calendar renders (above icons/below windows, above windows, etc.)
--- Defaults to `hs.canvas.windowLevels.utility`
obj.level = hs.canvas.windowLevels.utility

--- StatusOverview.interval
--- Variable
--- Update interval for getting stats. WARNING: Setting this below 1.0 causes very noticeable rendering delays (even at 1.0 there are occasional rendering delays)
--- Defaults to 2.0 seconds
obj.interval = 2.0

--- StatusOverview.showCPU
--- Variable
--- Display CPU usage as an overview, individually, or not at all. Valid values: 'overview', 'individual', or nil/false to not show at all
--- Defaults to false
obj.showCPU = false

--- StatusOverview.cpusPerRow
--- Variable
--- How many CPUs to display per row if StatusOverview.showCPU is set to 'individual'
--- Defaults to 4
obj.cpusPerRow = 4

--- StatusOverview.showCPUTemperatureGraph
--- Variable
--- Draw a graph for the CPU temperature. Ignored if StatusOverview.showCPU is nil or false
--- Defaults to false
obj.showCPUTemperatureGraph = false

--- StatusOverview.maxDrawnCPUTemperature
--- Variable
--- The maximum CPU temperature, in degrees celsius, for the purposes of drawing the CPU Temperature graph. If your CPU gets hotter than this, it will be clipped.
--- Defaults to 90.0 degrees celsius
obj.maxDrawnCPUTemperature = 90.0

--- StatusOverview.minDrawnCPUTemperature
--- Variable
--- The minimum CPU temperature, in degrees celsius, for the purposes of drawing the CPU Temperature graph. If your CPU gets cooler than this, it will be clipped.
--- Defaults to 50.0 degrees celsius
obj.minDrawnCPUTemperature = 50.0

--- StatusOverview.showGPUTemperatureGraph
--- Variable
--- Draw a graph for the GPU temperature. Ignored if StatusOverview.showGPU is false
--- Defaults to false
obj.showGPUTemperatureGraph = false

--- StatusOverview.maxDrawnGPUTemperature
--- Variable
--- As StatusOverview.maxDrawnCPUTemperature, but for the GPU
--- Defaults to 90.0 degrees celsius
obj.maxDrawnGPUTemperature = 90.0

--- StatusOverview.maxDrawnGPUTemperature
--- Variable
--- As StatusOverview.minDrawnCPUTemperature, but for the GPU
--- Defaults to 50.0 degrees celsius
obj.minDrawnGPUTemperature = 50.0

--- StatusOverview.showRAM
--- Variable
--- Display RAM usage overview
--- Defaults to false
obj.showRAM = false

--- StatusOverview.showVolumes
--- Variable
--- Display volume usage overview
--- Defaults to false
obj.showVolumes = false

--- StatusOverview.showVolumes
--- Variable
--- Display network usage overview
--- Defaults to false
obj.showNetwork = false

--- StatusOverview.networkUsesLogScale
--- Variable
--- Display network usage using a log scale for the vertical axis
--- Defaults to false
obj.networkUsesLogScale = false

--- StatusOverview.drawOrder
--- Variable
--- Determine the order (top to bottom) in which information is displayed
--- Defaults to `{'gpu', 'cpu', 'ram', 'volumes', 'network'}`
obj.drawOrder = {'gpu', 'cpu', 'ram', 'volumes', 'network'}

--- StatusOverview.duration
--- Variable
--- History duration of graphs
--- Defaults to 30.0 seconds
obj.duration = 30.0

--- StatusOverview.graphHeight
--- Variable
--- Determine graph height, in pixels
--- Defaults to 100
obj.graphHeight = 100

--- StatusOverview.graphWidth
--- Variable
--- Determine graph width, in pixels
--- Defaults to 450
obj.graphWidth = 450

--- StatusOverview.ui
--- Variable
--- Control UI rendering values for various items
obj.ui = {
  padding = 10,
  margin = 10,

  normalAlpha = 0.25,
  hoveredAlpha = 0.75,

  bgColor = '#000',
  titleColor = '#fff',
  titleFont = '16px system',
  detailColor = '#ccc',
  detailFont = '10px system',

  graphFillAlpha = 0.5,

  scaleLineColor = '#ffffff40',
  graphBorderColor = '#80808040',

  vramUsageColor = '#0f0',
  cpuUserColor = '#0f0',
  cpuSystemColor = '#f00',
  ramUsageColor = '#0f0',
  ramWiredColor = '#f00',
  ramAppUsageColor = '#00f',
  diskUsageColor = '#0f0',
  netRxColor = '#00f',
  netTxColor = '#f00',

  temperatureColorStops = {
    { position = 0,    color = '#ff0000' },
    { position = 0.15, color = '#ff8000' },
    { position = 0.33, color = '#ffff00' },
    { position = 0.9,  color = '#00ff00' },
    { position = 1,    color = '#0000ff' },
  },
}

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

local canvas
local frame = { x = 0, y = 0, w = 0, h = 0 }
local hovered = false
local mousemoveListener
local interval
local primary

local gpuTemperatureHistory
local cpuUsageHistory
local cpuTemperatureHistory
local ramHistory
local volumeHistory
local netHistory

local lastCpu
local lastActive = 0

local titleHeight
local detailHeight

local function getTopLeft(frame, width, height, ui)
  local margin = ui.margin

  if obj.corner == obj.Corners.topleft then
    return margin, margin
  end

  local padding = ui.padding

  local h = margin + height
  local w = margin + width

  if obj.corner == obj.Corners.bottomleft then
    return margin, frame.h - h
  elseif obj.corner == obj.Corners.topright then
    return frame.w - w, margin
  elseif obj.corner == obj.Corners.bottomright then
    return frame.w - w, frame.h - h
  else
    -- Unknown corner, put it in the default position, bottomright
    return frame.w - w, frame.h - h
  end
end

local function drawScale(ctx, min, max, interval)
  ctx:save()

  ctx:beginPath()
  ctx:rect(0, 0, obj.graphWidth, obj.graphHeight)
  ctx:clip()

  ctx:beginPath()
  local range = max - min
  for i = 0, range, interval do
    local yPos = obj.graphHeight - (i / range * obj.graphHeight)
    ctx:moveTo(0, yPos)
    ctx:lineTo(obj.graphWidth, yPos)
  end
  ctx:stroke()

  ctx:restore()
end

local function drawLogScale(ctx, max, logBase)
  drawScale(ctx, 0, math.log(max, logBase), 1)
end

local function drawLogScaleOnLinearGraph(ctx, max, logBase)
  ctx:save()

  ctx:beginPath()
  ctx:rect(0, 0, obj.graphWidth, obj.graphHeight)
  ctx:clip()

  ctx:beginPath()
  for i = 1, math.log(max, logBase) do
    local yPos = obj.graphHeight - (math.pow(logBase, i) / max * obj.graphHeight)
    ctx:moveTo(0, yPos)
    ctx:lineTo(obj.graphWidth, yPos)
  end
  ctx:stroke()

  ctx:restore()
end

local function drawGraph(ctx, color, percentages, ui, width)
  if not width then
    width = obj.graphWidth
  end

  local height = obj.graphHeight

  ctx:save()

  ctx:beginPath()
  ctx:rect(0, 0, width, height)
  ctx:clip()

  if ui.graphBorderColor then
    ctx.strokeStyle = ui.graphBorderColor
    ctx:stroke()
  end

  ctx.strokeStyle = color
  ctx.fillStyle = color

  local segments = #percentages - 1
  local segmentWidth = width / segments

  local heights = percentages:map(function(pct) return (1 - pct) * height end)

  ctx:beginPath()
  ctx:moveTo(0, heights[1])
  for i = 2, #heights do
    ctx:lineTo(segmentWidth * (i - 1), heights[i])
  end
  ctx:stroke()
  ctx:lineTo(segmentWidth * segments, height)
  ctx:lineTo(0, height)
  ctx:closePath()
  ctx.globalAlpha = ctx.globalAlpha * ui.graphFillAlpha
  ctx:fill()

  ctx:restore()
end

local function drawHeader(ctx, titleText, detailText, ui)
  ctx.textAlign = 'left'
  ctx.textBaseline = 'alphabetic'
  ctx.fillStyle = ui.titleColor
  ctx.font = ui.titleFont
  ctx:translate(0, titleHeight)
  ctx:fillText(titleText, 0, 0)

  ctx.textAlign = 'right'
  ctx.fillStyle = ui.detailColor
  ctx.font = ui.detailFont
  ctx:fillText(detailText, 0, 0, obj.graphWidth)

  ctx:translate(0, ui.padding * 0.5)
end

local function drawTemperatureGraph(ctx, history, temp, min, max, ui)
  if temp then
    history:push(temp)
    history:shift()
  end

  ctx:save()

  ctx:beginPath()
  ctx:rect(0, 0, obj.graphWidth, obj.graphHeight)
  ctx:clip()

  local range = max - min

  ctx.strokeStyle = ui.scaleLineColor
  drawScale(ctx, min, max, 10)

  local t = ctx:getTransform()
  local f = ctx:getFrame()
  local gradient = ctx:createLinearGradient(0, t.tY / f.h, 0, (t.tY + obj.graphHeight) / f.h);
  for i,stop in ipairs(ui.temperatureColorStops) do
    gradient:addColorStop(stop.position, stop.color)
  end
  drawGraph(ctx, gradient, history:map(function(point) return math.max((point - min) / range, 0) end), ui)

  ctx:restore()

  return obj.graphHeight
end

local function drawGPU(ctx, temp, ui)
  local temperature = temp or gpuTemperatureHistory[#gpuTemperatureHistory]

  drawHeader(ctx, 'GPU', string.format('%3.2f° C', temperature), ui)

  ctx:translate(0, drawTemperatureGraph(ctx, gpuTemperatureHistory, temp, obj.minDrawnGPUTemperature, obj.maxDrawnGPUTemperature, ui))
end

local function drawCPUOverview(ctx, ui)
  drawGraph(ctx, ui.cpuUserColor, cpuUsageHistory:map(function(entry) return entry.overall.active / 100 end), ui)
  drawGraph(ctx, ui.cpuSystemColor, cpuUsageHistory:map(function(entry) return entry.overall.system / 100 end), ui)

  return obj.graphHeight
end

local function drawCPUDetails(ctx, ui)
  local nProcessors = cpu and cpu.n or lastCpu.n
  local rows = math.ceil(nProcessors / obj.cpusPerRow)

  local x = 0
  local y = 0

  local padding = ui.padding
  local size = ctx:measureText('a').h

  local textColor = ui.detailColor
  local cpuUserColor = ui.cpuUserColor
  local cpuSystemColor = ui.cpuSystemColor

  local cpusPerRow = math.min(obj.cpusPerRow, nProcessors)
  local graphWidth = (obj.graphWidth - ((cpusPerRow - 1) * padding)) / cpusPerRow

  ctx.textAlign = 'right'
  ctx.textBaseline = 'top'

  local usage = cpuUsageHistory[#cpuUsageHistory]

  local originalHeight = obj.graphHeight
  obj.graphHeight = obj.graphHeight / 2

  for i = 1, usage.n do
    ctx:save()

    ctx:translate(x, y)

    drawGraph(ctx, cpuUserColor, cpuUsageHistory:map(function(entry) return entry[i].active / 100 end), ui, graphWidth)
    drawGraph(ctx, cpuSystemColor, cpuUsageHistory:map(function(entry) return entry[i].system / 100 end), ui, graphWidth)

    ctx:translate(0, obj.graphHeight + (padding * 0.5))

    ctx.fillStyle = textColor
    ctx:fillText(string.format('%3.2f%%', usage[i] and usage[i].active or 0), 0, 0, graphWidth)

    if i % cpusPerRow == 0 then
      x = 0
      y = y + obj.graphHeight + padding + size
    else
      x = x + graphWidth + padding
    end

    ctx:restore()
  end

  obj.graphHeight = originalHeight

  return (obj.graphHeight / 2 * rows) + ((rows - 1) * padding) + (rows * size)
end

local function drawCPU(ctx, cpu, temp, ui)
  if cpu then
    cpuUsageHistory:push(cpu)
    cpuUsageHistory:shift()
  end

  local usage = cpuUsageHistory[#cpuUsageHistory].overall.active
  local temperature = temp or cpuTemperatureHistory[#cpuTemperatureHistory]

  drawHeader(ctx, 'CPU', string.format('%3.2f%% | %3.2f° C', usage, temperature), ui)

  if obj.showCPUTemperatureGraph then
    ctx:translate(0, drawTemperatureGraph(ctx, cpuTemperatureHistory, temp, obj.minDrawnCPUTemperature, obj.maxDrawnCPUTemperature, ui))
    ctx:translate(0, ui.padding / 2)
  end

  ctx:save()

  local h = (obj.showCPU == 'individual')
    and drawCPUDetails(ctx, ui)
    or drawCPUOverview(ctx, ui)

  ctx:restore()
  ctx:translate(0, h)
end

local unitNames = {'B', 'KB', 'MB', 'GB', 'TB', 'PB'}

local function formatByteSize(pageSize, pages, label)
  local unit = 1

  local size = pages * pageSize

  while size >= 1024 do
    unit = unit + 1
    size = size / 1024
  end

  return string.format('%3.2f%s %s', size, unitNames[unit], label)
end

local function drawRAM(ctx, ram, ui)
  if ram then
    ramHistory:push(ram)
    ramHistory:shift()
  end

  local last = ramHistory[#ramHistory]
  local usage = last.pages ~= 0 and (last.pages - last.pagesFree) / last.pages * 100 or 0

  drawHeader(ctx, 'RAM', formatByteSize(last.pageSize, last.pages - last.pagesFree, string.format('(%3.2f%%) Total', usage)), ui)

  ctx.textBaseline = 'bottom'

  drawGraph(
    ctx,
    ui.ramUsageColor,
    ramHistory:map(function(entry)
      return (entry.pages - entry.pagesFree) / entry.pages
    end),
    ui
  )
  drawGraph(
    ctx,
    ui.ramAppUsageColor,
    ramHistory:map(function(entry)
      return (entry.pages - entry.pagesFree - entry.fileBackedPages) / entry.pages
    end),
    ui
  )
  drawGraph(
    ctx,
    ui.ramWiredColor,
    ramHistory:map(function(entry)
      return entry.pagesWiredDown / entry.pages
    end),
    ui
  )

  ctx.fillStyle = ui.detailColor
  local freeLessCachedFiles = last.pages - last.pagesFree - last.fileBackedPages
  ctx:fillText(
    formatByteSize(last.pageSize, last.fileBackedPages, 'Cached Files'),
    0,
    (1 - (freeLessCachedFiles / last.pages)) * obj.graphHeight - 1,
    obj.graphWidth - (ui.padding / 2)
  )
  ctx:fillText(
    formatByteSize(last.pageSize, freeLessCachedFiles - last.pagesWiredDown, 'Apps'),
    0,
    (1 - (last.pagesWiredDown / last.pages)) * obj.graphHeight - 1,
    obj.graphWidth - (ui.padding / 2)
  )
  ctx:fillText(
    formatByteSize(last.pageSize, last.pagesWiredDown, 'System'),
    0,
    obj.graphHeight - 1,
    obj.graphWidth - (ui.padding / 2)
  )

  ctx:translate(0, obj.graphHeight)
end

local function getVolumeUsagePercentage(volume)
  return (1 - (volume.NSURLVolumeAvailableCapacityKey / volume.NSURLVolumeTotalCapacityKey))
end

local function drawVolumes(ctx, volumes, ui)
  if not volumes then
    volumes = volumeHistory[#volumeHistory]
  else
    volumeHistory:push(volumes)
    volumeHistory:shift()
  end

  local capacities = Object.values(volumes)
    :map(function(entry) return {entry.NSURLVolumeAvailableCapacityKey, entry.NSURLVolumeTotalCapacityKey} end)
    :reduce(function(acc, entry) return {acc[1] + entry[1], acc[2] + entry[2]} end, {0, 0})
  local usage = (1 - (capacities[1] / capacities[2])) * 100

  drawHeader(ctx, 'Disk', string.format('Total: %3.2f%%', usage), ui)

  local baseline = ctx.textBaseline
  local padding = ui.padding
  ctx.textBaseline = 'top'

  for path,volume in pairs(volumes) do
    ctx:fillText(
      string.format('%s - %s: %3.2f%%', path, volume.NSURLVolumeNameKey, getVolumeUsagePercentage(volume)),
      0,
      0,
      obj.graphWidth
    )

    drawGraph(ctx, ui.diskUsageColor, volumeHistory:map(function(entry) return getVolumeUsagePercentage(entry[path]) end), ui)
    ctx:translate(0, obj.graphHeight + (padding * 0.5))
  end
  ctx.textBaseline = baseline
  ctx:translate(0, -(padding * 0.5))
end

local networkRates = {'b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb'}

local function formatNetworkRate(bytes, dir)
  local unit = 1
  rate = bytes * 8

  while rate >= 1024 do
    unit = unit + 1
    rate = rate / 1024
  end

  return string.format('%4.2f %sps %s', rate, networkRates[unit], dir)
end

local function drawNetwork(ctx, net, ui)
  if net then
    netHistory:push(net)
    netHistory:shift()
  end

  local last = netHistory[#netHistory]

  drawHeader(ctx, 'Network', string.format('%s ⥯ %s', formatNetworkRate(last.rxBytes, 'RX'), formatNetworkRate(last.txBytes, 'TX')), ui)
  local maxBytes = math.max(
    math.max(1, netHistory:map(function(entry) return entry.rxBytes end):spread()),
    math.max(1, netHistory:map(function(entry) return entry.txBytes end):spread())
  )
  local maxBytesLog = math.log(maxBytes, 2)

  ctx.strokeStyle = ui.scaleLineColor
  ctx.textBaseline = 'top'
  ctx.textAlign = 'left'
  local speed = math.pow(2, math.floor(maxBytesLog))
  if obj.networkUsesLogScale then
    drawLogScale(ctx, maxBytes, 2)
    ctx:fillText(formatNetworkRate(speed, ''), 0, obj.graphHeight - (math.floor(maxBytesLog) / maxBytesLog) * obj.graphHeight + 1)
  else
    drawLogScaleOnLinearGraph(ctx, maxBytes, 2)
    ctx:fillText(formatNetworkRate(speed, ''), 0, obj.graphHeight - (speed / maxBytes) * obj.graphHeight + 1)
  end
  drawGraph(
    ctx,
    ui.netRxColor,
    netHistory:map(obj.networkUsesLogScale
      and function(entry) return math.max(math.log(entry.rxBytes, 2), 0) / maxBytesLog end
      or function(entry) return (entry.rxBytes / maxBytes) end
    ),
    ui
  )
  drawGraph(
    ctx,
    ui.netTxColor,
    netHistory:map(obj.networkUsesLogScale
      and function(entry) return math.max(math.log(entry.txBytes, 2), 0) / maxBytesLog end
      or function(entry) return (entry.txBytes / maxBytes) end
    ),
    ui
  )

  ctx:translate(0, obj.graphHeight)
end

local sections = {
  gpu = {
    shouldDisplay = function() return obj.showGPUTemperatureGraph end,
    draw = function(ctx, ui, gpuTemp, cpu, temp, ram, volumes, net) drawGPU(ctx, gpuTemp, ui) end,
  },
  cpu = {
    shouldDisplay = function() return obj.showCPU end,
    draw = function(ctx, ui, gpuTemp, cpu, temp, ram, volumes, net) drawCPU(ctx, cpu, temp, ui) end,
  },
  ram = {
    shouldDisplay = function() return obj.showRAM end,
    draw = function(ctx, ui, gpuTemp, cpu, temp, ram, volumes, net) drawRAM(ctx, ram, ui) end,
  },
  volumes = {
    shouldDisplay = function() return obj.showVolumes end,
    draw = function(ctx, ui, gpuTemp, cpu, temp, ram, volumes, net) drawVolumes(ctx, volumes, ui) end,
  },
  network = {
    shouldDisplay = function() return obj.showNetwork end,
    draw = function(ctx, ui, gpuTemp, cpu, temp, ram, volumes, net) drawNetwork(ctx, net, ui) end,
  },
}

local function draw(gpuTemp, cpu, temp, ram, volumes, net)
  local ctx = canvas

  local ui = obj.ui

  if not ctx then -- Create canvas if it doesn't exist
    obj.logger:vf('Creating new canvas')
    ctx = Canvas.new(primary:frame())

    ctx:behavior(hs.canvas.windowBehaviors.canJoinAllSpaces | hs.canvas.windowBehaviors.stationary)
    ctx:level(obj.level)
    ctx:show()

    canvas = ctx

    ctx:save()

    -- Perform additional one-time setup steps
    -- Get heights of different fonts used during rendering once
    ctx.font = ui.titleFont
    titleHeight = ctx:measureText('a').h;
    ctx.font = ui.detailFont
    detailHeight = ctx:measureText('a').h;

    ctx:restore()
  end

  local margin = ui.margin
  local padding = ui.padding

  local width = obj.graphWidth + (padding * 2)
  local height = padding * 2 - margin

  if obj.showGPUTemperatureGraph then
    height = height + titleHeight + (padding * 0.5) + margin + obj.graphHeight
  end
  if obj.showCPU then
    if obj.showCPUTemperatureGraph then
      height = height + (padding * 0.5) + obj.graphHeight
    end

    local rows = math.ceil((cpu and cpu.n or lastCpu.n) / obj.cpusPerRow)
    height = height + titleHeight + (padding * 0.5) + margin + (
      obj.showCPU == 'individual'
        and (obj.graphHeight / 2 * rows) + ((rows - 1) * padding) + (rows * detailHeight)
        or obj.graphHeight
    )
  end
  if obj.showRAM then
    height = height + titleHeight + (padding * 0.5) + margin + obj.graphHeight
  end
  if obj.showVolumes then
    local nVolumes = #Object.keys(volumes and volumes or volumeHistory[#volumeHistory])
    height = height + titleHeight + margin + ((obj.graphHeight + (padding * 0.5)) * nVolumes)
  end
  if obj.showNetwork then
    height = height + titleHeight + (padding * 0.5) + margin + obj.graphHeight
  end

  local ctxFrame = ctx:getFrame()
  local x,y = getTopLeft(ctxFrame, width, height, ui)
  frame = { x = ctxFrame.x + x, y = ctxFrame.y + y, w = width, h = height }

  ctx:clearRect(0, 0, ctxFrame.w, ctxFrame.h)
  ctx:save()

  ctx.globalAlpha = hovered and ui.hoveredAlpha or ui.normalAlpha

  ctx:translate(x, y)
  ctx.fillStyle = ui.bgColor
  ctx:fillRect(0, 0, width, height)
  ctx:translate(padding, padding)

  for i,which in ipairs(obj.drawOrder) do
    local section = sections[which]

    if section.shouldDisplay() then
      section.draw(ctx, ui, gpuTemp, cpu, temp, ram, volumes, net)
      ctx:translate(0, margin)
    end
  end

  ctx:restore()
end

local function compareWithLastCpu(currentCpu)
  local res = {
    n = currentCpu.n -- This *shouldn't* differ between runs. If it does something has gone extremely wrong
  }

  for i = 1, currentCpu.n do
    local active = currentCpu[i].active - lastCpu[i].active
    local idle = currentCpu[i].idle - lastCpu[i].idle
    local total = active + idle

    res[i] = {
      user = ((currentCpu[i].user - lastCpu[i].user) / total) * 100,
      system = ((currentCpu[i].system - lastCpu[i].system) / total) * 100,
      nice = ((currentCpu[i].nice - lastCpu[i].nice) / total) * 100,
      active = (active / total) * 100,
      idle = (idle / total) * 100,
    }
  end

  local active = currentCpu.overall.active - lastCpu.overall.active
  local idle = currentCpu.overall.idle - lastCpu.overall.idle
  local total = active + idle
  res.overall = {
    user = ((currentCpu.overall.user - lastCpu.overall.user) / total) * 100,
    system = ((currentCpu.overall.system - lastCpu.overall.system) / total) * 100,
    nice = ((currentCpu.overall.nice - lastCpu.overall.nice) / total) * 100,
    active = (active / total) * 100,
    idle = (idle / total) * 100,
  }

  lastCpu = currentCpu
  lastActive = res.overall.active

  return res
end

local function compareWithLastNet(currentNet)
  local last = lastNetwork
  lastNetwork = currentNet

  return {
    rxBytes = currentNet.rxBytes - last.rxBytes,
    txBytes = currentNet.txBytes - last.txBytes,
    rxPackets = currentNet.rxPackets - last.rxPackets,
    txPackets = currentNet.txPackets - last.txPackets,
  }
end

local function getNetUsage(adapter)
  local status, out = utils.os.run('netstat -ibn | grep -e ' .. adapter .. ' -m 1');

  if not status then
    return { rxBytes = 0, txBytes = 0, rxPackets = 0, txPackets = 0 }
  end

  local rxPackets, rxBytes, txPackets, txBytes = out:match('(%d+)[ \t]+%d+[ \t]+(%d+)[ \t]+(%d+)[ \t]+%d+[ \t]+(%d+).*$')

  return { rxBytes = tonumber(rxBytes), txBytes = tonumber(txBytes), rxPackets = tonumber(rxPackets), txPackets = tonumber(txPackets) }
end

local function stat()
  utils.defer(collectgarbage) -- Keep Hammerspoon memory usage as low as possible by running the gc asap after this

  local gpuTemp = obj.showGPUTemperatureGraph and __StatusOverview.gpuTemperature() or nil
  local cpu = obj.showCPU and compareWithLastCpu(__StatusOverview.cpuUsageTicks()) or nil
  local temp = obj.showCPU and __StatusOverview.cpuTemperature() or nil
  local ram = obj.showRAM and __StatusOverview.ramUsage() or nil
  local volumes = obj.showVolumes and hs.host.volumeInformation() or nil
  local net = nil
  if obj.showNetwork then
    local ipv4, ipv6 = hs.network.primaryInterfaces()
    net = compareWithLastNet(getNetUsage(ipv4))
  end

  draw(gpuTemp, cpu, temp, ram, volumes, net)
end

--- StatusOverview:init()
--- Method
--- Initialize this spoon
---
--- Parameters
---  * None
---
--- Returns:
---  * nil
function obj:init()
  obj.logger = obj.createLogger(obj.name, obj.logLevel)
  primary = hs.screen.primaryScreen()
end

--- StatusOverview:start()
--- Method
--- Create the canvas to draw on and start the redraw interval
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:start()
  obj.logger:df('Starting...')

  if not (obj.showCPU or obj.showRAM or obj.showVolumes or obj.showNetwork) then
    error('Not configured to show anything')
  end

  nSamples = obj.duration / obj.interval

  if interval ~= nil then
    interval:stop()
    interval = nil
  end

  obj.logger:df('Creating query interval')
  interval = hs.timer.doEvery(obj.interval, stat)

  if obj.showGPUTemperatureGraph then
    gpuTemperatureHistory = Array:new(nSamples):fill(0)
  end

  if obj.showCPU then
    lastCpu = __StatusOverview.cpuUsageTicks()

    local defaultCpuUsageEntry = { overall = { active = 0, system = 0 }, n = lastCpu.n }
    for i = 1, lastCpu.n do
      defaultCpuUsageEntry[i] = { active = 0, system = 0 }
    end
    cpuUsageHistory = Array:new(nSamples):fill(defaultCpuUsageEntry)
    cpuTemperatureHistory = Array:new(nSamples):fill(0)
  end

  if obj.showRAM then
    ramHistory = Array:new(nSamples):fill({
      pages = 1,
      pagesFree = 1,
      fileBackedPages = 0,
      pagesWiredDown = 0,
    })
  end

  if obj.showVolumes then
    volumeHistory = Array:new(nSamples):fill(hs.host.volumeInformation())
  end

  if obj.showNetwork then
    local ipv4, ipv6 = hs.network.primaryInterfaces()
    lastNetwork = getNetUsage(ipv4)
    netHistory = Array:new(nSamples):fill({ rxBytes = 0, txBytes = 0, rxPackets = 0, txPackets = 0 })
  end

  obj.logger:df('Scheduling initial deferred draw to canvas')
  utils.defer(draw)

  obj.logger:df('Creating mousemove listener for hover effects')
  mousemoveListener = HwEvents.on('mousemove', function(evt)
    if evt.screen ~= primary or not canvas then -- Ignore mouse movements on other screens or if the overview hasn't rendered yet
      if hovered then
        canvas:level(obj.level)
        hovered = false

        draw()
      end

      return
    end

    local orig = hovered

    -- If cursor is inside overview background frame, then apply hovered effects
    if utils.between(evt.screenX, frame.x, frame.x + frame.w) and utils.between(evt.screenY, frame.y, frame.y + frame.h) then
      canvas:level(hs.canvas.windowLevels.overlay) -- Raise above windows but keep below dock/menubar/etc.
      hovered = true
    else
      canvas:level(obj.level) -- Lower back to original level
      hovered = false
    end

    if hovered ~= orig then -- Only redraw if hovered state has changed, otherwise the computer gets *really slow* while hovering
      draw()
    end
  end)

  obj.logger:df('Started')
end

--- StatusOverview:destroy()
--- Method
--- Explicitly destroy this StatusOverview and interval/canvas for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
  obj.logger:df('Destroying...')

  obj.logger:vf('Destroying mousemove listener')
  if mousemoveListener then
    mousemoveListener:off()
    mousemoveListener = nil
  end

  obj.logger:vf('Stopping query/redraw interval')
  if interval then
    interval:stop()
    interval = nil
  end

  obj.logger:vf('Destroying canvas')
  if canvas then
    canvas:delete()
    canvas = nil
  end

  if scratchpad then
    scratchpad:delete()
    scratchpad = nil
  end

  __StatusOverview.destroy()

  obj.logger:vf('Destroyed')
  obj.logger = nil

  obj.createLogger = nil
end

return obj
