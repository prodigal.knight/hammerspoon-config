@import Darwin.C.stdio;
@import Darwin.C.string;
@import Darwin.Mach.host_info;
@import Darwin.Mach.processor_info;
@import Darwin.Mach;
@import Darwin.POSIX.sys.types;
@import Darwin.sys.sysctl;
@import IOKit;
@import LuaSkin;

#include "smc.h"

// These values *shouldn't* be changing at runtime
static unsigned num_processors = 0;
static uint32_t page_size = 0;
static uint64_t mem_size = 0;
static uint32_t mem_pages = 0;

// Connection to SMC to get various temperatures & other readouts
static io_connect_t conn = 0;

#define CHECK_INIT(check) if (check == 0 && init(L) == 0) { return 0; }

// SMC Utility Functions
static uint32_t _strtoul(char *str, int size, int base) {
  uint32_t total = 0;

  for (int i = 0; i < size; i++) {
    if (base == 16) {
      total += (unsigned int) str[i] << (size - 1 - i) * 8;
    } else {
      total += (unsigned char) (str[i] << (size - 1 - i) * 8);
    }
  }

  return total;
}

static void _ultostr(char *str, uint32_t val) {
  str[0] = '\0';
  sprintf(str, "%c%c%c%c", (unsigned int) val >> 24, (unsigned int) val >> 16, (unsigned int) val >> 8, (unsigned int) val);
}

static kern_return_t SMCOpen(LuaSkin *skin) {
  kern_return_t result;
  io_iterator_t iterator;
  io_object_t device;

  CFMutableDictionaryRef matchingDictionary = IOServiceMatching("AppleSMC");
  result = IOServiceGetMatchingServices(kIOMasterPortDefault, matchingDictionary, &iterator);
  if (result != kIOReturnSuccess) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#SMCOpen() IOServiceGetMatchingServices() = %08x", result]];
    return 1;
  }

  device = IOIteratorNext(iterator);
  IOObjectRelease(iterator);
  if (device == 0) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#SMCOpen() No device found"]];
    return 1;
  }

  result = IOServiceOpen(device, mach_task_self(), 0, &conn);
  IOObjectRelease(device);
  if (result != kIOReturnSuccess) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#SMCOpen() IOServiceOpen = %08x", result]];
    return 1;
  }

  return kIOReturnSuccess;
}

static kern_return_t SMCClose() {
  return IOServiceClose(conn);
}

static kern_return_t SMCCall(uint32_t index, SMCKeyData_t *inputStructure, SMCKeyData_t *outputStructure) {
  size_t structureInputSize = sizeof(SMCKeyData_t);
  size_t structureOutputSize = sizeof(SMCKeyData_t);

#if MAC_OS_X_VERSION_10_5
  return IOConnectCallStructMethod(conn, index, inputStructure, structureInputSize, outputStructure, &structureOutputSize);
#else
  return IOConnectMethodStructureIStructureO(conn, structureInputSize, &structureOutputSize, inputStructure, outputStructure);
#endif
}

static kern_return_t SMCReadKey(UInt32Char_t key, SMCVal_t *val) {
  kern_return_t result;
  SMCKeyData_t inputStructure;
  SMCKeyData_t outputStructure;

  memset(&inputStructure, 0, sizeof(SMCKeyData_t));
  memset(&outputStructure, 0, sizeof(SMCKeyData_t));
  memset(val, 0, sizeof(SMCVal_t));

  inputStructure.key = _strtoul(key, 4, 16);
  inputStructure.data8 = SMC_CMD_READ_KEYINFO;

  result = SMCCall(KERNEL_INDEX_SMC, &inputStructure, &outputStructure);
  if (result != kIOReturnSuccess) {
    return result;
  }

  val->dataSize = outputStructure.keyInfo.dataSize;
  _ultostr(val->dataType, outputStructure.keyInfo.dataType);
  inputStructure.keyInfo.dataSize = val->dataSize;
  inputStructure.data8 = SMC_CMD_READ_BYTES;

  result = SMCCall(KERNEL_INDEX_SMC, &inputStructure, &outputStructure);
  if (result != kIOReturnSuccess) {
    return result;
  }

  memcpy(val->bytes, outputStructure.bytes, sizeof(outputStructure.bytes));

  return kIOReturnSuccess;
}

static double SMCGetTemperature(char *key) {
  SMCVal_t val;

  kern_return_t result = SMCReadKey(key, &val);

  if (result != kIOReturnSuccess || val.dataSize <= 0 || strcmp(val.dataType, DATATYPE_SP78) != 0) {
    return 0.0;
  }

  return (val.bytes[0] * 256 + (unsigned char) val.bytes[1]) / 256.0;
}

// Fetch static values that are unlikely to change at runtime once here, as well as creating a connection to SMC
static int init(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  int mib[2U] = { CTL_HW, HW_NCPU };
  size_t size = sizeof(num_processors);
  if (sysctl(mib, 2U, &num_processors, &size, NULL, 0U) != 0) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#init() error getting cpu count (%d): %s", errno, strerror(errno)]];
    return 0;
  }

  mib[1] = HW_PAGESIZE;
  size = sizeof(page_size);
  if (sysctl(mib, 2U, &page_size, &size, NULL, 0) < 0) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#init() error getting page size (%d): %s", errno, strerror(errno)]];
    return 0;
  }

  mib[1] = HW_MEMSIZE;
  size = sizeof(mem_size);
  if (sysctl(mib, 2U, &mem_size, &size, NULL, 0) < 0) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#init() error getting mem size (%d): %s", errno, strerror(errno)]];
    return 0;
  }

  mem_pages = (uint32_t) (mem_size / page_size);

  if (SMCOpen(skin) != kIOReturnSuccess) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#init() error opening SMC connection"]];
    return 0;
  }

  return 1;
}

static int destroy(__unused lua_State *L) {
  if (conn) {
    SMCClose();
  }
  conn = 0;

  num_processors = 0;
  page_size = 0;
  mem_size = 0;

  return 0;
}

static int ramUsage(lua_State *L) {
  CHECK_INIT(page_size)

  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TBREAK];

  mach_msg_type_number_t count = HOST_VM_INFO64_COUNT;

  vm_statistics64_data_t vm_stat;

  kern_return_t err = host_statistics64(mach_host_self(), HOST_VM_INFO64, (host_info_t) &vm_stat, &count);

  if (err != KERN_SUCCESS) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#ramUsage() error: %s", mach_error_string(err)]];
    return 0;
  }

  lua_newtable(L);
    lua_pushinteger(L, (vm_stat.free_count - vm_stat.speculative_count));             lua_setfield(L, -2, "pagesFree");
    lua_pushinteger(L, vm_stat.active_count);                                         lua_setfield(L, -2, "pagesActive");
    lua_pushinteger(L, vm_stat.inactive_count);                                       lua_setfield(L, -2, "pagesInactive");
    lua_pushinteger(L, vm_stat.speculative_count);                                    lua_setfield(L, -2, "pagesSpeculative");
    lua_pushinteger(L, vm_stat.throttled_count);                                      lua_setfield(L, -2, "pagesThrottled");
    lua_pushinteger(L, vm_stat.wire_count);                                           lua_setfield(L, -2, "pagesWiredDown");
    lua_pushinteger(L, vm_stat.purgeable_count);                                      lua_setfield(L, -2, "pagesPurgeable");
    lua_pushinteger(L, (lua_Integer) vm_stat.faults);                                 lua_setfield(L, -2, "translationFaults");
    lua_pushinteger(L, (lua_Integer) vm_stat.cow_faults);                             lua_setfield(L, -2, "pagesCopyOnWrite");
    lua_pushinteger(L, (lua_Integer) vm_stat.zero_fill_count);                        lua_setfield(L, -2, "pagesZeroFilled");
    lua_pushinteger(L, (lua_Integer) vm_stat.reactivations);                          lua_setfield(L, -2, "pagesReactivated");
    lua_pushinteger(L, (lua_Integer) vm_stat.purges);                                 lua_setfield(L, -2, "pagesPurged");
    lua_pushinteger(L, vm_stat.external_page_count);                                  lua_setfield(L, -2, "fileBackedPages");
    lua_pushinteger(L, vm_stat.internal_page_count);                                  lua_setfield(L, -2, "anonymousPages");
    lua_pushinteger(L, (lua_Integer) vm_stat.total_uncompressed_pages_in_compressor); lua_setfield(L, -2, "uncompressedPages");
    lua_pushinteger(L, vm_stat.compressor_page_count);                                lua_setfield(L, -2, "pagesUsedByVMCompressor");
    lua_pushinteger(L, (lua_Integer) vm_stat.decompressions);                         lua_setfield(L, -2, "pagesDecompressed");
    lua_pushinteger(L, (lua_Integer) vm_stat.compressions);                           lua_setfield(L, -2, "pagesCompressed");
    lua_pushinteger(L, (lua_Integer) vm_stat.pageins);                                lua_setfield(L, -2, "pageIns");
    lua_pushinteger(L, (lua_Integer) vm_stat.pageouts);                               lua_setfield(L, -2, "pageOuts");
    lua_pushinteger(L, (lua_Integer) vm_stat.swapins);                                lua_setfield(L, -2, "swapIns");
    lua_pushinteger(L, (lua_Integer) vm_stat.swapouts);                               lua_setfield(L, -2, "swapOuts");
    lua_pushinteger(L, (lua_Integer) vm_stat.lookups);                                lua_setfield(L, -2, "cacheLookups");
    lua_pushinteger(L, (lua_Integer) vm_stat.hits);                                   lua_setfield(L, -2, "cacheHits");
    lua_pushinteger(L, page_size);                                                    lua_setfield(L, -2, "pageSize");
    lua_pushinteger(L, (lua_Integer) mem_size);                                       lua_setfield(L, -2, "memSize");
    lua_pushinteger(L, mem_pages);                                                    lua_setfield(L, -2, "pages");
  return 1;
}

static int cpuUsageTicks(lua_State *L) {
  CHECK_INIT(num_processors)

  LuaSkin *skin = [LuaSkin sharedWithState:L];
  [skin checkArgs:LS_TBREAK];

  processor_info_array_t cpuInfo;
  mach_msg_type_number_t numCpuInfo;
  natural_t numCPUsU = 0U;

  mach_port_t host_port = mach_host_self();

  kern_return_t err = host_processor_info(host_port, PROCESSOR_CPU_LOAD_INFO, &numCPUsU, &cpuInfo, &numCpuInfo);

  if (err != KERN_SUCCESS) {
    [skin logError:[NSString stringWithFormat:@"StatusOverview.internal.m#cpuUsageTicks() error: %s", mach_error_string(err)]];
    return 0;
  }
  mach_port_deallocate(mach_task_self(), host_port);

  uint64_t overallInUser   = 0;
  uint64_t overallInSystem = 0;
  uint64_t overallInNice   = 0;
  uint64_t overallInIdle   = 0;
  uint64_t overallInUse    = 0;
  uint64_t overallTotal    = 0;

  lua_newtable(L);
  for (unsigned i = 0U; i < num_processors; ++i) {
    uint32_t inUser   = (uint32_t) cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER];
    uint32_t inSystem = (uint32_t) cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM];
    uint32_t inNice   = (uint32_t) cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE];
    uint32_t inIdle   = (uint32_t) cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE];
    uint32_t inUse    = inUser + inSystem + inNice;
    uint32_t total    = inUse + inIdle;

    overallInUser   += inUser;
    overallInSystem += inSystem;
    overallInNice   += inNice;
    overallInIdle   += inIdle;
    overallInUse    += inUse;
    overallTotal    += total;

    lua_newtable(L);
      lua_pushinteger(L, inUser);   lua_setfield(L, -2, "user");
      lua_pushinteger(L, inSystem); lua_setfield(L, -2, "system");
      lua_pushinteger(L, inNice);   lua_setfield(L, -2, "nice");
      lua_pushinteger(L, inUse);    lua_setfield(L, -2, "active");
      lua_pushinteger(L, inIdle);   lua_setfield(L, -2, "idle");
    lua_rawseti(L, -2, luaL_len(L, -2) + 1);
  }
  lua_newtable(L);
    lua_pushinteger(L, (lua_Integer) overallInUser);   lua_setfield(L, -2, "user");
    lua_pushinteger(L, (lua_Integer) overallInSystem); lua_setfield(L, -2, "system");
    lua_pushinteger(L, (lua_Integer) overallInNice);   lua_setfield(L, -2, "nice");
    lua_pushinteger(L, (lua_Integer) overallInUse);    lua_setfield(L, -2, "active");
    lua_pushinteger(L, (lua_Integer) overallInIdle);   lua_setfield(L, -2, "idle");
  lua_setfield(L, -2, "overall");
  vm_deallocate(mach_task_self(), (vm_address_t) cpuInfo, sizeof(integer_t) * numCpuInfo);

  lua_pushinteger(L, num_processors); lua_setfield(L, -2, "n");
  return 1;
}

static int cpuTemperature(lua_State *L) {
  CHECK_INIT(conn)

  lua_pushnumber(L, SMCGetTemperature(SMC_KEY_CPU_TEMP));
  return 1;
}

static int gpuTemperature(lua_State *L) {
  CHECK_INIT(conn)

  lua_pushnumber(L, SMCGetTemperature(SMC_KEY_GPU_TEMP));
  return 1;
}

static const luaL_Reg hostlib[] = {
  {"destroy",        destroy},
  {"ramUsage",       ramUsage},
  {"cpuUsageTicks",  cpuUsageTicks},
  {"cpuTemperature", cpuTemperature},
  {"gpuTemperature", gpuTemperature},

  {NULL, NULL}
};

static const luaL_Reg metalib[] = {
  {"__gc", destroy},

  {NULL, NULL}
};

extern int luaopen_StatusOverview_internal(lua_State *L) {
  LuaSkin *skin = [LuaSkin sharedWithState:L];

  [skin registerLibrary:"StatusOverview" functions:hostlib metaFunctions:metalib];

  return 1;
}
