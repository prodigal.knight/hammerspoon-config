local Array = require('Array')
local JSON = require('JSON')
local Object = require('Object')

local styledtext = hs.styledtext

-- Log levels
local Levels = {
	Nothing = 0,
	Fatal = 1,
	Error = 2,
	Warn = 3,
	Info = 4,
	Debug = 5,
	Verbose = 6,
}
-- Reverse mapped level names
local LevelNames = {}

Object.entries(Levels):forEach(function(entry)
	Levels[string.lower(entry[1])] = entry[2]
	LevelNames[entry[2]] = string.upper(entry[1])
end)

-- Log level colors
local Colors = {
	{ hex = '#f92672' }, -- FATAL
	{ hex = '#fd971f' }, -- ERROR
	{ hex = '#e6db74' }, -- WARN
	{ hex = '#66d9ef' }, -- INFO
	{ hex = '#7c7865' }, -- DEBUG
	{ hex = '#565656' }, -- VERBOSE
}

-- Colors for link underlines in logs (currently same as log level colors)
local LinkUnderlineColors = Colors

local function clamp(v, min, max)
	if v < min then
		return min
	elseif v > max then
		return max
	else
		return v
	end
end

-- A logger that represents different log levels with various colors when drawn to the Hammerspoon console
local StyledLogger = {}

StyledLogger.font = hs.console.consoleFont()

StyledLogger.defaultLevel = Levels.info

StyledLogger.Levels = Levels
StyledLogger.Colors = Colors
StyledLogger.LinkUnderlineColors = LinkUnderlineColors

-- Custom wrapper for `string.format` that supports `%o` as a format specifier for printing a JSON representation of an object
function StyledLogger.format(format, ...)
	local args = table.pack(...)
	local result = ''
	local i = 1
	local pos = 1

	while i <= #format do
		local c = format:sub(i, i)
		if c == '%' then
			local f = format:sub(i, i + 1)
			if f == '%o' then
				result = result .. JSON.stringify(args[pos])
			else
				result = result .. string.format(f, args[pos])
			end
			pos = pos + 1
			i = i + 1
		else
			result = result .. c
		end
		i = i + 1
	end

	return result
end

-- Create a new logger with the given name and level (or the default level if none is specified)
function StyledLogger:new(name, lvl)
	local logger = {
		name = name,
		level = StyledLogger.defaultLevel,
	}
	StyledLogger.setLogLevel(logger, lvl)

	self.__index = self

	return setmetatable(logger, self)
end

-- Get the current log level
function StyledLogger:getLogLevel()
	return self.level
end

-- Set the current log level
function StyledLogger:setLogLevel(lvl)
	local level = self.level

	if type(lvl) == 'string' then
		level = Levels[lvl:lower()]
	elseif type(lvl) == 'number' then
		level = clamp(lvl, Levels.Nothing, Levels.Verbose)
	end

	self.level = level or StyledLogger.defaultLevel
end

-- Print a message to the console with "Fatal" styling
function StyledLogger:fatal(format, ...)
	return self:printIf(Levels.Fatal, format, ...)
end

-- Print a message to the console with "Error" styling. e, ef are included for compatibility with hs.logger objects
function StyledLogger:error(format, ...)
	return self:printIf(Levels.Error, format, ...)
end

function StyledLogger:e(...)
	return self:error('', ...)
end

function StyledLogger:ef(...)
	return self:error(...)
end

-- Print a message to the console with "Warn" styling. w, wf are included for compatibility with hs.logger objects
function StyledLogger:warn(format, ...)
	return self:printIf(Levels.Warn, format, ...)
end

function StyledLogger:w(...)
	return self:warn('', ...)
end

function StyledLogger:wf(...)
	return self:warn(...)
end

-- Print a message to the console with "Info" styling. i, f are included for compatibility with hs.logger objects
function StyledLogger:info(format, ...)
	return self:printIf(Levels.Info, format, ...)
end

function StyledLogger:i(...)
	return self:info('', ...)
end

function StyledLogger:f(...)
	return self:info(...)
end

-- Print a message to the console with "Debug" styling. d, df are included for compatibility with hs.logger objects
function StyledLogger:debug(format, ...)
	return self:printIf(Levels.Debug, format, ...)
end

function StyledLogger:d(...)
	return self:debug('', ...)
end

function StyledLogger:df(...)
	return self:debug(...)
end

-- Print a message to the console with "Verbose" styling. v, vf are included for compatibility with hs.logger objects
function StyledLogger:verbose(format, ...)
	return self:printIf(Levels.Verbose, format, ...)
end

function StyledLogger:v(...)
	return self:verbose('', ...)
end

function StyledLogger:vf(...)
	return self:verbose(...)
end

-- Helper method for printing a message to the console if this logger is set to use a level equal to or higher than the given level
function StyledLogger:printIf(level, format, ...)
	if level <= self.level then
		self:printStyledToConsole(level, format, ...)
	end

	return self
end

-- Print the given method to the console using the styling for that level. If any arguments are functions, those functions are run here once it has been confirmed that the message should actually be printed to the console.
function StyledLogger:printStyledToConsole(level, format, ...)
	local args = table.pack(...)
	local fmt_type = type(format)

	if fmt_type ~= 'string' then
		args = table.pack(format, ...)
		format = ''
	end

	if format == '' then
		format = string.rep('%o', #args, ' ')
	end

	local interpolatedArgs = Array.map(args, function(arg)
		local argType = type(arg)

		local res = argtype == 'function' and arg() or arg

		local resType = type(res)

		if resType == 'function' or resType == 'thread' or resType == 'userdata' then
			return string.format('%s', arg)
		end

		return arg
	end)

	local text = styledtext.new(
		StyledLogger.format('%s: [%s] (%s) ' .. format, os.date('%Y-%m-%dT%H:%M:%S'), LevelNames[level], self.name, table.unpack(interpolatedArgs)),
		{ color = StyledLogger.Colors[level], font = StyledLogger.font }
	)

	local idx = 1

	while true do -- Style links in this message with underlines (they aren't clickable yet)
		local Start, End = text:find("https?://[%w%-%._~:/%?#%[%]@!%$&%'%*%+,;=]+", idx)

		if not Start then
			break
		end

		idx = End

		text = text:setStyle({ underlineColor = StyledLogger.LinkUnderlineColors[level], underlineStyle = styledtext.lineStyles.single }, Start, End)
	end

	hs.console.printStyledtext(text)
end

return StyledLogger
