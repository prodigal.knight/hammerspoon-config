--- === Switcher ===
---
--- Improved version of hs.switcher using hs.canvas, taking some cues from hs.expose implementation to reduce delay to bring up GUI
---
--- Download: None

local Snapshot = require('Snapshot.internal')

local Array = require('Array')
local Canvas = require('EnhancedCanvas')
local HwEvents = require('HwEvents')
local Logger = require('StyledLogger')
local Object = require('Object')
local Promise = require('Promise')
local Symbol = require('Symbol')
local utils = require('utils')

local windowFilter = hs.window.filter

local Boxes = Symbol('boxes')
local Cache = Symbol('cache')
local canvas = Symbol('canvas')
local Default = Symbol('default')
local Icons = Symbol('icons')
local Interval = Symbol('interval')
local Highlighted = Symbol('highlighted')
local OnKeyDown = Symbol('onkeydown')
local OnKeyUp = Symbol('onkeyup')
local OnMouseDown = Symbol('onmousedown')
local OnMouseMove = Symbol('onmousemove')
local OnMouseUp = Symbol('onmouseup')
local Screen = Symbol('screen')
local selected = Symbol('selected')
local Thumbnails = Symbol('thumbnails')
local UI = Symbol('UI')
local WF = Symbol('WF')
local Windows = Symbol('windows')

local log = Logger:new('Switcher')

local Unavailable = hs.image.imageFromName('NSStopProgressTemplate')

local Switcher = {}
Switcher.ui = {
  padding = 10,
  margin = 100,
  splashColor = '#00000040',

  textColor = '#fff',
  font = '16px Lucida Grande',

  backgroundColor = '#444',
  selectedApplicationBackgroundColor = '#cc8000cc',
  defaultApplicationBackgroundColor = '#00000000',

  thumbnailSize = 128,
}

function Switcher.nextWindow()
  return Switcher[Default]:next()
end

function Switcher.prevWindow()
  return Switcher[Default]:prev()
end

function Switcher.previousWindow() -- Compatibility with hs.window.switcher
  return Switcher.prevWindow()
end

function Switcher:new(wf, ui)
  local switcher = {
    [WF] = wf and hs.window.filter.new(wf) or hs.window.filter.default,
    [UI] = Object.assign({}, Switcher.ui, ui or {}),

    -- Canvas where everything is rendered once shown
    [canvas]      = nil,

    -- Event Listeners
    [OnKeyUp]     = nil,
    [OnKeyDown]   = nil,
    [OnMouseDown] = nil,
    [OnMouseMove] = nil,
    [OnMouseUp]   = nil,

    -- Miscellaneous data
    [Boxes]       = nil,
    [Highlighted] = -1,
    [Icons]       = nil,
    [selected]    = 1,
    [Thumbnails]  = nil,
    [Windows]     = nil,
  }
  self.__index = self
  return setmetatable(switcher, self)
end

function Switcher:next()
  return self:show(1)
end

function Switcher:prev()
  return self:show(-1)
end

function Switcher:previous() -- Compatibility with hs.window.switcher
  return self:prev()
end

function Switcher:show(dir)
  if not self[canvas] then -- Switcher has not been rendered previously, create a new canvas
    log:df('Showing switcher')
    log:vf('No previous canvas exists, creating new canvas')
    self[Screen] = hs.mouse.getCurrentScreen()
    local ctx = Canvas.new(self[Screen]:fullFrame())

    ctx:behavior(hs.canvas.windowBehaviors.canJoinAllSpaces | hs.canvas.windowBehaviors.stationary)
    ctx:level(hs.canvas.windowLevels.screenSaver)
    ctx:show()

    self[canvas] = ctx
  end

  if not self[Windows] then
    self[Windows] = self[WF]:getWindows(hs.window.filter.sortByFocusedLast)
  end

  if not self[Thumbnails] then
    self[Thumbnails] = {}

    local maxSize = self[UI].thumbnailSize

    Array.forEach(self[Windows], function(window)
      local id = window:id()

      local image = Snapshot.forWindow(id) or Unavailable
      local size = image:size()
      if size.w > maxSize or size.h > maxSize then
        image:size({ w = maxSize, h = maxSize })
      end

      self[Thumbnails][id] = image
    end)
  end

  if not self[Icons] then
    self[Icons] = {}

    Array.forEach(self[Windows], function(window)
      self[Icons][window:id()] = hs.image.imageFromAppBundle(window:application():bundleID())
    end)
  end

  if not self[OnMouseDown] then
    log:vf('Creating mousedown listener')

    self[OnMouseDown] = HwEvents.on('mousedown', function(evt)
      evt:preventDefault()
      evt:stopPropagation()
    end)
  end

  if not self[OnMouseMove] then
    log:vf('Creating mouse movement listener')

    self[OnMouseMove] = HwEvents.on('mousemove', function(evt)
      if evt.screen ~= self[Screen] then
        return
      end

      local boxes = self[Boxes]
      local ctx = self[canvas]

      if boxes then
        local highlighted = boxes:findIndex(function(box)
          return utils.between(evt.screenX, box.frame.x, box.frame.x + box.frame.w) and utils.between(evt.screenY, box.frame.y, box.frame.y + box.frame.h)
        end)

        if highlighted ~= self[Highlighted] then
          if highlighted ~= -1 then
            log:vf('Highlighted "%s"', self[Boxes][highlighted].window:title())
          end
          self[Highlighted] = highlighted

          self:__draw(self[Windows])
        end
      end
    end)
  end

  if not self[OnMouseUp] then
    log:vf('Creating mousedown listener')

    self[OnMouseUp] = HwEvents.on('mouseup', function(evt)
      if evt.button ~= 0 then -- left mouse button
        return
      end

      if self[Highlighted] ~= -1 then
        local focused = self[Boxes][self[Highlighted]].window
        log:vf('Mouseup on "%s", focusing and hiding switcher', focused:title())

        focused:unminimize()
        focused:focus()

        self:hide()
      end
    end)
  end

  if not self[OnKeyDown] then
    log:vf('Creating keydown listener for Esc exit without switching windows')
    self[OnKeyDown] = HwEvents.on('keydown', function(evt)
      if evt.key == 'Escape' then
        evt:preventDefault()
        evt:stopPropagation()

        self:hide()
      end
    end)
  end

  if not self[OnKeyUp] then
    log:vf('Creating keyup listener for all modifier keys being released')
    self[OnKeyUp] = HwEvents.on('keyup', function(evt)
      if not (evt.altKey or evt.ctrlKey or evt.metaKey or evt.shiftKey) then
        local focused = self[Windows][self[selected]]

        focused:unminimize()
        focused:focus()

        self:hide()
      end
    end)
  end

  self[selected] = utils.wrap(self[selected] + dir, 1, #self[Windows])
  utils.defer(function() self:__draw(self[Windows]) end) -- Defer drawing the canvas to improve performance in case the user releases the keybind immediately
end

function Switcher:__draw(windows)
  local ctx = self[canvas]
  local ui = self[UI]

  if not ctx then
    return
  end

  local frame = ctx:getFrame()
  ctx:clearRect(0, 0, frame.w, frame.h)
  -- ctx.textBaseline = 'bottom'

  ctx.font = ui.font

  -- Draw splash overlay
  ctx.fillStyle = ui.splashColor
  ctx:fillRect(0, 0, frame.w, frame.h)

  -- Draw background and windows
  self:__drawBgAndWindows(ctx, ui, frame, windows)
end

function Switcher:__drawBgAndWindows(ctx, ui, frame, windows)
  local centerX = frame.w / 2
  local centerY = frame.h / 2

  local maxWidth = frame.w - (ui.margin * 2)
  local maxHeight = frame.h - (ui.margin * 2)

  local rectWidth = ui.thumbnailSize + (ui.padding * 2)
  local rectHeight = math.floor(ui.thumbnailSize * 1.1) + (ui.padding * 3)

  local totalWidth = #windows * rectWidth
  local width = totalWidth

  while width > maxWidth do
    width = width - rectWidth
  end

  local height = (math.ceil(totalWidth / width) * rectHeight)

  if height > maxHeight then
    -- TODO: Figure out how to add a scrollbar if this happens
    log:warn('You have too many windows open, and they cannot all be fit in a single switcher on this screen. Vertical overflow will occur.')
  end

  local left = centerX - (width / 2)
  local top = centerY - (height / 2)

  ctx:save()
  ctx:translate(left, top)
  -- Draw background rectangle
  ctx.fillStyle = ui.backgroundColor
  ctx:fillRect(-ui.padding, -ui.padding, width + (ui.padding * 2), height + (ui.padding * 2))

  -- Draw windows
  self:__drawWindows(ctx, ui, left, top, width, windows)
  ctx:restore()
end

function Switcher:__drawWindows(ctx, ui, bgX, bgY, width, windows)
  ctx:save()

  local selectedIdx = self[selected]
  local highlightedIdx = self[Highlighted]

  local left = 0
  local top = 0
  local padding = ui.padding
  local dPadding = padding * 2
  local bgSize = ui.thumbnailSize + dPadding
  local iconSize = math.floor(ui.thumbnailSize / 10)

  local textHeight = ctx:measureText('a').h
  local boxes = Array.map(windows, function(win, i)
    local id = win:id()

    ctx.fillStyle = (i == selectedIdx or i == highlightedIdx) and ui.selectedApplicationBackgroundColor or ui.defaultApplicationBackgroundColor
    ctx:fillRect(left, top, bgSize, bgSize + iconSize + padding)

    ctx:drawImage(self[Icons][id], left + padding, top + padding, iconSize, iconSize)
    ctx.fillStyle = ui.textColor
    ctx:fillText(win:title(), left + dPadding + iconSize, top + padding + textHeight, bgSize - dPadding - padding - iconSize)

    local thumb = self[Thumbnails][id]
    local size = thumb:size()
    local windowTop = (bgSize - size.h) / 2
    local windowLeft = (bgSize - size.w) / 2
    ctx:drawImage(thumb, left + windowLeft, top + padding + iconSize + windowTop)

    local frame = { x = bgX + left, y = bgY + top, w = bgSize, h = bgSize + iconSize + padding }

    left = left + bgSize

    if left + bgSize > width then
      left = 0
      top = top + bgSize + iconSize + padding
    end

    return {
      frame = frame,
      window = win,
    }
  end)

  self[Boxes] = boxes

  ctx:restore()
end

local function destroyListener(switcher, listener)
  if switcher[listener] then
    log:vf('Destroying %s listener', listener)
    switcher[listener]:off()
    switcher[listener] = nil
  end
end

function Switcher:hide()
  log:df('Hiding switcher')
  if self[canvas] then
    log:vf('Destroying canvas')
    self[canvas]:delete()
    self[canvas] = nil
  end

  destroyListener(self, OnKeyUp)
  destroyListener(self, OnKeyDown)
  destroyListener(self, OnMouseDown)
  destroyListener(self, OnMouseMove)
  destroyListener(self, OnMouseUp)

  self[Boxes]       = nil
  self[Highlighted] = -1
  self[Icons]       = nil
  self[selected]    = 1
  self[Thumbnails]  = nil
  self[Windows]     = nil
end

Switcher[Default] = Switcher:new(nil)

return Switcher
