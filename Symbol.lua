local function toString(symbol)
  return symbol.name
end

-- Create a unique Symbol object that is considered to be "Private" and cannot be easily replicated
local function Symbol(name)
  local symbol = { name = name }

  return setmetatable(symbol, {
    __index = symbol,
    __tostring = toString,
  })
end

return Symbol
