--- === TruePushToTalk ===
---
--- Implements push-to-talk and push-to-mute functionality with `Fn` key (and additional as specified by end-user)
--- Based on PushToTalk spoon by Roman Khomenko, but simplified to the bare minimum
---
--- Download: None

local Array = require('Array')
local HwEvents = require('HwEvents')
local utils = require('utils')

local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'TruePushToTalk'
obj.version = '0.1'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

--- TruePushToTalk.pushTo
--- Variable
--- Adjust behavior when key is pushed. Valid values: 'talk', 'mute'
--- Defaults to 'talk'
obj.pushTo = 'talk'

--- TruePushToTalk.keys
--- Variable
--- Adjust the key(s) that trigger push-to-talk/push-to-mute functionality
--- Defaults to {'Fn'}
obj.keys = Array:new('Fn')

--- TruePushToTalk.indicateButtonPushed
--- Variable
--- Indicate using an HID LED when your microphone is unmuted. Valid values: refer to valid hs.hid.led.set "name" parameter values
--- Defaults to nil, meaning no LEDs will be toggled
obj.indicateButtonPushed = nil

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

local menubar = nil
local pushed = false

local keydown = nil
local keyup = nil

-- Update menubar icon, HID LED (if any), and mute/unmute the microphone as appropriate
local function updateMenuAndMutedStatus()
  local muted = (obj.pushTo == 'talk' and not pushed) or (obj.pushTo == 'mute' and pushed)

  if obj.indicateButtonPushed ~= nil then
    hs.hid.led.set(obj.indicateButtonPushed, not muted)
  end

  menubar:setMenu({
    { title = "Push-to-Talk", fn = function() setMode('talk') end, checked = obj.pushTo == 'talk', shortcut = 't' },
    { title = "Push-to-Mute", fn = function() setMode('mute') end, checked = obj.pushTo == 'mute', shortcut = 'm' },
  })

  menubar:setIcon(utils.path.absolute(muted and './resources/unrecord.pdf' or './resources/record.pdf', muted))

  hs.audiodevice.defaultInputDevice():setMuted(muted)
end

-- Set the current mode (called by menu callback)
local function setMode(pushTo)
  obj.pushTo = pushTo
  updateMenuAndMutedStatus()
end

-- Toggle pushed state (called by keydown/keyup listener)
local function togglePushed(state)
  pushed = state
  updateMenuAndMutedStatus()
end

-- Key down/up listener
local function toggleOnKeyEvent(evt)
  if obj.keys:some(function(key) return key == evt.key end) then
    evt:preventDefault()
    evt:stopPropagation()

    togglePushed(evt.type == 'keydown')
  end
end

--- TruePushToTalk:init()
--- Method
--- Initialize this spoon
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:init()
  obj.logger = obj.createLogger(obj.name, obj.logLevel)
end

--- TruePushToTalk:start()
--- Method
--- Create menubar entry and start listening for keydown/keyup in order to toggle mute/talk state
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:start()
  obj.logger:df('Starting...')

  obj.logger:df('Creating menubar icon')
  menubar = hs.menubar.new()

  obj.logger:df('Creating keydown/keyup listeners')
  keydown = HwEvents.on('keydown', toggleOnKeyEvent)
  keyup = HwEvents.on('keyup', toggleOnKeyEvent)

  obj.logger:df('Updating menubar state')
  updateMenuAndMutedStatus()

  obj.logger:df('Started')
end

--- TruePushToTalk:destroy()
--- Method
--- Explicitly destroy this TruePushToTalk, keydown/keyup listeners, and menubar entry for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
  self.logger:df('Destroying...')

  self.logger:vf('Removing keydown listener')
  if keydown then
    keydown:off()
  end
  keydown = nil

  self.logger:vf('Removing keyup listener')
  if keyup then
    keyup:off()
  end
  keyup = nil

  self.logger:vf('Removing menubar icon')
  if menubar then
    menubar:delete()
  end
  menubar = nil

  self.logger:vf('Destroyed')
  self.logger = nil

  self.createLogger = nil
end

return obj
