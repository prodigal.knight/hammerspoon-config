--- === WindowManip ===
---
--- Unix/Windows-style window manipulation and alt+tab behavior
---
--- Download: None

local Array = require('Array')
local HwEvents = require('HwEvents')
local Switcher = require('Switcher')
local Symbol = require('Symbol')
local utils = require('utils')

local bindSpec = hs.hotkey.bindSpec
local screenWatcher = hs.screen.watcher
local windowFilter = hs.window.filter

local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'WindowManip'
obj.version = '1.0'
obj.author = 'prodigal.knight <https://gitlab.com/prodigal.knight>'
obj.homepage = 'https://gitlab.com/prodigal.knight/hammerspoon-config/'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

--------------------------------------------------------------------------------
--                                 Utilities                                  --
--------------------------------------------------------------------------------

local Default = Symbol('Default')

local Screen = {
  Next = Symbol('Next'),
  Prev = Symbol('Prev'),
}

local Position = {
  BottomLeft  = Symbol('BottomLeft'),
  BottomRight = Symbol('BottomRight'),
  FullHeight  = Symbol('FullHeight'),
  HalfLeft    = Symbol('HalfLeft'),
  HalfRight   = Symbol('HalfRight'),
  Initial     = Symbol('Initial'),
  Last        = Symbol('Last'),
  Maximized   = Symbol('Maximized'),
  Minimized   = Symbol('Minimized'),
  TopLeft     = Symbol('TopLeft'),
  TopRight    = Symbol('TopRight'),
}

-- Window position setters
local WindowMovements = {
  -- Position window in bottom left quadrant of screen
  [Position.BottomLeft] = function(state, screen)
    local height = screen.h / 2

    state:move({ x = screen.x, y = screen.y + height, w = screen.w / 2, h = height })
  end,
  -- Position window in bottom right quadrant of screen
  [Position.BottomRight] = function(state, screen)
    local width = screen.w / 2
    local height = screen.h / 2

    state:move({ x = screen.x + width, y = screen.y + height, w = width, h = height })
  end,
  -- Move window back to its "initial" position, scaled to the current screen
  [Position.Initial] = function(state, screen)
    state:move(hs.geometry.copy(state.frame):fit(screen))
  end,
  -- Resize the window to take up the full height of the screen it is on
  [Position.FullHeight] = function(state, screen)
    local windowFrame = state.window:frame()

    state:move({ x = windowFrame.x, y = screen.y, w = windowFrame.w, h = screen.h })
  end,
  -- Position window on the left half of the screen
  [Position.HalfLeft] = function(state, screen)
    state:move({ x = screen.x, y = screen.y, w = screen.w / 2, h = screen.h })
  end,
  -- Position window on the right half of the screen
  [Position.HalfRight] = function(state, screen)
    local width = screen.w / 2

    state:move({ x = screen.x + width, y = screen.y, w = width, h = screen.h })
  end,
  -- Unminimize the window
  [Position.Last] = function(state, screen)
    state:unminimize()
  end,
  -- Maximize the window on the current screen
  [Position.Maximized] = function(state, screen)
    state:maximize()
  end,
  -- Minimize the window
  [Position.Minimized] = function(state, screen)
    state:minimize()
  end,
  -- Position window in top left quadrant of screen
  [Position.TopLeft] = function(state, screen)
    state:move({ x = screen.x, y = screen.y, w = screen.w / 2, h = screen.h / 2 })
  end,
  -- Position window in top right quadrant of screen
  [Position.TopRight] = function(state, screen)
    local width = screen.w / 2

    state:move({ x = screen.x + width, y = screen.y, w = width, h = screen.h / 2 })
  end,
}

-- Track window state
local WindowState = {}

function WindowState:new(window)
  local state = {
    frame = window:frame(),   -- Store window frame as seen by plugin when first moved
    last = Position.Initial,  -- Last window position
    pos = Position.Initial,   -- Current window position
    screen = window:screen(), -- Current screen
    window = window,          -- Tracked window
  }
  self.__index = self
  return setmetatable(state, self)
end

-- Update window state to new position (on given screen)
function WindowState:update(pos, screen)
  if pos == self.pos and screen == self.screen then
    obj.logger:df('New state/screen is same as current state/screen, ignoring')

    return
  elseif self.pos == Position.Minimized and pos ~= Position.Last then -- Ignore attempting to move the window to various positions while it is minimized
    obj.logger:df('Window is minimized and target position is not to restore last known position, ignoring')

    return
  end

  if pos == Position.Minimized then -- Track current window state when minimizing so it can be restored
    self.last = self.pos
  end

  obj.logger:df('Updating window position from %s to %s', self.pos, pos)
  self.pos = pos

  if screen ~= self.screen then -- Move this window to the indicated screen if necessary
    self.window:moveToScreen(screen)
    self.screen = screen
  end

  WindowMovements[pos](self, screen:frame()) -- Call the appropriate window movement handler
end

-- Maximize this WindowState's window
function WindowState:maximize()
  self.window:maximize()
end

-- Minimize this WindowState's window
function WindowState:minimize()
  self.window:minimize()
end

-- Move this WindowState's window into the given frame
function WindowState:move(frame)
  self.window:setFrameInScreenBounds(frame)
end

-- Unminimize this WindowState's window
function WindowState:unminimize()
  self.window:unminimize()
  self.pos = self.last -- Restore last position as current position
end

--------------------------------------------------------------------------------
--                               End Utilities                                --
--------------------------------------------------------------------------------

local lastWindow = nil
local hotkeys = {}
local windowState = {}

--- WindowManip.killAppWhenLastWindowClosed
--- Variable
--- Tell this spoon whether or not to kill applications when all of their windows are closed
--- Defaults to false
obj.killAppWhenLastWindowClosed = false

obj.logLevel = 'info'
obj.createLogger = function(name, level) return hs.logger.new(name, level) end
obj.logger = nil

-- Ignore (do not kill) the following applications if their last window is closed and `obj.killAppWhenLastWindowClosed` is true
local doNotQuitApplicationIfNoWindowsRemain = Array:new('Hammerspoon', 'System Preferences', 'Control Center')

-- Callbacks for various window events
local WindowSubscriptions = {
  -- Clean up tracked state (if any) when a window is closed. If `obj.killAppWhenLastWindowClosed` is true, then also kill the application if it is not ignored and that was its last open window.
  [hs.window.filter.windowDestroyed] = function(window, appName, event)
    local id = window:id()

    obj.logger:vf('Window %s was destroyed', id)

    windowState[id] = nil

    if not obj.killAppWhenLastWindowClosed then
      obj.logger:vf('Not quitting applications automatically')

      return
    end

    if doNotQuitApplicationIfNoWindowsRemain:includes(appName) or hs.window.filter.ignoreAlways[appName] then
      obj.logger:vf('Ignoring application "%s"', appName)

      return
    end

    local app = window:application() or hs.application.get(appName)

    if not app then
      obj.logger:ef('Something went terribly wrong, the window that was just closed seems to not be associated with an application (given appName: "%s")', appName)
    elseif #app:allWindows() == 0 then
      obj.logger:f('Killing "%s" because all of its windows have been closed', appName)
      app:kill()
    end
  end,
  -- When a window is unminimized, if it has a tracked state, mark that state as unminimized so that it can be moved around using the keyboard shortcuts
  [hs.window.filter.windowUnminimized] = function(window, appName, event)
    local id = window:id()

    obj.logger:vf('Window %s was unminimized', id)

    if windowState[id] then
      windowState[id]:unminimize()
    end
  end,
}

-- Update current window screen when displays are added/removed
local function rescan()
  obj.logger:df('Screen layout change detected. Rescanning current screens')
  for k,v in pairs(windowState) do
    v.screen = v.window:screen():id()
  end
end

-- Returns a callback that will adjust the given WindowState to the given position (and screen adjustment direction)
local function moveTo(pos, dir)
  -- Move the given WindowState to the given position, switching screens if necessary
  return function(state, screen)
    if dir == Screen.Next then
      obj.logger:vf('Movement from %s to %s requires switching to next screen', state.pos, pos)
      state:update(pos, screen:next())
    elseif dir == Screen.Prev then
      obj.logger:vf('Movement from %s to %s requires switching to previous screen', state.pos, pos)
      state:update(pos, screen:previous())
    else
      state:update(pos, screen)
    end
  end
end

-- Movement handlers for various current positions
local Movements = {
  Down = {
    [Default] = moveTo(Position.Minimized),

    [Position.HalfLeft] = moveTo(Position.BottomLeft),
    [Position.HalfRight] = moveTo(Position.BottomRight),
    [Position.Maximized] = moveTo(Position.Initial),
    [Position.TopLeft] = moveTo(Position.HalfLeft),
    [Position.TopRight] = moveTo(Position.HalfRight),
  },
  Left = {
    [Default] = moveTo(Position.HalfLeft),

    [Position.BottomLeft] = moveTo(Position.BottomRight, Screen.Prev),
    [Position.BottomRight] = moveTo(Position.BottomLeft),
    [Position.HalfLeft] = moveTo(Position.HalfRight, Screen.Prev),
    [Position.HalfRight] = moveTo(Position.Initial),
    [Position.Maximized] = moveTo(Position.Maximized, Screen.Prev),
    [Position.TopLeft] = moveTo(Position.TopRight, Screen.Prev),
    [Position.TopRight] = moveTo(Position.TopLeft),
  },
  Right = {
    [Default] = moveTo(Position.HalfRight),

    [Position.BottomLeft] = moveTo(Position.BottomRight),
    [Position.BottomRight] = moveTo(Position.BottomLeft, Screen.Next),
    [Position.HalfLeft] = moveTo(Position.Initial),
    [Position.HalfRight] = moveTo(Position.HalfLeft, Screen.Next),
    [Position.Maximized] = moveTo(Position.Maximized, Screen.Next),
    [Position.TopLeft] = moveTo(Position.TopRight),
    [Position.TopRight] = moveTo(Position.TopLeft, Screen.Next),
  },
  Up = {
    [Default] = moveTo(Position.Maximized),

    [Position.BottomLeft] = moveTo(Position.HalfLeft),
    [Position.BottomRight] = moveTo(Position.HalfRight),
    [Position.HalfLeft] = moveTo(Position.TopLeft),
    [Position.HalfRight] = moveTo(Position.TopRight),
    [Position.Minimized] = moveTo(Position.Last),
  },

  FullHeight = {
    [Default] = moveTo(Position.FullHeight),

    [Position.BottomLeft] = moveTo(Position.HalfLeft),
    [Position.BottomRight] = moveTo(Position.HalfRight),
    [Position.HalfLeft] = utils.nop,
    [Position.HalfRight] = utils.nop,
    [Position.Maximized] = utils.nop,
    [Position.TopLeft] = moveTo(Position.HalfLeft),
    [Position.TopRight] = moveTo(Position.HalfRight),
  },
}

-- Handle movement of the focused window appropriately
local function handleMovement(actions)
  local window = hs.window.focusedWindow() or lastWindow

  if window then
    lastWindow = window
    local id = window:id()
    obj.logger:vf('Focused window: %s', id)
    if not windowState[id] then
      obj.logger:df('Creating persistent stored state for window: %s', id)
      windowState[id] = WindowState:new(window)
    end

    local screen = window:screen()
    local windowState = windowState[id]

    local action = actions[windowState.pos]

    if action then -- If there is an explicit handler for a given state, call that
      obj.logger:vf('There is a handler for window state: %s, calling it', windowState.pos)
      action(windowState, screen)
    elseif actions[Default] then -- Otherwise, call the default handler (if any)
      obj.logger:vf('There is no handler for window state: %s, calling default handler', windowState.pos)
      actions[Default](windowState, screen)
    else -- Otherwise something has gone terribly wrong, because they should all have default handlers
      obj.logger:wf('No handler for current window state: %s', windowState.pos)
    end
  end
end

local hotkeyActions = {
  full_height = function() handleMovement(Movements.FullHeight) end,
  move_down = function() handleMovement(Movements.Down) end,
  move_left = function() handleMovement(Movements.Left) end,
  move_right = function() handleMovement(Movements.Right) end,
  move_up = function() handleMovement(Movements.Up) end,
  next_window = function() obj.switcher:next() end,
  prev_window = function() obj.switcher:previous() end,
}

--- WindowManip:bindHotkeys(mapping)
--- Method
--- Bind hotkeys for WindowManip
---
--- Parameters:
---  * mapping - A table containing the hotkey bindSpecs for the following (as described in hs.hotkey.bindSpec):
---   * full_height - Make the focused window take the full height of the screen it is on, without adjusting window x, width, or height
---   * move_down   - Make the window take on a smaller size lower on the screen, incrementally, until it is minimized
---   * move_left   - Move the window left by steps: left half the screen, right half of previous screen, scaled position on previous screen, etc.
---   * move_right  - Move the window right by steps: right half of screen, left half of next screen, scaled position on next screen, etc.
---   * move_up     - Make the window take on a larger size or higher position on the screen, incrementally, until it is maximized
---   * next_window - hs.window.switcher:next()
---   * prev_window - hs.window.switcher:previous()
---
--- Returns:
---  * The WindowManip object
function obj:bindHotkeys(mapping)
  self.logger:df('Binding hotkeys')
  self.logger:vf('Given mapping: %o', mapping)

  for action,spec in pairs(mapping) do
    if hotkeys[action] then
      hotkeys[action]:off()
      hotkeys[action] = nil
    end

    if hotkeyActions[action] then
      self.logger:vf('Binding spec %o for action %s', spec, action)
      hotkeys[action] = HwEvents.bindSpec(spec, hotkeyActions[action])
    else
      self.logger:wf('Unknown action: %s', action)
    end
  end

  return self
end

--- WindowManip:init()
--- Method
--- Initialize this spoon
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:init()
  self.logger = self.createLogger(self.name, self.logLevel)
end

--- WindowManip:ignore(...)
--- Method
--- Ignore (do not kill) additional applications beyond the defaults (Hammerspoon, System Preferences, and hs.window.filter.ignoreAlways) if `WindowManip.killAppWhenLastWindowClosed` is true
---
--- Parameters:
---  * ... - Additional application names as individual arguments
---
--- Returns:
---  * nil
function obj:ignore(...)
  self.logger:d('Ignoring additional applications:', ...)

  doNotQuitApplicationIfNoWindowsRemain:push(...)
end

--- WindowManip:start(mapping)
--- Method
--- Start the WindowManip spoon, creating window filter, filter subscription, switcher, screen watcher, etc.
---
--- Parameters:
---  * mapping - (Optional) Hotkey mapping as described in WindowManip:bindHotkeys(mapping). Overrides any hotkeys bound in a separate call to WindowManip:bindHotkeys(mapping)
---
--- Returns:
---  * nil
function obj:start(mapping)
  self.logger:df('Starting...')

  self.logger:df('Creating window filter for Windows-style Alt+Tab behavior')
  self.filter = windowFilter.new():setCurrentSpace(true):setDefaultFilter({
    -- Microsoft Teams seems to spawn an invisible window in addition to the visible one, but the invisible window's title is always "Microsoft Teams"
    rejectTitles = {'Microsoft Teams'},
  })
  self.logger:df('Subscribing to window events')
  self.filter:subscribe(WindowSubscriptions)

  self.logger:df('Creating window switcher for Windows-style Alt+Tab behavior')
  self.switcher = Switcher:new(self.filter, {
    backgroundColor = 'rgba(0, 0, 0, 0.75)',
    selectedApplicationBackgroundColor = 'rgba(255, 255, 255, 0.25)',
    thumbnailSize = 256,
  })

  self.logger:df('Creating screen watcher to update current window screen when screens are added/removed')
  self.screenWatcher = screenWatcher.new(utils.debounce(0.5, rescan))

  rescan()

  self.logger:vf('Starting watchers')
  self.screenWatcher:start()

  if mapping then
    self:bindHotkeys(mapping)
  end

  self.logger:df('Started')
end

--- WindowManip:destroy()
--- Method
--- Explicitly destroy this WindowManip, window filter, window filter subscription(s), screen watcher, etc. for garbage collection purposes. Called automatically by Reloadable module when reloading this spoon.
---
--- Parameters:
---  * None
---
--- Returns:
---  * nil
function obj:destroy()
  self.logger:df('Destroying...')

  self.logger:vf('Unbinding hotkeys')
  for k,hotkey in pairs(hotkeys) do
    hotkey:off()
  end
  hotkeys = nil

  self.logger:vf('Unsubscribing from window events')
  self.filter:unsubscribe(WindowSubscriptions)
  self.logger:vf('Destroying window filter')
  self.filter = nil

  self.logger:vf('Destroying window switcher')
  self.switcher = nil

  self.logger:vf('Destroying screen watcher')
  self.screenWatcher:stop()
  self.screenWatcher = nil

  self.logger:vf('Destroyed')
  self.logger = nil

  self.createLogger = nil
end

return obj
