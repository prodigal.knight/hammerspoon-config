local HwEvents = require('HwEvents')
local Logger = require('StyledLogger')
local Promise = require('Promise')
local Reloadable = require('Reloadable')
local utils = require('utils')

local log = Logger:new('init.lua', Logger.Levels.Debug)

-- Configure console preferences and raise to front *before* proceeding with the rest of the init
hs.console.clearConsole()
hs.preferencesDarkMode(true)
utils.attempt('configuring the Hammerspoon console', function()
  hs.console.consoleCommandColor({ hex = '#f8f8f2', alpha = 1 })
  hs.console.consolePrintColor({ red = 0.5, green = 0.5, blue = 0.5, alpha = 1 })
  hs.console.consoleResultColor({ red = 0, green = 0.75, blue = 0.75, alpha = 1 })
  hs.console.outputBackgroundColor({ hex = '#111111', alpha = 1 })
  hs.console.windowBackgroundColor({ hex = '#0c0c0c', alpha = 1 })
  hs.console.toolbar(nil)

  if not hs.console.hswindow() then
    hs.openConsole(false)
  end
  hs.console.hswindow():raise()
end)

-- Standardized "onLoaded" callback to handle destructuring result array, configuration errors, etc.
local function onLoaded(cb)
  return function(res)
    local name, spoon = table.unpack(res)

    local status, result = pcall(cb, spoon)

    if not status then
      return Promise.reject(string.format('There was an issue configuring/starting spoon %s: %s', name, result))
    end

    return res
  end
end

hs.window.animationDuration = 0

local loading = true

log:info('Loading...')
Promise.allSettled({
  -- Configure reload shortcuts
  Promise.resolve():next(function()
    log:info('Binding custom shortcuts')
    HwEvents.bindSpec({{'alt'}, 'r'}, function() if not loading then hs.reload() end end)
    HwEvents.bindSpec({{'alt', 'shift'}, 'r'}, Reloadable.showPicker)

    HwEvents.bindSpec({{'alt'}, 't'}, function()
      local term = hs.application.get('iTerm2')

      if not term then
        hs.application.open('iTerm') -- Automatically starts a window
      else
        term:selectMenuItem({'Shell', 'New Window'})
      end
    end)
  end),
  -- Configure CAPSLOCK+Shift behavior (turn off CAPSLOCK while shift is held)
  Promise.resolve():next(function()
    log:info('Applying Unix/Windows-style CAPSLOCK+Shift behavior hack')
    HwEvents.on('keydown', function(evt)
      if evt.key == 'Shift' and evt[HwEvents.Symbols.RawFlags] & HwEvents.rawFlagMasks.alphaShift ~= 0 then
        hs.hid.capslock.toggle()

        local reapply
        reapply = HwEvents.on('keyup', function(evt)
          if evt.key == 'Shift' then
            hs.hid.capslock.toggle()

            reapply:off()
          end
        end)
      end
    end)
  end),
  -- Configure StatusOverview
  Reloadable.module('StatusOverview'):next(onLoaded(function(spoon)
    spoon.interval = 0.5

    spoon.showGPUTemperatureGraph = true
    spoon.showCPU = 'individual'
    spoon.showCPUTemperatureGraph = true
    spoon.showRAM = true
    spoon.showNetwork = true

    spoon.ui.normalAlpha = 0.75
    spoon.ui.hoveredAlpha = 0.25
    spoon.ui.graphBorderColor = nil

    spoon:start()
  end)),
  -- Configure ImprovedHCalendar
  Reloadable.module('ImprovedHCalendar'):next(onLoaded(function(spoon)
    spoon.titleFormat = '%Y-%m-%d'
    spoon.showProgress = true

    spoon:start()
  end)),
  -- Configure ImprovedCaffeine
  Reloadable.module('ImprovedCaffeine'):next(onLoaded(function(spoon)
    spoon:start({
      lock = {{'alt'}, 'l'},
      display = {{'alt'}, 'c'},
    })
  end)),
  -- Configure TruePushToTalk
  Reloadable.module('TruePushToTalk'):next(onLoaded(function(spoon)
    spoon.pushTo = 'talk'
    spoon.keys:push('Clear') -- Add Num Lock as a valid key for toggling
    spoon.indicateButtonPushed = 'scroll' -- Turn Scroll Lock light on/off when unmuted/muted

    spoon:start()
  end)),
  -- Configure WindowManip
  Reloadable.module('WindowManip'):next(onLoaded(function(spoon)
    -- Force application quit when all its windows are closed to mimic Unix/Windows behavior
    spoon.killAppWhenLastWindowClosed = true
    -- Ignore the following applications that can run with no open windows (in addition to defaults, e.g. Finder)
    spoon:ignore('iCUE', 'Scroll Reverser', 'Viscosity', 'Turbo Boost Switcher', 'OneDrive')

    spoon:start({
      full_height = {{'alt', 'shift'}, 'ArrowUp'},
      move_up = {{'alt'}, 'ArrowUp'},
      move_down = {{'alt'}, 'ArrowDown'},
      move_left = {{'alt'}, 'ArrowLeft'},
      move_right = {{'alt'}, 'ArrowRight'},

      next_window = {{'cmd'}, 'Tab'},
      prev_window = {{'cmd', 'shift'}, 'Tab'},
    })
  end)),
  -- Configure ImprovedBingDaily
  Reloadable.module('ImprovedBingDaily'):next(onLoaded(function(spoon)
    spoon.downloadDestinationFolderPath = 'Pictures'

    spoon:start()
  end)),
  -- Configure FileWatcher
  Reloadable.module('FileWatcher'):next(onLoaded(function(spoon)
    -- Automatically have nginx validate and reload config when changed
    spoon:watch('/usr/local/etc/nginx/nginx.conf', {spoon.flags.itemIsFile}, utils.debounce(0.5, function()
      log:debug('nginx.conf was modified. Reloading nginx.')

      local ok, output = utils.os.runAllSudo(
        '/usr/local/bin/nginx -t 2>&1',
        '/usr/local/bin/nginx -s reload 2>&1'
      )

      if not ok then
        log:warn('Reload failed:\n%s', output)
      else
        log:debug('Reload succeeded:\n%s', output)
      end
    end))

    spoon:start()
  end)),
  -- Configure TurboBoost
  -- NOTE: Load this last so that it doesn't delay other spoons from taking effect while it's setting up
  Reloadable.module('ImprovedTurboBoost'):next(onLoaded(function(spoon)
    spoon.notify = false
    spoon.reenable_on_stop = false
    spoon.disable_on_start = true
    spoon.load_kext_cmd = "/sbin/kextload '%s'"
    spoon.unload_kext_cmd = "/sbin/kextunload '%s'"

    spoon.cmd_runner = function(cmd) local status, output, signal, code = utils.sudo(cmd) return status, output, signal, code end

    spoon:start({
      toggle = {{'alt', 'shift'}, 't'},
    })
  end)),
})
  :next(function(res)
    loading = false

    log:info('Done')

    local rejected = res:filter(function(result) return result.status == 'rejected' end)
    if #rejected > 0 then
      log:warn(
        'Some spoons/modules did not initialize correctly:\n%s',
        rejected:map(function(r) return '  ' .. r.reason end):join('\n')
      )
    end
  end)
