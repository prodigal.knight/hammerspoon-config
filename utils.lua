local Array = require('Array')
local Signals = require('Signals')
local Logger = require('StyledLogger')

local log = Logger:new('utils')

local utils = {}

local configdir = hs.configdir
local SUDO_PASSWORD = nil

--------------------------------------------------------------------------------
--                           Static Utility Members                           --
--------------------------------------------------------------------------------

-- None yet

--------------------------------------------------------------------------------
--                          Static Utility Functions                          --
--------------------------------------------------------------------------------

-- Wrap pcall and log a warning if it failed
function utils.attempt(description, fn, ...)
	local status, res = pcall(fn, ...)

	if not status then
		log:wf('utils.attempt: An error occurred while %s: %s', description, res)
	end

	return status, res
end

-- Why isn't this part of the standard math library?
function utils.between(v, min, max)
	return v >= min and v <= max
end

-- Same here, why do languages not include a default `clamp` function?
function utils.clamp(v, min, max)
	if v < min then
		return min
	elseif v > max then
		return max
	else
		return v
	end
end

-- Utility for my local modules to create a styled logger for them
function utils.createLogger(name, level)
	log:vf('Creating logger for "%s" with level "%o"', name, level)

	return Logger:new(name, level)
end

-- Prevent functions from being called too often in a short period of time
function utils.debounce(delay, cb)
	local args
	local timer = hs.timer.delayed.new(delay, function()
		cb(table.unpack(args))
	end)

	return function(...)
		args = table.pack(...)
		if timer:running() then
			log:vf('debounce: Debouncing delay for callback: %s', cb)
		else
			log:vf('debounce: Starting debounced timer for callback: %s', cb)
		end

		timer:start(delay)
	end
end

-- Return default if value is nil
function utils.default(value, default)
	return value ~= nil and value or default
end

-- Defer execution of a function since Lua doesn't support a native `defer` keyword block
function utils.defer(cb, ...)
	local args = table.pack(args)
  -- NOTE: For some reason `hs.timer.doAfter(time, fn) doesn't work here for
  -- multiple simultaneous timers and only fires for the first one
  local timer

  timer = hs.timer.new(0.005, function()
    local status, res = pcall(cb, table.unpack(args))

    if not status then
      log:wf('An error occurred while executing a deferred function: %s', res)
    end

    timer:stop()
  end)

  timer:start()
end

-- Check to see if a file exists at the given path
function utils.fileExists(path)
	log:vf('fileExists: Checking to see if file "%s" exists', path)
	local file = io.open(path, 'r')

	if not file then
		log:vf('fileExists: File "%s" does not exist', path)
		return false
	end

	log:vf('fileExists: File "%s" exists', path)
	file:close()

	return true
end

-- Handy to have as a callback for debugging purposes
function utils.identity(value)
	return value
end

-- Determine whether or not a given year is a leap year
function utils.isLeapYear(year)
  return value % 4 == 0 and (not value % 100 == 0 or value % 400 == 0)
end

-- Handy to have as a callback argument if you don't want to do anything
function utils.nop()
	-- Do nothing
end

-- Attempt to convert a color array [r,g,b,a] to a hs.drawing.color table
function utils.toColor(t)
  if type(t) ~= 'table' or t.red or not t[1] then
    return t
  end

  return {
    red = t[1] or 0,
    green = t[2] or 0,
    blue = t[3] or 0,
    alpha = t[4] or 1,
  }
end

-- Trim whitespace from the start/end of a string
function utils.trim(str)
	return str:gsub('^%s+', ''):gsub('%s+$', '')
end

-- Clamp a number between two others, wrapping around if it is outside the bounds
function utils.wrap(n, min, max)
	if n < min then
		return max
	elseif n > max then
		return min
	else
		return n
	end
end

--------------------------------------------------------------------------------
--                              Utility Objects                               --
--------------------------------------------------------------------------------

utils.env = {}

-- Replace environment variables in a given string with their current values
function utils.env.replace(str)
	return str:gsub('$(%w+)', os.getenv):gsub('${(%w+)}', os.getenv)
end

utils.path = {}

-- Get absolute path
function utils.path.absolute(path)
	return hs.fs.pathToAbsolute(path)
end

-- Join multiple parts of a path with the system separator (hardcoded here as '/' because this will only ever run on a Mac)
function utils.path.join(...)
	return Array:new(...):join('/')
end

utils.os = {}

-- Function for creating coroutines in order to run commands via the OS
local osRunCoroutineFunction = function(cmd, loggableCommand)
  log:vf('utils.os.run: Running: %s', loggableCommand)
  local handle = assert(io.popen(cmd, 'r'))
  local output = utils.trim(assert(handle:read('*a')))

  local ok, signal, code = handle:close()

  if not ok then
    if signal == 'signal' then
      log:vf('utils.os.run: %s: Interrupted by signal %s', loggableCommand, Signals[code])
    else
      log:vf('utils.os.run: %s: Exit code %d', loggableCommand, code)
    end
  else
    log:vf('utils.os.run: Success')
  end

  return ok, output, signal, code
end

-- Run a shell command and get the output
function utils.os.run(cmd, loggableCommand)
  local co = coroutine.create(osRunCoroutineFunction)
  local success, status, output, signal, code = coroutine.resume(co, cmd, loggableCommand or cmd)
  coroutine.close(co)

  if not success then
    log:vf('An error occurred while running the OS execution coroutine: %s', status)

    return false, '', 'exit', -1
  end

  return status, output, signal, code
end

-- Turn the given shell command into one run using elevated privileges
function sudo(cmd)
  return 'echo "' .. SUDO_PASSWORD .. '" | /usr/bin/sudo -S -k ' .. cmd
end

function utils.sudo(cmd)
  return utils.os.run(sudo(cmd), cmd)
end

-- Run all commands and return output for all of them
local function runAll(continue, cmds)
	local output = Array:new()

	for i,cmd in ipairs(cmds) do
		local ok, out = utils.os.run(cmd)

		output:push(out)

		if not ok and not continue then
			log:error('%s failed: %s', cmd, out)
			return false, output:join('\n')
		end
	end

	return true, output:join('\n')
end

-- Run all commands, exiting on error
function utils.os.runAll(...)
	return runAll(false, table.pack(...))
end

-- Run all commands, continuing on error
function utils.os.runAllIgnoreExitCode(...)
	return runAll(true, table.pack(...))
end

-- Run all commands using elevated privileges, exiting on error
function utils.os.runAllSudo(...)
  return runAll(false, Array:new(...):map(sudo))
end

-- Run all commands using elevated privileges, continuing on error
function utils.os.runAllSudo(...)
  return runAll(true, Array:new(...):map(sudo))
end

--------------------------------------------------------------------------------
--                              Utility Classes                               --
--------------------------------------------------------------------------------

-- ID Generator
utils.IDGenerator = {}

function utils.IDGenerator:new()
	return setmetatable({ value = 0 }, { __index = self })
end

function utils.IDGenerator:next()
	self.value = self.value + 1;

	return self.value
end

--------------------------------------------------------------------------------
--                                   Setup                                    --
--------------------------------------------------------------------------------

function grabSudoPasswordFromFile()
  local replacementFunction = function() log:ef('Unable to get encoded sudo password from settings file') end
  local path = utils.env.replace(utils.path.join('${HOME}', '.hammerspoon', 'settings.json'))
  local contents = hs.json.read(path)

  if not contents then
    log:ef('Unable to find "' .. path .. '"')
    utils.sudo = replacementFunction
    return
  end

  if not contents.spe then
    log:wf('Unable to get encoded sudo password from settings file')
    utils.sudo = replacementFunction
    return
  end

  -- TODO: Make this more secure than simply base64-encoding the password and using a hard-to-guess value name
  local password = hs.base64.decode(contents.spe)

  password = string.gsub(password, '\\', '\\\\')
  password = string.gsub(password, '"', '\\"')
  SUDO_PASSWORD = password
end

grabSudoPasswordFromFile()

return utils
